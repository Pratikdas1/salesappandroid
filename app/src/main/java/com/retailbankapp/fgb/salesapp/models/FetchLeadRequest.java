package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by fgb on 11/24/16.
 */

public final class FetchLeadRequest {
    private SearchFilter searchFilter;

    public SearchFilter getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(SearchFilter searchFilter) {
        this.searchFilter = searchFilter;
    }
}
