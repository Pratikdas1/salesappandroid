package com.retailbankapp.fgb.salesapp.dashboard;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.retailbankapp.fgb.salesapp.AppApplication;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.customui.FontCache;
import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.LeadByCategoryHolder;

import java.util.ArrayList;
import java.util.List;


public class Tab2Fragment extends Fragment {

    private BarChart mChart;
    private List<LeadByCategoryHolder> leadByProductList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab2, container, false);
        mChart = (BarChart) view.findViewById(R.id.barChartLayout);
        AppApplication appApplication = (AppApplication) getActivity().getApplication();
        FetchLeadResponse fetchLeadResponse = appApplication.getmDashboardData();
        if(null != fetchLeadResponse){

            if(fetchLeadResponse.getLeadsByProduct() != null && !fetchLeadResponse.getLeadsByProduct().isEmpty()){
                view.findViewById(R.id.noDataFoundTextView).setVisibility(View.GONE);
                view.findViewById(R.id.chartView).setVisibility(View.VISIBLE);
                leadByProductList = new ArrayList<LeadByCategoryHolder>();
                leadByProductList.addAll(fetchLeadResponse.getLeadsByProduct());
                intializeChart();
            }
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mChart = null ;
        leadByProductList = null;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void intializeChart() {
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);
      //  IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(FontCache.get(getString(R.string.app_font), getContext()));
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setTextSize(12f);
    //    xAxis.setValueFormatter(xAxisFormatter);

    //    IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
    //    leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
      //  leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setTypeface(FontCache.get(getString(R.string.app_font), getContext()));
        rightAxis.setLabelCount(8, false);
       // rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        mChart.getLegend().setEnabled(false);

        setData();
        mChart.setVisibleXRangeMaximum(4);
        mChart.animateXY(1000, 1000);

    }

    @Override
    public void onResume() {
        super.onResume();
        //mChart.animateXY(500, 500);
    }

    private void setData() {

        int i = 0 ;
        String xAxisValues[] = new String[leadByProductList.size()];
        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

        for (LeadByCategoryHolder product : leadByProductList) {
            yVals1.add(new BarEntry(i, product.getPercentageTotal()));
            xAxisValues[i] = product.getCategory();
            i+= 1;
        }

        BarDataSet set1;

        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            //set1 = new BarDataSet(yVals1, "The year 2017");
            set1 = new BarDataSet(yVals1, "");
            set1.setColors(ColorTemplate.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueTypeface(FontCache.get(getString(R.string.app_font), getContext()));
            data.setBarWidth(0.9f);
            mChart.getXAxis().setValueFormatter(new LabelFormatter(xAxisValues));
            mChart.setData(data);
        }
    }

    public class LabelFormatter implements IAxisValueFormatter {
        private final String[] mLabels;

        public LabelFormatter(String[] labels) {
            mLabels = labels;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return mLabels[(int) value];
        }
    }
}
