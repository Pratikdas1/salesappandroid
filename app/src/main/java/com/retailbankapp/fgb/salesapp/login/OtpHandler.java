package com.retailbankapp.fgb.salesapp.login;

import android.text.TextUtils;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.databinding.ActivityLoginBinding;
import com.retailbankapp.fgb.salesapp.databinding.ActivityOtpBinding;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.User;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPRequest;

/**
 * Created by O7548 on 12/5/2016.
 */

public class OtpHandler {

    private String TAG = OtpHandler.class.getName() ;
    public String VERIFY_STRING = Constants.VERIFY ;
    private ActivityOtpBinding mOtpBinding;

    public OtpHandler(ActivityOtpBinding mOtpBinding) {
        this.mOtpBinding = mOtpBinding ;
    }

    public  boolean validate(){
        Logger.v(TAG,"Otp Validate");
        mOtpBinding.layoutOtp.setError(null);
        if(TextUtils.isEmpty(String.valueOf(mOtpBinding.otpEditText.getText()))) {
            mOtpBinding.layoutOtp.setError(Constants.OTP_REQUIRED);
            return  false;
        }
        return  true ;
    }

    public void setErros() {
        mOtpBinding.layoutOtp.setError(Constants.INVALID_OTP);
    }
}
