package com.retailbankapp.fgb.salesapp.models;

import com.retailbankapp.fgb.salesapp.common.AbstractActivity;

import java.util.List;

/**
 * Created by Pratik Das on 12/6/16.
 */

public enum SessionContext {

    INSTANCE;
    private String encIv;
    private String encKey;
    private String userName;
    private String userEmail;
    private String userID;
    private String convID;
    private String macKey;
    private String deviceID;
    private String deviceToken;
    private Integer ec = 0;
    private AbstractActivity context ;

    private StaticDataHolder staticDataHolder;
    private List<MenuItem> menuItems;

    private double latitude ;
    private double longitude ;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public AbstractActivity getContext() {
        return context;
    }

    public void setContext(AbstractActivity context) {
        this.context = context;
    }

    public StaticDataHolder getStaticDataHolder() {
        return staticDataHolder;
    }

    public void setStaticDataHolder(StaticDataHolder staticDataHolder) {
        this.staticDataHolder = staticDataHolder;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void incrementEC(){
        ec = ec + 1;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public void clear(){
        encIv=null;
        encKey=null;
        userName=null;
        userID=null;
        convID=null;
        macKey=null;
        deviceID=null;
        Integer ec=0;
        StaticDataHolder staticDataHolder = null;
        menuItems = null;
    }
    
    public void set(String encIv,
                    String encKey,
                    String userName,
                    String userID,
                    String convID, String macKey,String userEmail
          ){
        this.encIv = encIv;
        this.encKey = encKey;
        this.userID = userID;
        this.userName = userName;
        this.convID = convID;
        this.macKey = macKey;
        this.userEmail = userEmail ;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Integer getEc() {
        return ec;
    }

    public void setEc(Integer ec) {
        this.ec = ec;
    }

    public void setMacKey(String macKey) {
        this.macKey = macKey;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getMacKey() {
        return macKey;
    }

    public String getConvID() {
        return convID;
    }

    public void setConvID(String convID) {
        this.convID = convID;
    }

    public String getEncIv() {
        return encIv;
    }

    public void setEncIv(String encIv) {
        this.encIv = encIv;
    }

    public String getEncKey() {
        return encKey;
    }

    public void setEncKey(String encKey) {
        this.encKey = encKey;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
