package com.retailbankapp.fgb.salesapp.lead;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;

import com.microblink.recognizers.blinkid.mrtd.MRTDRecognitionResult;
import com.retailbankapp.fgb.salesapp.AppApplication;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.customui.AppEditText;
import com.retailbankapp.fgb.salesapp.customui.DatePickerUtility;
import com.retailbankapp.fgb.salesapp.databinding.ActivityCreateLeadBinding;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.helpers.StaticData;
import com.retailbankapp.fgb.salesapp.helpers.Utils;
import com.retailbankapp.fgb.salesapp.models.Document;
import com.retailbankapp.fgb.salesapp.models.Lead;
import com.retailbankapp.fgb.salesapp.models.ProductSelection;
import com.retailbankapp.fgb.salesapp.models.SessionContext;
import com.retailbankapp.fgb.salesapp.models.StaticDataTypes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by O7548 on 12/7/2016.
 */

public class CreateLeadHandler implements View.OnTouchListener, View.OnFocusChangeListener {

    private final ActivityCreateLeadBinding mActivityCreateLeadBinding;
    private String TAG = CreateLeadHandler.class.getName();
    public String PROCEED_STRING = Constants.PROCEED;
    AbstractActivity mCreateLeadActivity;
    private HashMap<String, String> countryMap;
    private static boolean isPopDisplayed;

    public CreateLeadHandler(ActivityCreateLeadBinding fragmentLeadBinding, AbstractActivity createLeadActivity) {
        this.mActivityCreateLeadBinding = fragmentLeadBinding;
        mCreateLeadActivity = createLeadActivity;
        if(createLeadActivity instanceof UpdateLeadActivity){
            Logger.e(TAG,"In Edit Lead Activity");
            PROCEED_STRING = Constants.SAVE ;
        }
        initialize();
    }


    private void initializeCountryMap() {
        countryMap = new HashMap<String, String>();
   /*     countryMap.put("IND","India");
        countryMap.put("ARE","UAE");
        countryMap.put("AND","ANDORRA");
        countryMap.put("AFG","AFGANISTAN");
        countryMap.put("ATG","ANTIGUA AND BARBUDA");
        countryMap.put("AIA","ANGUILLA");
        countryMap.put("ALB","ALBANIA");
        countryMap.put("ARM","ARMENIA");
        countryMap.put("AGO","ANGOLA");
        countryMap.put("ATA","ANTARCTICA");
        countryMap.put("ARG","ARGENTINA");
        countryMap.put("ASM","AMERICAN SAMOA");
        countryMap.put("AUT","AUSTRIA");
        countryMap.put("AUS","AUSTRALIA");
        countryMap.put("ABW","ARUBA");
        countryMap.put("AZE","AZERBAIJAN");

        countryMap.put("BIH","BOSNIA-HERZEGOVINA");
        countryMap.put("BRB","BARBADOS");
        countryMap.put("BGD","BANGLADESH");
        countryMap.put("BEL","BELGIUM");
        countryMap.put("BFA","BURKINA FASO");
        countryMap.put("BGR","BULGARIA");
        countryMap.put("BHR","BAHRAIN");
        countryMap.put("BDI","BURUNDI");
        countryMap.put("BEN","BENIN");
        countryMap.put("BMU","BERMUDA");
        countryMap.put("BRN","BRUNEI DARUSSALAM");
        countryMap.put("BOL","BOLIVIA");
        countryMap.put("BRA","BRAZIL");
        countryMap.put("BHS","BAHAMAS");
        countryMap.put("BTN","BHUTAN");
        countryMap.put("BVT","BOUVET ISLAND");
        countryMap.put("BWA","BOTSWANA");
        countryMap.put("BLR","BELARUS");
        countryMap.put("BLZ","BELIZE");
        countryMap.put("CAN","CANADA");
        countryMap.put("CCK","COCOS (KEELING) ISLANDS");
        countryMap.put("COD","CONGO DEMOCRATIC REPUBLIC OF THE");
        countryMap.put("CAF","CENTRAL AFRICAN REPUBLIC");
        countryMap.put("COG","CONGO");
        countryMap.put("CHE","SWITZERLAND");
        countryMap.put("CIV","IVORY COAST");
        countryMap.put("COK","COOK ISLANDS");
        countryMap.put("CHL","CHILE");
        countryMap.put("CMR","CAMEROON");
        countryMap.put("CHN","PEOPLES REPUBLIC OF CHINA");
        countryMap.put("COL","COLUMBIA");
        countryMap.put("CRI","COSTA RICA");
        countryMap.put("SRB","SERBIA");
        countryMap.put("CUB","CUBA");
        countryMap.put("CPV","CAPE VERDE");
        countryMap.put("CXR","CHRISTMAS ISLANDS");
        countryMap.put("CYP","CYPRUS");
        countryMap.put("CZE","CZECH REPUBLIC");
        countryMap.put("DEU","GERMANY");
        countryMap.put("DJI","DJIBOUTI");
        countryMap.put("DNK","DENMARK");
        countryMap.put("DMA","DOMINICA");
        countryMap.put("DOM","DOMINICAN REPUBLIC");
        countryMap.put("DZA","ALGERIA");
        countryMap.put("ECU","ECUADOR");
        countryMap.put("EST","ESTONIA");
        countryMap.put("EGY","EGYPT");
        countryMap.put("ESH","WESTERN SAHARA");
        countryMap.put("ERI","ERITREA");
        countryMap.put("ESP","SPAIN");
        countryMap.put("ETH","ETHIOPIA");
        countryMap.put("FIN","FINLAND");
        countryMap.put("FJI","FIJI");
        countryMap.put("FLK","FALKLAND ISLANDS (MALVINAS)");
        countryMap.put("FSM","MICRONESIA FEDERATED STATES OF");
        countryMap.put("FRO"," FAROE ISLANDS");
        countryMap.put("FRA","FAROE ISLANDS");
        countryMap.put("GAB","GABON");
        countryMap.put("GBR","GREAT BRITAIN");
        countryMap.put("GRD","GRENADA");
        countryMap.put("GEO","GEORGIA");
        countryMap.put("GUF","FRENCH GUIANA");
        countryMap.put("GGY","CHANNEL ISLANDS AND GUERNSEY");
        countryMap.put("GHA","GHANA");

        countryMap.put("GIB","GIBRALTAR");
        countryMap.put("GRL","GREENLAND");
        countryMap.put("GMB","GAMBIA");
        countryMap.put("GIN","GUINEA");
        countryMap.put("GLP","GUADELOUPE");
        countryMap.put("GNQ","EQUATORIAL GUINEA");
        countryMap.put("GRC","GREECE");
        countryMap.put("SGS","SOUTH GEORGIA a SANDWICH IS");
        countryMap.put("GTM","GUATEMALA");
        countryMap.put("GUM","GUAM");
        countryMap.put("GNB","GUINEA-BISSAU");
        countryMap.put("GUY","GUYANA");
        countryMap.put("HKG","HONG KONG");
        countryMap.put("HMD","HEARD AND MCDONALD ISLANDS");
        countryMap.put("HND","HONDURAS");
        countryMap.put("HRV","CROATIA");
        countryMap.put("HTI","HAITI");
        countryMap.put("HUN","HUNGARY");
        countryMap.put("IDN","INDONESIA");
        countryMap.put("IRL","IRELAND");
        countryMap.put("IMN","ISLE OF MAN");
        countryMap.put("IOT","BRITISH INDIAN OCEAN TERRITORY");
        countryMap.put("IRQ","IRAQ");
        countryMap.put("IRN","IRAN (ISLAMIC REPUBLIC OF)");
        countryMap.put("ISL","ICELAND");
        countryMap.put("ITA","ITALY");
        countryMap.put("JEY","JERSEY");
        countryMap.put("JAM","JAMAICA");

        countryMap.put("JOR","JORDAN");
        countryMap.put("JPN","JAPAN");
        countryMap.put("KEN","KENYA");
        countryMap.put("KGZ","KYRGYZSTAN");
        countryMap.put("KHM","CAMBODIA");
        countryMap.put("KIR","KIRIBATI");
        countryMap.put("COM","COMORO ISLANDS");
        countryMap.put("KNA","SAINT KITTS AND NEVIS");
        countryMap.put("PRK","KOREA DEMOCRATIC PEOPLES REP. OF");
        countryMap.put("KOR","KOREA REPUBLIC OF");
        countryMap.put("KWT","KUWAIT");
        countryMap.put("CYM","CAYMAN ISLANDS");
        countryMap.put("KAZ","KAZAKSTAN");
        countryMap.put("LAO","LAO PEOPLES DEMOCRATIC REPUBLIC");
        countryMap.put("LBN","LEBANON");
        countryMap.put("LCA","SAINT LUCIA");
        countryMap.put("LIE","LIECHTENSTEIN");
        countryMap.put("LKA","SRI LANKA");
        countryMap.put("LBR","LIBERIA");
        countryMap.put("LSO","LESOTHO");
        countryMap.put("LTU","LITHUANIA");
        countryMap.put("LUX","LUXEMBOURG");
        countryMap.put("LVA","LATIVA");
        countryMap.put("LBY","LIBYAN ARAB JAMAHIRIYA");
        countryMap.put("MAR","MOROCCO");
        countryMap.put("MCO","MONACO");
        countryMap.put("MDA","MOLDOVA REPUBLIC OF");
        countryMap.put("MNE","MONTENEGRO");
        countryMap.put("MDG","MADAGASCAR");
        countryMap.put("MHL"," MARSHALL ISLANDS REPUBLIC OF");
        countryMap.put("MKD"," MACEDONIA");
        countryMap.put("MLI"," MALI");
        countryMap.put("MMR"," MYANMAR");
        countryMap.put("MNG"," MONGOLIA");*/


        String[] countryCodes = Locale.getISOCountries();

        for (String cc : countryCodes) {
            Locale locale = new Locale("", cc);
            countryMap.put(locale.getISO3Country(), locale.getDisplayCountry());
        }
    }


    public void populateDataFromBlink(MRTDRecognitionResult mrtdRecognitionResult) {
       // initializeCountryMap();
        if (mrtdRecognitionResult.isValid() && !mrtdRecognitionResult.isEmpty()) {
            if (mrtdRecognitionResult.isMRZParsed()) {
                Logger.e(TAG,mrtdRecognitionResult.toString());
                Logger.e(TAG,mrtdRecognitionResult.getSecondaryId().replace(",", ""));
                mActivityCreateLeadBinding.firstNameEditText.setText(mrtdRecognitionResult.getSecondaryId().replace(",", ""));
                mActivityCreateLeadBinding.lastNameEditText.setText(mrtdRecognitionResult.getPrimaryId().replace(",", ""));
                Date dateOfBirth = mrtdRecognitionResult.getDateOfBirth();
                String DATE_FORMAT = "MM/dd/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
                mActivityCreateLeadBinding.dobTextEditText.setText(sdf.format(dateOfBirth));
                if (mrtdRecognitionResult.getSex().equalsIgnoreCase(AppConstants.Gender.MALE_CODE)) {
                    mActivityCreateLeadBinding.genderEditText.setText(AppConstants.Gender.MALE);
                } else {
                    mActivityCreateLeadBinding.genderEditText.setText(AppConstants.Gender.FEMALE);
                }

                String nationality = SessionContext.INSTANCE.getStaticDataHolder().getStaticDataDesc(StaticDataTypes.COUNTRY.name(),mrtdRecognitionResult.getNationality());
                mActivityCreateLeadBinding.nationalityEditText.setText(nationality);
                mActivityCreateLeadBinding.emiratesIdEditText.setText(mrtdRecognitionResult.getOpt1());
            }
        }
    }

    private void initialize() {
        mActivityCreateLeadBinding.firstNameEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.lastNameEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.mobileEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.incomeEditText.setOnFocusChangeListener(this);

        mActivityCreateLeadBinding.dobTextEditText.setOnFocusChangeListener(this);
        //    mActivityCreateLeadBinding.dobTextEditText.setOnTouchListener(this);
        //  mActivityCreateLeadBinding.dobTextEditText.setOnClickListener(this);
        mActivityCreateLeadBinding.dobTextEditText.setRawInputType(InputType.TYPE_NULL);

        mActivityCreateLeadBinding.emailEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.nationalityEditText.setOnTouchListener(this);
        mActivityCreateLeadBinding.nationalityEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.nationalityEditText.setRawInputType(InputType.TYPE_NULL);

        mActivityCreateLeadBinding.productTypeEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.productTypeEditText.setOnTouchListener(this);
        mActivityCreateLeadBinding.productTypeEditText.setRawInputType(InputType.TYPE_NULL);

        mActivityCreateLeadBinding.productInterestEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.productInterestEditText.setOnTouchListener(this);
        mActivityCreateLeadBinding.productInterestEditText.setRawInputType(InputType.TYPE_NULL);

        mActivityCreateLeadBinding.philosophyEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.philosophyEditText.setOnTouchListener(this);
        mActivityCreateLeadBinding.philosophyEditText.setRawInputType(InputType.TYPE_NULL);

        mActivityCreateLeadBinding.subProductInterestEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.subProductInterestEditText.setOnTouchListener(this);
        mActivityCreateLeadBinding.subProductInterestEditText.setRawInputType(InputType.TYPE_NULL);

        mActivityCreateLeadBinding.tierEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.tierEditText.setOnTouchListener(this);
        mActivityCreateLeadBinding.tierEditText.setRawInputType(InputType.TYPE_NULL);


        mActivityCreateLeadBinding.genderEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.genderEditText.setOnTouchListener(this);
        mActivityCreateLeadBinding.genderEditText.setRawInputType(InputType.TYPE_NULL);

        mActivityCreateLeadBinding.emiratesEditText.setOnTouchListener(this);
        mActivityCreateLeadBinding.emiratesEditText.setOnFocusChangeListener(this);
        mActivityCreateLeadBinding.emiratesEditText.setRawInputType(InputType.TYPE_NULL);
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus) {
            switch (view.getId()) {
                case R.id.dobTextEditText:
                    //mCreateLeadActivity.hideKeyboard();

                    Logger.e(TAG, "onFocusChange");
                    (new DatePickerUtility()).setDate(mActivityCreateLeadBinding.dobTextEditText, mCreateLeadActivity);
                    mActivityCreateLeadBinding.emailEditText.requestFocus();

                    break;
                case R.id.nationalityEditText:
                    Logger.e(TAG, "onFocusChange");
                    if (!isPopDisplayed) {
                        mCreateLeadActivity.hideKeyboard();
                        populateDropDown(mActivityCreateLeadBinding.nationalityEditText, Constants.NATIONALITY
                                , SessionContext.INSTANCE.getStaticDataHolder().getStaticData(StaticDataTypes.COUNTRY.name()));
                        //showCountryList();
                    }
                    break;
                case R.id.productTypeEditText:
                case R.id.philosophyEditText:
                case R.id.genderEditText:
                case R.id.subProductInterestEditText :
                case R.id.tierEditText :
                    mCreateLeadActivity.hideKeyboard();
                /*case R.id.emiratesEditText :

                    break;*/

            }
        } else {
            final Lead lead = mActivityCreateLeadBinding.getLead();
            switch (view.getId()) {
                case R.id.firstNameEditText:
                    if (TextUtils.isEmpty(lead.getFirstName())) {
                        mActivityCreateLeadBinding.layoutFirstname.setError(Constants.FIRST_NAME_MANDATORY);
                        //mActivityCreateLeadBinding.firstNameEditText.requestFocus();
                    } else {
                        mActivityCreateLeadBinding.layoutFirstname.setError(null);
                    }
                    break;
                case R.id.lastNameEditText:
                    if (TextUtils.isEmpty(lead.getLastName())) {
                        mActivityCreateLeadBinding.layoutLastname.setError(Constants.LAST_NAME_MANDATORY);
                    } else {
                        mActivityCreateLeadBinding.layoutLastname.setError(null);
                    }
                    break;
                case R.id.mobileEditText:
                    if (TextUtils.isEmpty(lead.getMobileNumber())) {
                        mActivityCreateLeadBinding.layoutMobile.setError(Constants.PHONE_MANDATORY);
                    } else {
                        mActivityCreateLeadBinding.layoutMobile.setError(null);
                    }
                    break;
                case R.id.incomeEditText:
                    if (TextUtils.isEmpty(lead.getMonthlySalary())) {
                        mActivityCreateLeadBinding.layoutMonthlyincome.setError(Constants.MONTHLY_INCOME_MANDATORY);
                    } else {
                        mActivityCreateLeadBinding.layoutMonthlyincome.setError(null);
                    }
                    break;
                case R.id.dobTextEditText:
                   /* if(TextUtils.isEmpty(lead.getDateOfBirth())){
                        mActivityCreateLeadBinding.layoutDob.setError(Constants.DOB_MANDAROTY);
                    }else{
                        mActivityCreateLeadBinding.layoutDob.setError(null);
                    }*/
                    break;
                case R.id.nationalityEditText:
                    if (TextUtils.isEmpty(lead.getNationality())) {
                        mActivityCreateLeadBinding.layoutNationality.setError(Constants.NATIONALITY_MANDAROTY);
                    } else {
                        mActivityCreateLeadBinding.layoutNationality.setError(null);
                    }
                    break;
                case R.id.productTypeEditText:
                    if (TextUtils.isEmpty(lead.getProductType())) {
                        mActivityCreateLeadBinding.layoutProducttype.setError(Constants.PRODUCT_TYPE_MANDAROTY);
                    } else {
                        mActivityCreateLeadBinding.layoutProducttype.setError(null);
                    }
                    break;
                case R.id.productInterestEditText:
                    if (TextUtils.isEmpty(lead.getProductInterest())) {
                        mActivityCreateLeadBinding.layoutProductinterest.setError(Constants.PRODUCT_INTEREST_MANDAROTY);
                    } else {
                        mActivityCreateLeadBinding.layoutProductinterest.setError(null);
                    }
                    break;
                case R.id.philosophyEditText:
                    if (TextUtils.isEmpty(lead.getPhilosophy())) {
                        mActivityCreateLeadBinding.layoutPhilosophy.setError(Constants.PHILOSOPY_MANDAROTY);
                    } else {
                        mActivityCreateLeadBinding.layoutPhilosophy.setError(null);
                    }
                    break;

                case R.id.genderEditText:
                    if (TextUtils.isEmpty(lead.getGender())) {
                        mActivityCreateLeadBinding.layoutGender.setError(Constants.GENDER_MANDAROTY);
                    } else {
                        mActivityCreateLeadBinding.layoutGender.setError(null);
                    }
                    break;
                case R.id.emiratesEditText:
                    if (TextUtils.isEmpty(lead.getEmirate())) {
                        mActivityCreateLeadBinding.layoutEmirates.setError(Constants.EMIRATES_MANDATORY);
                    } else {
                        mActivityCreateLeadBinding.layoutEmirates.setError(null);
                    }
                    break;
                case R.id.enameEditText:
                    if (TextUtils.isEmpty(lead.getEmployerName())) {
                        mActivityCreateLeadBinding.layoutEmployername.setError(Constants.EMPNAME_REQUIRED);
                    } else {
                        mActivityCreateLeadBinding.layoutEmployername.setError(null);
                    }
                    break;
                case R.id.passportEditText:
                    if (TextUtils.isEmpty(lead.getPassportNumber())) {
                        mActivityCreateLeadBinding.layoutPassportnum.setError(Constants.PASSPORT_MANDATORY);
                    } else {
                        mActivityCreateLeadBinding.layoutPassportnum.setError(null);
                    }
                    break;
                case R.id.emiratesIdEditText:
                    if (TextUtils.isEmpty(lead.getEmiratesID())) {
                        mActivityCreateLeadBinding.layoutEmiratesid.setError(Constants.EMIRATES_ID_MANDATORY);
                    } else {
                        mActivityCreateLeadBinding.layoutEmiratesid.setError(null);
                    }
                    break;
            }
        }

    }

    public void clearFields() {
        mActivityCreateLeadBinding.firstNameEditText.requestFocus();
        mActivityCreateLeadBinding.firstNameEditText.setText(null);
        mActivityCreateLeadBinding.lastNameEditText.setText(null);
        mActivityCreateLeadBinding.mobileEditText.setText(null);
        mActivityCreateLeadBinding.incomeEditText.setText(null);
        mActivityCreateLeadBinding.dobTextEditText.setText(null);
        mActivityCreateLeadBinding.emailEditText.setText(null);
        mActivityCreateLeadBinding.nationalityEditText.setText(null);
        mActivityCreateLeadBinding.productTypeEditText.setText(null);
        mActivityCreateLeadBinding.productInterestEditText.setText(null);
        mActivityCreateLeadBinding.philosophyEditText.setText(null);
        mActivityCreateLeadBinding.genderEditText.setText(null);
        mActivityCreateLeadBinding.emailEditText.setText(null);
        mActivityCreateLeadBinding.emiratesEditText.setText(null);
        mActivityCreateLeadBinding.emiratesIdEditText.setText(null);
        mActivityCreateLeadBinding.passportEditText.setText(null);
        mActivityCreateLeadBinding.enameEditText.setText(null);
        mActivityCreateLeadBinding.commentEditText.setText(null);
        mActivityCreateLeadBinding.subProductInterestEditText.setText(null);
        mActivityCreateLeadBinding.tierEditText.setText(null);
       // mActivityCreateLeadBinding.ecbConsentSwitch.setChecked(false);
    }

    public boolean validate() {
        Logger.v(TAG, "Create Lead Validate");
        boolean isValid = true;
        boolean isFocusRequested = false ;
        Lead lead = mActivityCreateLeadBinding.getLead();
        mActivityCreateLeadBinding.layoutFirstname.setError(null);
        mActivityCreateLeadBinding.layoutLastname.setError(null);
        mActivityCreateLeadBinding.layoutMobile.setError(null);
        mActivityCreateLeadBinding.layoutMonthlyincome.setError(null);
        mActivityCreateLeadBinding.layoutDob.setError(null);
        mActivityCreateLeadBinding.layoutEmail.setError(null);
        mActivityCreateLeadBinding.layoutNationality.setError(null);
        mActivityCreateLeadBinding.layoutProducttype.setError(null);
        mActivityCreateLeadBinding.layoutProductinterest.setError(null);
        mActivityCreateLeadBinding.layoutPhilosophy.setError(null);
        mActivityCreateLeadBinding.layoutEmail.setError(null);
        mActivityCreateLeadBinding.layoutGender.setError(null);
        mActivityCreateLeadBinding.layoutEmirates.setError(null);
        mActivityCreateLeadBinding.layoutEmployername.setError(null);
        mActivityCreateLeadBinding.layoutPassportnum.setError(null);
        mActivityCreateLeadBinding.layoutEmiratesid.setError(null);
        mActivityCreateLeadBinding.layoutSubproductinterest.setError(null);
        mActivityCreateLeadBinding.layoutTier.setError(null);

        if (TextUtils.isEmpty(lead.getFirstName())) {
            mActivityCreateLeadBinding.layoutFirstname.setError(Constants.FIRST_NAME_MANDATORY);
            mActivityCreateLeadBinding.firstNameEditText.requestFocus();
            isFocusRequested = true ;
            isValid = false;
        }
        if (TextUtils.isEmpty(lead.getLastName())) {
            mActivityCreateLeadBinding.layoutLastname.setError(Constants.LAST_NAME_MANDATORY);
            if(!isFocusRequested){
                mActivityCreateLeadBinding.lastNameEditText.requestFocus();
                isFocusRequested = true ;
            }

            isValid = false;
        }
        if (TextUtils.isEmpty(lead.getMobileNumber())) {
            mActivityCreateLeadBinding.layoutMobile.setError(Constants.PHONE_MANDATORY);
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.mobileEditText.requestFocus();
                isFocusRequested = true ;
            }
            isValid = false;
        }
        if (TextUtils.isEmpty(lead.getMonthlySalary())) {
            mActivityCreateLeadBinding.layoutMonthlyincome.setError(Constants.MONTHLY_INCOME_MANDATORY);
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.incomeEditText.requestFocus();
                isFocusRequested = true ;
            }
            isValid = false;
        }
        if (TextUtils.isEmpty(lead.getDateOfBirth())) {
            mActivityCreateLeadBinding.layoutDob.setError(Constants.DOB_MANDAROTY);
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.dobTextEditText.requestFocus();
                isFocusRequested = true ;
            }
            isValid = false;
        }

        if (!TextUtils.isEmpty(lead.getEmail())) {
            if (!Utils.isValidEmail(mActivityCreateLeadBinding.emailEditText.getText())) {
                mActivityCreateLeadBinding.layoutEmail.setError(Constants.INVALID_EMAIL);
                isValid = false;
                if(!isFocusRequested) {
                    mActivityCreateLeadBinding.emailEditText.requestFocus();
                    isFocusRequested = true ;
                }
            }

        }else{
            mActivityCreateLeadBinding.layoutEmail.setError(Constants.EMAIL_MANDATORY);
            isValid = false;
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.emailEditText.requestFocus();
                isFocusRequested = true ;
            }
        }

        if (TextUtils.isEmpty(lead.getNationality())) {
            mActivityCreateLeadBinding.layoutNationality.setError(Constants.NATIONALITY_MANDAROTY);
            isValid = false;
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.nationalityEditText.requestFocus();
                isFocusRequested = true ;
            }
        }
        if (TextUtils.isEmpty(lead.getGender())) {
            mActivityCreateLeadBinding.layoutGender.setError(Constants.GENDER_MANDAROTY);
            isValid = false;
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.genderEditText.requestFocus();
                isFocusRequested = true ;
            }
        }

        if (TextUtils.isEmpty(lead.getProductType())) {
            mActivityCreateLeadBinding.layoutProducttype.setError(Constants.PRODUCT_TYPE_MANDAROTY);
            isValid = false;
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.productTypeEditText.requestFocus();
                isFocusRequested = true ;
            }
        }
        if (TextUtils.isEmpty(lead.getProductInterest())) {
            mActivityCreateLeadBinding.layoutProductinterest.setError(Constants.PRODUCT_INTEREST_MANDAROTY);
            isValid = false;
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.productInterestEditText.requestFocus();
                isFocusRequested = true ;
            }
        }
        if (TextUtils.isEmpty(lead.getPhilosophy())) {
            mActivityCreateLeadBinding.layoutPhilosophy.setError(Constants.PHILOSOPY_MANDAROTY);
            isValid = false;
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.philosophyEditText.requestFocus();
                isFocusRequested = true ;
            }
        }

        if(mActivityCreateLeadBinding.layoutSubproductinterest.getVisibility() == View.VISIBLE){
            if (TextUtils.isEmpty(lead.getSubProductInterest())) {
                mActivityCreateLeadBinding.layoutSubproductinterest.setError(Constants.FIELD_REQUIRED);
                isValid = false;
                if(!isFocusRequested) {
                    mActivityCreateLeadBinding.subProductInterestEditText.requestFocus();
                    isFocusRequested = true;
                }
            }
        }
        if(mActivityCreateLeadBinding.layoutTier.getVisibility() == View.VISIBLE){
            if (TextUtils.isEmpty(lead.getTier())) {
                mActivityCreateLeadBinding.layoutTier.setError(Constants.FIELD_REQUIRED);
                isValid = false;
                if(!isFocusRequested) {
                    mActivityCreateLeadBinding.tierEditText.requestFocus();
                    isFocusRequested = true;
                }
            }
        }

        if (TextUtils.isEmpty(lead.getEmirate())) {
            mActivityCreateLeadBinding.layoutEmirates.setError(Constants.EMIRATES_MANDATORY);
            isValid = false;
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.emiratesEditText.requestFocus();
                isFocusRequested = true ;
            }
        }
        if (TextUtils.isEmpty(lead.getEmployerName())) {
            mActivityCreateLeadBinding.layoutEmployername.setError(Constants.EMPNAME_REQUIRED);
            isValid = false;
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.enameEditText.requestFocus();
                isFocusRequested = true ;
            }
        }
        if (TextUtils.isEmpty(lead.getPassportNumber())) {
            mActivityCreateLeadBinding.layoutPassportnum.setError(Constants.PASSPORT_MANDATORY);
            isValid = false;
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.passportEditText.requestFocus();
                isFocusRequested = true;
            }
        }
        if (TextUtils.isEmpty(lead.getEmiratesID())) {
            mActivityCreateLeadBinding.layoutEmiratesid.setError(Constants.EMIRATES_ID_MANDATORY);
            isValid = false;
            if(!isFocusRequested) {
                mActivityCreateLeadBinding.emiratesIdEditText.requestFocus();
                isFocusRequested = true ;
            }
        }


       /* if (!mActivityCreateLeadBinding.ecbConsentSwitch.isChecked()) {
            mCreateLeadActivity.showError(Constants.PLS_ACCEPT_ECB_CONSENT);
            isValid = false;
        }*/
        return isValid;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            if (view.getId() != R.id.emiratesEditText)
                mCreateLeadActivity.hideKeyboard();
            switch (view.getId()) {
                case R.id.dobTextEditText:
                    Logger.e(TAG, "onTouch");
                    // (new DatePickerUtility()).setDate(mActivityCreateLeadBinding.dobTextEditText, mCreateLeadActivity);
                    break;
                case R.id.nationalityEditText:
                    Logger.e(TAG, "onTouch");
                    //showCountryList();
                    populateDropDown(mActivityCreateLeadBinding.nationalityEditText, Constants.NATIONALITY
                            , SessionContext.INSTANCE.getStaticDataHolder().getStaticData(StaticDataTypes.COUNTRY.name()));
                    break;
                case R.id.productTypeEditText:
                    populateDropDown(mActivityCreateLeadBinding.productTypeEditText, Constants.SELECT_PRODUCT
                            , SessionContext.INSTANCE.getStaticDataHolder().getStaticData(StaticDataTypes.PRODUCT_TYPE.name()));
                    break;
                case R.id.productInterestEditText:
                    String selectedProduct = String.valueOf(mActivityCreateLeadBinding.productTypeEditText.getText());
                  /*  if (selectedProduct.equalsIgnoreCase(Constants.AUTO)) {
                        populateDropDown(mActivityCreateLeadBinding.productInterestEditText, Constants.SELECT_PRODUCT_INTEREST, StaticData.getAutoProductNames());
                    } else if (selectedProduct.equalsIgnoreCase(Constants.CREDIT_CARD)) {
                        populateDropDown(mActivityCreateLeadBinding.productInterestEditText, Constants.SELECT_PRODUCT_INTEREST, StaticData.getCcProductNames());
                    } else if (selectedProduct.equalsIgnoreCase(Constants.PERSONAL)) {
                        populateDropDown(mActivityCreateLeadBinding.productInterestEditText, Constants.SELECT_PRODUCT_INTEREST, StaticData.getPersonalProductNames());
                    } else {
                        populateDropDown(mActivityCreateLeadBinding.productInterestEditText, Constants.SELECT_PRODUCT_INTEREST
                                ,  SessionContext.INSTANCE.getStaticDataHolder().getStaticData(StaticDataTypes.PRODUCT_INTEREST.name()));
                    }*/
                    if(!TextUtils.isEmpty(selectedProduct)){
                        populateDropDown(mActivityCreateLeadBinding.productInterestEditText, Constants.SELECT_PRODUCT_INTEREST
                                ,  SessionContext.INSTANCE.getStaticDataHolder().getStaticData(StaticDataTypes.PRODUCT_TYPE.name(),selectedProduct));
                    }else{
                        mActivityCreateLeadBinding.productInterestEditText.clearFocus();
                        mActivityCreateLeadBinding.productTypeEditText.requestFocus();
                    }

                    break;
                case R.id.philosophyEditText:
                    populateDropDown(mActivityCreateLeadBinding.philosophyEditText, Constants.SELECT_PHILOSOPHY
                            ,  SessionContext.INSTANCE.getStaticDataHolder().getStaticData(StaticDataTypes.PHILOSOPHY.name()));
                    break;
                case R.id.subProductInterestEditText:
                    populateDropDown(mActivityCreateLeadBinding.subProductInterestEditText, Constants.SELECT_PRODUCT_INTEREST
                            ,  SessionContext.INSTANCE.getStaticDataHolder().getStaticData(StaticDataTypes.ACC_CC_TYPE.name()));
                    break;
                case R.id.tierEditText:
                    populateDropDown(mActivityCreateLeadBinding.tierEditText, Constants.SELECT_TIER,  SessionContext.INSTANCE.getStaticDataHolder().getStaticData(StaticDataTypes.TIER.name()));
                    break;
                case R.id.genderEditText:
                    populateDropDown(mActivityCreateLeadBinding.genderEditText, Constants.SELECT_GENDER, SessionContext.INSTANCE.getStaticDataHolder().getStaticData(StaticDataTypes.GENDER.name()));
                    break;
                case R.id.emiratesEditText:
                    //showEmiratesList();
                    //populateDropDown(mActivityCreateLeadBinding.emiratesEditText, Constants.SELECT_EMIRATES, StaticData.getEmirates());
                    populateDropDown(mActivityCreateLeadBinding.emiratesEditText, Constants.SELECT_EMIRATES,
                            SessionContext.INSTANCE.getStaticDataHolder().getStaticData(StaticDataTypes.EMIRATES.name()));

                    break;
            }

        }
        return false;
    }

    public void populateDropDown(AppEditText dropDown, String title, List<com.retailbankapp.fgb.salesapp.models.StaticData> staticData) {
        int i = 0;
        isPopDisplayed = true ;
        int selectedPosition = -1;
        String[] list = null ;
        String selectedItem = String.valueOf(dropDown.getText());
        if(null != staticData && !staticData.isEmpty()){
            list = new String[staticData.size()];
            for (com.retailbankapp.fgb.salesapp.models.StaticData currentItem : staticData) {
                if (!TextUtils.isEmpty(currentItem.getDescription()) && selectedItem.equalsIgnoreCase(currentItem.getDescription())) {
                    selectedPosition = i;
                }
                list[i] = currentItem.getDescription();
                i++;
            }
        }
        showDropDownDialog(title, dropDown, list, selectedPosition);
    }

    private void showDropDownDialog(String title, final AppEditText appEditText, final String[] items, int selectedPosition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivityCreateLeadBinding.getRoot().getContext());
        builder.setTitle(title)
                .setSingleChoiceItems(items, selectedPosition, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        appEditText.setText(items[which]);

                        // check for product type
                        if (appEditText.getId() == R.id.productTypeEditText) {
                            mActivityCreateLeadBinding.productInterestEditText.setText(null);
                        }
                        if (appEditText.getId() == R.id.emiratesEditText) {
                            mActivityCreateLeadBinding.enameEditText.requestFocus();
                        }

                        if (appEditText.getId() == R.id.productInterestEditText) {
                            if(String.valueOf(appEditText.getText()).equalsIgnoreCase(AppConstants.Values.SUBPROD_INTEREST_ACC)){
                                mActivityCreateLeadBinding.layoutSubproductinterest.setVisibility(View.VISIBLE);
                                mActivityCreateLeadBinding.layoutTier.setVisibility(View.VISIBLE);
                                //mActivityCreateLeadBinding.dynamicLayout.setVisibility(View.VISIBLE);
                                //setDynamicProductAdapter();
                            }else{
                                mActivityCreateLeadBinding.layoutSubproductinterest.setVisibility(View.GONE);
                                mActivityCreateLeadBinding.layoutTier.setVisibility(View.GONE);
                                //mActivityCreateLeadBinding.dynamicLayout.setVisibility(View.GONE);
                            }
                        }

                        dialog.dismiss();
                        isPopDisplayed = false;
                    }
                });
       /* builder.setNegativeButton(Constants.CANCEL, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });*/
        builder.setCancelable(false);

        builder.create().show();
    }

    ProductTypeAdapter mProductTypeAdapter;
    ArrayList<ProductSelection> productSelectionList ;
    private void setDynamicProductAdapter() {
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
     //   mActivityCreateLeadBinding.dynamicLayout.setHasFixedSize(true);

        // use a linear layout manager
       /* LinearLayoutManager mLayoutManager = new LinearLayoutManager(mCreateLeadActivity);
        mActivityCreateLeadBinding.dynamicLayout.setLayoutManager(mLayoutManager);

        productSelectionList = new ArrayList<ProductSelection>();
        // specify an adapter (see also next example)
        mProductTypeAdapter = new ProductTypeAdapter(productSelectionList,this);
        mActivityCreateLeadBinding.dynamicLayout.setAdapter(mProductTypeAdapter);
        mActivityCreateLeadBinding.dynamicLayout.setNestedScrollingEnabled(false);
        productSelectionList.add(new ProductSelection(Constants.ADD_MORE));
        mProductTypeAdapter.notifyDataSetChanged();*/
    }
}
