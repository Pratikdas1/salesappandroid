package com.retailbankapp.fgb.salesapp.dashboard;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.retailbankapp.fgb.salesapp.AppApplication;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.customui.FontCache;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.LeadByCategoryHolder;

import java.util.ArrayList;
import java.util.List;


public class Tab1Fragment extends Fragment {

    private PieChart mChart;
    private List<LeadByCategoryHolder> leadByCategoryHolderList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab1, container, false);
        mChart = (PieChart) view.findViewById(R.id.pieChartView);
        AppApplication appApplication = (AppApplication) getActivity().getApplication();
        FetchLeadResponse fetchLeadResponse = appApplication.getmDashboardData();
        if(fetchLeadResponse != null) {

            if (fetchLeadResponse.getLeadsByStatus() != null && !fetchLeadResponse.getLeadsByStatus().isEmpty()) {
                view.findViewById(R.id.noDataFoundTextView).setVisibility(View.GONE);
                view.findViewById(R.id.chartView).setVisibility(View.VISIBLE);
                leadByCategoryHolderList = new ArrayList<LeadByCategoryHolder>();
                leadByCategoryHolderList.addAll(fetchLeadResponse.getLeadsByStatus());
                intializeChart();
            }
        }
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mChart = null ;
        leadByCategoryHolderList = null ;
    }

    private void intializeChart() {

        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

        mChart.setCenterTextTypeface(FontCache.get(getString(R.string.app_font), getContext()));
        mChart.setCenterText(generateCenterSpannableText());

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(61f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        //mChart.setOnChartValueSelectedListener(this);

        setData();

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(4f);
        //from bottom to up
        l.setYOffset(15f);
        l.setTextSize(15f);
        l.setTypeface(FontCache.get(getString(R.string.app_font), getContext()));

        // entry label styling
        mChart.setEntryLabelColor(Color.WHITE);
        mChart.setEntryLabelTypeface(FontCache.get(getString(R.string.app_font), getContext()));
        mChart.setEntryLabelTextSize(12f);
    }

    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString(Constants.LEAD_STATUS);
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 11, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
       /* s.setSpan(new ForegroundColorSpan(Color.GRAY), 11, s.length() - 12, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 11, s.length() - 12, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 11, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 11, s.length(), 0);*/
        return s;
    }

    private void setData() {

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();
        ArrayList<Integer> colors = new ArrayList<Integer>();
        for ( LeadByCategoryHolder leadByCategoryHolder : leadByCategoryHolderList ) {
            if(leadByCategoryHolder.getCategory().equalsIgnoreCase(AppConstants.LeadStatus.APPROVED)){
                colors.add(ContextCompat.getColor(getContext(),R.color.leadStatusGreen));
                entries.add(new PieEntry(leadByCategoryHolder.getPercentageTotal(), leadByCategoryHolder.getCategory()));
            }else if(leadByCategoryHolder.getCategory().equalsIgnoreCase(AppConstants.LeadStatus.REJECTED)){
                colors.add(ContextCompat.getColor(getContext(),R.color.leadStatusRed));
                entries.add(new PieEntry(leadByCategoryHolder.getPercentageTotal(), leadByCategoryHolder.getCategory()));
            }else if(leadByCategoryHolder.getCategory().equalsIgnoreCase(AppConstants.LeadStatus.PENDING)){
                colors.add(ContextCompat.getColor(getContext(),R.color.leadStatusYellow));
                entries.add(new PieEntry(leadByCategoryHolder.getPercentageTotal(), leadByCategoryHolder.getCategory()));
            }
        }

        //PieDataSet dataSet = new PieDataSet(entries, "Lead Status");
        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

       /* for (int c : ColorTemplate.MATERIAL_COLORS)
            colors.add(c);*/
        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(FontCache.get(getString(R.string.app_font), getContext()));
        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }

}
