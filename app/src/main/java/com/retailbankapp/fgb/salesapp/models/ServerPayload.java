package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by Pratik Das on 1/21/17.
 */

public  enum ServerPayload {
    INSTANCE;
    private String type;
    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
