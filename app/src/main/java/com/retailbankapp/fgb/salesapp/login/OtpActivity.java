
package com.retailbankapp.fgb.salesapp.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.common.VipConfigurator;
import com.retailbankapp.fgb.salesapp.customui.CircleProgressBar;
import com.retailbankapp.fgb.salesapp.databinding.ActivityOtpBinding;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.SendOTPRequest;
import com.retailbankapp.fgb.salesapp.models.ServerPayload;
import com.retailbankapp.fgb.salesapp.models.SessionContext;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPRequest;
import com.retailbankapp.fgb.salesapp.models.VerifyUserResponse;

public class OtpActivity extends AbstractActivity {

    private static final String TAG = OtpActivity.class.getCanonicalName();

    private ActivityOtpBinding mActivityOtpBinding ;
    private int mOtpDuration ;
    private  CircleProgressBar mCircleProgressBar ;
    private  OtpTimer mOtpTimer ;
    private VerifyOTPRequest mVerifyOTPRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_otp);
        mActivityOtpBinding = DataBindingUtil.setContentView(this, R.layout.activity_otp);
        VipConfigurator.INSTANCE.configure(this, new LoginPresenter(), new LoginInteractor());

        VerifyUserResponse verifyUserResponse = null ;
        Intent intent = getIntent();
        if (null != intent) {
            verifyUserResponse = intent.getExtras().getParcelable(Constants.LOGIN_RESPONSE);
        }

        OtpHandler otpHandler = new OtpHandler(mActivityOtpBinding);
        mActivityOtpBinding.setOtpHandler(otpHandler);

        mVerifyOTPRequest = new VerifyOTPRequest();
       // mVerifyOTPRequest.setConvID(verifyUserResponse.getConvID());
        mActivityOtpBinding.setVerifyOTPRequest(mVerifyOTPRequest);

        mCircleProgressBar = (CircleProgressBar) findViewById(R.id.circleProgressBar);

        mOtpDuration = verifyUserResponse.getOtpDuration() ;
        mOtpDuration = mOtpDuration*1000 ;
        mOtpTimer = new OtpTimer(mOtpDuration,1000);
        mOtpTimer.start();
    }

    public void resetTimer(Integer newOtpDuration) {
        Logger.e(TAG,"reset timer");
        Logger.e(TAG,"In resend OTP METHOD");

        mVerifyOTPRequest.setOtp(String.valueOf(newOtpDuration));
        //RESET FIELDS
        mActivityOtpBinding.layoutOtp.setError(null);
        mActivityOtpBinding.verifyButton.setText(Constants.VERIFY);
        mCircleProgressBar.setProgress(0f);
        mOtpTimer = null ;
        mOtpDuration = newOtpDuration ;
        mOtpDuration = mOtpDuration*1000 ;
        mOtpTimer = new OtpTimer(mOtpDuration,1000);
        mOtpTimer.start();
    }

    public void setErrors() {
        mActivityOtpBinding.getOtpHandler().setErros();
    }

    public void regenerateOtp() {
        //mActivityOtpBinding.secondsTextView.setText("0");
        //mCircleProgressBar.setProgress(0f);
        mOtpTimer.cancel();
        mActivityOtpBinding.verifyButton.setText(Constants.REGENERATE_OTP);
    }


    private class OtpTimer extends CountDownTimer{

        private float mProgress = 0 ;
        private float mInterval = 0 ;

        public OtpTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            float divider = (mOtpDuration/1000f) ;
            this.mInterval = 100 / divider;
            mProgress = mInterval ;
          //  Logger.e(TAG,mProgress+" mInterval:"+mInterval);
        }

        @Override
        public void onTick(long l) {
           // Logger.e(TAG,mProgress+"");
            String otpFromPush = ServerPayload.INSTANCE.getValue();
            if(otpFromPush!=null){
                mActivityOtpBinding.otpEditText.setText(otpFromPush);
                verifyOtp(null);
                ServerPayload.INSTANCE.setValue(null);
                cancel();
                return;
            }
            mProgress = mInterval + mProgress ;
           // Logger.e(TAG,mProgress+"");
            mCircleProgressBar.setProgressWithAnimation(mProgress);
            mActivityOtpBinding.secondsTextView.setText(String.valueOf(l/1000));
        }

        @Override
        public void onFinish() {
            mActivityOtpBinding.secondsTextView.setText("0");
            mCircleProgressBar.setProgressWithAnimation(100);
            mActivityOtpBinding.verifyButton.setText(Constants.REGENERATE_OTP);
        }

    }

    public void verifyOtp(View view) {
        LoginInteractor loginInteractor = getInteractor(LoginInteractor.class);
        //If OTP Is expired verify button text will change RESEND_OTP
        if(String.valueOf(mActivityOtpBinding.verifyButton.getText()).equalsIgnoreCase(Constants.REGENERATE_OTP)){
            SendOTPRequest sendOTPRequest = new SendOTPRequest();
            sendOTPRequest.setTxnCode(AppConstants.TxnCodes.LOGIN);
            loginInteractor.resendOTP(sendOTPRequest);
        }else{
            if(mActivityOtpBinding.getOtpHandler().validate()){
                VerifyOTPRequest verifyOTPRequest = mActivityOtpBinding.getVerifyOTPRequest();

                Logger.e(TAG, verifyOTPRequest.toString());
               // verifyOTPRequest.setConvID(SessionContext.INSTANCE.getConvID());
                verifyOTPRequest.setOtp(String.valueOf(mActivityOtpBinding.otpEditText.getText()));// set otp
                verifyOTPRequest.setTxnCode(AppConstants.TxnCodes.LOGIN);
                Logger.i("process  otp verify ", "otp");
                loginInteractor.verifyOTP(verifyOTPRequest);
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mOtpTimer.cancel();
        mActivityOtpBinding = null ;
    }
}


