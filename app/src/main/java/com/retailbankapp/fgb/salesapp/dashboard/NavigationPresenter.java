package com.retailbankapp.fgb.salesapp.dashboard;

import android.content.Intent;

import com.retailbankapp.fgb.salesapp.common.AbstractPresenter;
import com.retailbankapp.fgb.salesapp.lead.CreateLeadActivity;
import com.retailbankapp.fgb.salesapp.lead.MyLeadsActivity;
import com.retailbankapp.fgb.salesapp.login.LoginActivity;
import com.retailbankapp.fgb.salesapp.models.SessionContext;

/**
 * Created by o7548 on 12/15/2016.
 */

public class NavigationPresenter extends AbstractPresenter {

    public void logout() {
        SessionContext.INSTANCE.clear();
        Intent intent = new Intent(getmContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getmContext().startActivity(intent);
        getmContext().finish();
    }

    public void startCreateLeadScreen() {
        Intent intent = new Intent(getmContext(), CreateLeadActivity.class);
        getmContext().startActivity(intent);
    }

    public void startMyLeadScreen() {
        Intent intent = new Intent(getmContext(), MyLeadsActivity.class);
        getmContext().startActivity(intent);
    }
    public void setErrors(String response) {
        getmContext().showError(response);
    }
    public void loadDashboard() {
        ((NavigationActivity)getmContext()).loadDashboard();
    }
}
