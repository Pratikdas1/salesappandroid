package com.retailbankapp.fgb.salesapp;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPResponse;

/**
 * Created by o7548 on 11/29/2016.
 */

public class AppApplication extends Application {

    private VerifyOTPResponse mVerifyOTPResponse ;
    private FetchLeadResponse mMyLeadData;
    private FetchLeadResponse mDashboardData ;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        /*final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);*/
        //Fabric.with(this,new Crashlytics());
    }



    public VerifyOTPResponse getmVerifyOTPResponse() {
        return mVerifyOTPResponse;
    }

    public void setmVerifyOTPResponse(VerifyOTPResponse mVerifyOTPResponse) {
        this.mVerifyOTPResponse = mVerifyOTPResponse;
    }

    public FetchLeadResponse getmMyLeadData() {
        return mMyLeadData;
    }

    /**
     * Dashboard data for all three tabs
     * @param mMyLeadData
     */
    public void setmMyLeadData(FetchLeadResponse mMyLeadData) {
        this.mMyLeadData = mMyLeadData;
    }

    public FetchLeadResponse getmDashboardData() {
        return mDashboardData;
    }

    public void setmDashboardData(FetchLeadResponse mDashboardData) {
        this.mDashboardData = mDashboardData;
    }
}
