package com.retailbankapp.fgb.salesapp.login;

import android.content.Intent;
import android.databinding.BindingAdapter;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;

import com.retailbankapp.fgb.salesapp.AppApplication;
import com.retailbankapp.fgb.salesapp.common.AbstractPresenter;
import com.retailbankapp.fgb.salesapp.dashboard.NavigationActivity;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.User;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPResponse;
import com.retailbankapp.fgb.salesapp.models.VerifyUserResponse;

/**
 * Created by fgb on 11/21/16.
 */

public class LoginPresenter extends AbstractPresenter {


    public void setErrors(String msg) {
        getmContext().showError(msg);
    }

    public void redirectLogin() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getmContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getmContext().startActivity(intent);
                getmContext().finish();
            }
        },3000);


    }

    public void redirectOTPActivity(VerifyUserResponse response) {
        Intent intent = new Intent(getmContext(), OtpActivity.class);
        getmContext().hideProgress();
        intent.putExtra(Constants.LOGIN_RESPONSE, response);
        getmContext().startActivity(intent);
        getmContext().finish();
    }

    public void redirectDashboard(VerifyOTPResponse response) {
        // Set global data .
        AppApplication appApplication = (AppApplication) getmContext().getApplication();
        appApplication.setmVerifyOTPResponse(response);
        Intent intent = new Intent(getmContext(),NavigationActivity.class);
        getmContext().startActivity(intent);
        getmContext().finish();
    }

    public void restartTimer(Integer otpDuration) {
        ((OtpActivity) getmContext()).resetTimer(otpDuration);
    }

    public void redirectDiagnosisScreen(){
        Intent intent = new Intent(getmContext(),DiagnosisActivity.class);
        getmContext().startActivity(intent);
    }

    public void regenerateOtp() {
        ((OtpActivity) getmContext()).regenerateOtp();
    }
}
