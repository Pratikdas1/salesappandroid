package com.retailbankapp.fgb.salesapp.stores;

import android.util.Base64;

import com.google.gson.Gson;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.helpers.PropertyReader;
import com.retailbankapp.fgb.salesapp.models.JsonResponse;
import com.retailbankapp.fgb.salesapp.models.LogoutResponse;
import com.retailbankapp.fgb.salesapp.models.PingResponse;
import com.retailbankapp.fgb.salesapp.models.RegisterDeviceRequest;
import com.retailbankapp.fgb.salesapp.models.RegisterDeviceResponse;
import com.retailbankapp.fgb.salesapp.models.SendOTPRequest;
import com.retailbankapp.fgb.salesapp.models.SendOTPResponse;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.models.SettingsResponse;
import com.retailbankapp.fgb.salesapp.models.UpdateRegisterDeviceRequest;
import com.retailbankapp.fgb.salesapp.models.UpdateRegisterDeviceResponse;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPRequest;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPResponse;
import com.retailbankapp.fgb.salesapp.models.VerifyUserRequest;
import com.retailbankapp.fgb.salesapp.models.VerifyUserResponse;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pratik Das on 11/22/16.
 */

public enum UserAPIStore implements UserStore {
    INSTANCE;

    private ServiceBuilder serviceBuilder = ServiceBuilder.INSTANCE;
    //private ServiceInvoker service = serviceBuilder.create(ServiceInvoker.class);
    private final Gson gson = new Gson();



    @Override
    public void verifyUser(final VerifyUserRequest request, final ResponseListener<VerifyUserResponse> serviceResponse) {
        serviceBuilder.invoke(AppConstants.ServiceURLs.VERIFY_USER,request, serviceResponse, VerifyUserResponse.class);

    }

    @Override
    public void verifyOTP(VerifyOTPRequest request, final ResponseListener<VerifyOTPResponse> serviceResponse) {

        serviceBuilder.invoke(AppConstants.ServiceURLs.VERIFY_OTP, request,serviceResponse, VerifyOTPResponse.class);


    }

    @Override
    public void sendOTP(SendOTPRequest request, final ResponseListener<SendOTPResponse> serviceResponse) {

        serviceBuilder.invoke(AppConstants.ServiceURLs.RESEND_OTP,request, serviceResponse, SendOTPResponse.class);

    }

    @Override
    public void registerDevice(RegisterDeviceRequest request, final ResponseListener<RegisterDeviceResponse> serviceResponse) {

        serviceBuilder.invoke(AppConstants.ServiceURLs.REGISTER_DEVICE,request, serviceResponse, RegisterDeviceResponse.class);


    }

    @Override
    public void logout(ResponseListener<LogoutResponse> responseListener) {
        serviceBuilder.invoke(AppConstants.ServiceURLs.LOGOUT,null, responseListener, LogoutResponse.class);

    }

    @Override
    public void updateDeviceRegistration(UpdateRegisterDeviceRequest request, ResponseListener<UpdateRegisterDeviceResponse> serviceResponse) {
        serviceBuilder.invoke(AppConstants.ServiceURLs.UPDATE_REGISTER_DEVICE,request, serviceResponse, UpdateRegisterDeviceResponse.class);

    }

    @Override
    public void fetchSettings(final ResponseListener<SettingsResponse> serviceResponse) {
        serviceBuilder.invoke(AppConstants.ServiceURLs.FETCH_SETTINGS,null, serviceResponse, SettingsResponse.class);
    }

    @Override
    public void pingServer(final ResponseListener<PingResponse> serviceResponse) {
        serviceBuilder.invoke(AppConstants.ServiceURLs.PING,null, serviceResponse, PingResponse.class);
    }

}
