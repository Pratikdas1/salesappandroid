package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by fgb on 11/23/16.
 */

public class AppError {
    private String code;
    private String desc;

    public AppError(String code, String description) {
        this.code = code;
        this.desc = description;
    }

    public static AppError getDefaultError(){
        AppError appError = new AppError("A001","System is not available.");
        return  appError;
    }

    public static AppError getAccessDeniedError(){
        AppError appError = new AppError("ACCESS_DENIED","Access Denied.");
        return  appError;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return desc;
    }

    public void setDescription(String description) {
        this.desc = description;
    }
}
