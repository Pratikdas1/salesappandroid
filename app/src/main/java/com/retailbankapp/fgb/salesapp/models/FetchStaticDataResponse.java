package com.retailbankapp.fgb.salesapp.models;

import java.util.List;

/**
 * Created by fgb on 11/24/16.
 */

public final class FetchStaticDataResponse extends AbstractResponse {
   private List<StaticData> staticData;

   public List<StaticData> getStaticData() {
      return staticData;
   }

   public void setStaticData(List<StaticData> staticData) {
      this.staticData = staticData;
   }
}
