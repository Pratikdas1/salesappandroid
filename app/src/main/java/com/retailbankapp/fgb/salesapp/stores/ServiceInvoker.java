package com.retailbankapp.fgb.salesapp.stores;

import com.retailbankapp.fgb.salesapp.models.JsonResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Pratik Das on 11/23/16.
 */

public interface ServiceInvoker {
    @FormUrlEncoded
    @POST("service")
    Call<String> invoke(@Field("request") String request);
}
