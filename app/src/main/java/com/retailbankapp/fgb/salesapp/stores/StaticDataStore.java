package com.retailbankapp.fgb.salesapp.stores;

import com.retailbankapp.fgb.salesapp.models.FetchLeadRequest;
import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.FetchStaticDataRequest;
import com.retailbankapp.fgb.salesapp.models.FetchStaticDataResponse;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;

/**
 * Created by fgb on 12/5/16.
 */

public interface StaticDataStore {

     void fetchStaticData(FetchStaticDataRequest request, ResponseListener<FetchStaticDataResponse> serviceResponse);
}
