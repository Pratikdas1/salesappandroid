package com.retailbankapp.fgb.salesapp.stores.stubs;

import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.models.AppError;
import com.retailbankapp.fgb.salesapp.models.CreateLeadRequest;
import com.retailbankapp.fgb.salesapp.models.CreateLeadResponse;
import com.retailbankapp.fgb.salesapp.models.FetchLeadRequest;
import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.Lead;
import com.retailbankapp.fgb.salesapp.models.LeadByCategoryHolder;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.models.SearchFilter;
import com.retailbankapp.fgb.salesapp.models.UpdateLeadRequest;
import com.retailbankapp.fgb.salesapp.models.UpdateLeadResponse;
import com.retailbankapp.fgb.salesapp.stores.LeadStore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fgb on 12/5/16.
 */

public enum LeadStoreStub implements LeadStore {
    INSTANCE;


    @Override
    public void createLead(CreateLeadRequest createLeadRequest, ResponseListener<CreateLeadResponse> serviceResponse) {
        CreateLeadResponse createLeadResponse = new CreateLeadResponse();
        if(createLeadRequest.getOtp().equalsIgnoreCase("123456")){
            createLeadResponse.setLeadRef("L-47576");
            createLeadResponse.setResult("success");
            serviceResponse.onSuccess(createLeadResponse);
        }else{
            createLeadResponse.setError(new AppError(AppConstants.ErrorCodes.OTP_ATTEMPT_EXCEEDED,"Invalid OTP"));
            serviceResponse.onFailure(createLeadResponse);
        }

    }

    @Override
    public void updateLead(CreateLeadRequest updateLeadRequest, ResponseListener<CreateLeadResponse> serviceResponse) {
        CreateLeadResponse updateLeadResponse = new CreateLeadResponse();

        //updateLeadResponse.setLeadRef("leadref");
        updateLeadResponse.setResult("success");
        serviceResponse.onSuccess(updateLeadResponse);
    }
    private int assignedLeadCount = 143 ;
    private  int submittedLeadCount = 274 ;
    @Override
    public void fetchLeads(FetchLeadRequest request, ResponseListener<FetchLeadResponse> serviceResponse) {
        FetchLeadResponse fetchLeadResponse = new FetchLeadResponse();
        String[] status = {"Approved","Rejected","Pending"};
        String[] productType = {"Account","Loan","Personal","Product Bundle","Credit Cards",""};
        List<Lead> leads = new ArrayList<>();
        List<LeadByCategoryHolder> leadsByStatus = new ArrayList<LeadByCategoryHolder>();
        List<LeadByCategoryHolder> leadsByProduct = new ArrayList<LeadByCategoryHolder>();
        List<LeadByCategoryHolder> leadsByAssigneeStatus = new ArrayList<LeadByCategoryHolder>();

        LeadByCategoryHolder leadByCategoryHolder = null;
        int i = 0 ;
        Lead lead = null;

        SearchFilter searchFilter = request.getSearchFilter();
        if(searchFilter != null && searchFilter.getPageNumber() != null){
            int pageNumber = searchFilter.getPageNumber();
         //   if(pageNumber >=1){

                ArrayList<Lead> assignedArrayList = new ArrayList<>();
                leadByCategoryHolder = new LeadByCategoryHolder();
                leadByCategoryHolder.setCategory("assigned");
                int max =  pageNumber*15 + 15 ;
                for( i = pageNumber*15 ; i < max && i <= assignedLeadCount ; i++){
                    lead = new Lead();
                    lead.setFirstName("Mahesh"+i);
                    lead.setLastName("Jadhav"+i);
                    lead.setMobileNumber("056478569"+i);
                    lead.setLeadRef("L-596968"+i);
                    lead.setCreateDate(i+" Days ago");
                    lead.setProductType(productType[i%6]);
                    lead.setMonthlySalary("3044"+i);
                    lead.setDateOfBirth("25.02.19"+i);
                    lead.setEmail("kedar.jadhav"+i+"@gmail.com");
                    lead.setNationality("India");
                    lead.setSalesComments("Customer is interest Gold card . Please do the needful");
                    lead.setPhilosophy("Islamic");
                    lead.setGender("Male");
                    lead.setEmirate("Sharjah");
                    lead.setEmployerName("FGB");
                    lead.setPassportNumber("G36998"+i);
                    lead.setEmiratesID("5286529895"+i);
                    lead.setProductInterest("Personal Loan");
                    lead.setLeadApplicationStatus(status[i%3]);
                    assignedArrayList.add(lead);
                }
                leadByCategoryHolder.setLeads(assignedArrayList);
                leadsByAssigneeStatus.add(leadByCategoryHolder);
                i = 0 ;

                ArrayList<Lead> createdArrayList = new ArrayList<>();
                leadByCategoryHolder = new LeadByCategoryHolder();
                leadByCategoryHolder.setCategory("submitted");
                max =  pageNumber*15 + 15 ;
                for( i = pageNumber*15 ; i < max && i <= submittedLeadCount ; i++){
                    lead = new Lead();
                    lead.setFirstName("Rohit"+i);
                    lead.setLastName("Sharmna"+i);
                    lead.setMobileNumber("037487598"+i);
                    lead.setLeadRef("L-94097"+i);
                    lead.setCreateDate(i+" Days ago");
                    lead.setProductType(productType[i%6]);
                    lead.setMonthlySalary("3044"+i);
                    lead.setDateOfBirth("25.02.19"+i);
                    lead.setEmail("shaikh.anwar"+i+"@gmail.com");
                    lead.setNationality("UAE");
                    lead.setPhilosophy("Conventional");
                    lead.setGender("male");
                    lead.setEmployerName("FGB");
                    lead.setEmirate("Abu Dhabi");
                    lead.setPassportNumber("J998"+i);
                    lead.setEmiratesID("56895456"+i);
                    lead.setProductInterest("Home Loan");
                    lead.setSalesComments("Customer is interest Platinum card . Please do the needful");
                    lead.setLeadApplicationStatus(status[i%3]);
                    createdArrayList.add(lead);
                }
                leadByCategoryHolder.setLeads(createdArrayList);
                leadsByAssigneeStatus.add(leadByCategoryHolder);
                fetchLeadResponse.setLeadsByAssigneeStatus(leadsByAssigneeStatus);

                i = 0 ;
                String[] productTypes = {"Loans","Credit Card","Accelerator","Others"};
                Float[] productTypeValues = {24f,65f,19f,42f};


                Float[] productStatusValues = {60f,20.22f,19.78f};


                String prevProduct = null;
                String prevStatus = null;

                for ( String product : productTypes) {
                    leadByCategoryHolder = new LeadByCategoryHolder();
                    leadByCategoryHolder.setCategory(product);
                    leadByCategoryHolder.setPercentageTotal(productTypeValues[i]);
                    leadsByProduct.add(leadByCategoryHolder);
                    i += 1;
                }

                i = 0 ;
                for ( String appStatus : status) {
                    leadByCategoryHolder = new LeadByCategoryHolder();
                    leadByCategoryHolder.setCategory(appStatus);
                    leadByCategoryHolder.setPercentageTotal(productStatusValues[i]);
                    leadsByStatus.add(leadByCategoryHolder);
                    i += 1;
                }

                for( i = 0; i < 100 ; ++i) {
                    lead = new Lead();
                    lead.setLeadRef("LRef" + i);
                    lead.setLastName("Lname"+i);
                    lead.setFirstName("Fname"+i);
                    lead.setMobileNumber("05447859"+i);
                    lead.setEmiratesID("56895456"+i);
                    lead.setProductType(productType[i%5]);
                    lead.setLeadApplicationStatus(status[i%3]);
                    leads.add(lead);
                }
                fetchLeadResponse.setLeads(leads);

                fetchLeadResponse.setLeadsByProduct(leadsByProduct);
                fetchLeadResponse.setLeadsByStatus(leadsByStatus);
                fetchLeadResponse.setResult("success");
                serviceResponse.onSuccess(fetchLeadResponse);
            }
       // }

        /*ArrayList<Lead> assignedArrayList = new ArrayList<>();
        leadByCategoryHolder = new LeadByCategoryHolder();
        leadByCategoryHolder.setCategory("assigned");
        for( i = 0 ; i < 15 ; i++){
            lead = new Lead();
            lead.setFirstName("first"+i);
            lead.setLastName("last"+i);
            lead.setMobileNumber("056478569"+i);
            lead.setLeadRef("L-596968"+i);
            lead.setCreateDate("2 Days ago");
            lead.setProductType(productType[i%6]);
            lead.setMonthlySalary("3044"+i);
            lead.setDateOfBirth("25.02.19"+i);
            lead.setEmail("shaikh.anwar"+i+"@gmail.com");
            lead.setNationality("India");
            lead.setSalesComments("Customer is interest Gold card . Please do the needful");
            lead.setPhilosophy("Islamic");
            lead.setGender("female");
            lead.setEmirate("Dubai");
            lead.setEmployerName("CTS");
            lead.setPassportNumber("G36998"+i);
            lead.setEmiratesID("646989"+i);
            lead.setProductInterest("Credit Card");
            lead.setLeadApplicationStatus(status[i%3]);
            assignedArrayList.add(lead);
        }
        leadByCategoryHolder.setLeads(assignedArrayList);
        leadsByAssigneeStatus.add(leadByCategoryHolder);
        i = 0 ;

        ArrayList<Lead> createdArrayList = new ArrayList<>();
        leadByCategoryHolder = new LeadByCategoryHolder();
        leadByCategoryHolder.setCategory("submitted");
        for( i = 0 ; i < 15 ; i++){
            lead = new Lead();
            lead.setFirstName("Anwar"+i);
            lead.setLastName("Shaikh"+i);
            lead.setMobileNumber("05447859"+i);
            lead.setLeadRef("L-76875969"+i);
            lead.setCreateDate("3 Days ago");
            lead.setProductType(productType[i%6]);
            lead.setMonthlySalary("3044"+i);
            lead.setDateOfBirth("25.02.19"+i);
            lead.setEmail("shaikh.anwar"+i+"@gmail.com");
            lead.setNationality("UAE");
            lead.setPhilosophy("Conventional");
            lead.setGender("male");
            lead.setEmployerName("FGB");
            lead.setEmirate("Abu Dhabi");
            lead.setPassportNumber("J998"+i);
            lead.setEmiratesID("56895456"+i);
            lead.setProductInterest("Gold Card");
            lead.setSalesComments("Customer is interest Platinum card . Please do the needful");
            lead.setLeadApplicationStatus(status[i%3]);
            createdArrayList.add(lead);
        }
        leadByCategoryHolder.setLeads(createdArrayList);
        leadsByAssigneeStatus.add(leadByCategoryHolder);
        fetchLeadResponse.setLeadsByAssigneeStatus(leadsByAssigneeStatus);*/


        /*fetchLeadResponse.setLeads(leads);

        fetchLeadResponse.setLeadsByProduct(leadsByProduct);
        fetchLeadResponse.setLeadsByStatus(leadsByStatus);

        fetchLeadResponse.setResult("success");
        serviceResponse.onSuccess(fetchLeadResponse);*/
    }
}
