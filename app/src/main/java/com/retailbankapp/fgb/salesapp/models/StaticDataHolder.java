package com.retailbankapp.fgb.salesapp.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pratik Das on 12/15/16.
 */

/*StaticDataHolder{
     [  {"staticDataType" : "Country","staticDataList": [{"code":"DE","desc":"Germany"}, {"code":"IN","desc":"India"}]},
        {"staticDataType" : "Emirates","staticDataList": [{"code":"DU","desc":"Dubai"}, {"code":"AD","desc":"Abu Dhabi"}]},
        {"staticDataType" : "ProductType","staticDataList": [{"code":"PBN","desc":"ProductBundle",
        "subtypes" : [
           {
              "staticDataType":"ProductInterest", "staticDataList": [{"code":"ACL", "desc" : "Accelerator"}]
           }
        ] }, {"code":"CD","desc":"Cards"}]},
        Philosophy

       ]
        }*/


public final class StaticDataHolder {
    private List<StaticDataContainer> staticData;
    private List<StaticData> staticDataList;

    public List<StaticDataContainer> getStaticData() {
        return staticData;
    }

    public List<StaticData> getStaticDataList() {
        return staticDataList;
    }

    public void setStaticDataList(List<StaticData> staticDataList) {
        this.staticDataList = staticDataList;
    }

    public void setStaticData(List<StaticDataContainer> staticData) {
        this.staticData = staticData;
    }

    public List<StaticData> getStaticData(String staticDataType){
        List<StaticData> stDataList = new ArrayList<>();
        for (StaticData st : staticDataList
             ) {
            if(staticDataType.equalsIgnoreCase(st.getEntityName())){
                stDataList.add(st);
            }
        }

        return stDataList;
    }
    public List<StaticData> getStaticData(String staticDataType,final String staticDataDesc){
        List<StaticData> stDataList = null;
        for (StaticData st : staticDataList
                ) {
            if(staticDataType.equalsIgnoreCase(st.getEntityName()) && staticDataDesc.equalsIgnoreCase(st.getDescription())){
                String staticDataCode = st.getCode();
                stDataList = getStaticData(String.format("%s.%s",staticDataType,staticDataCode));
                break;
            }
        }

        return stDataList;
    }

    public String getStaticDataDesc(String staticDataType,final String staticDataCode){
        List<StaticData> stDataList = null;
        for (StaticData st : staticDataList
                ) {
            if(staticDataType.equalsIgnoreCase(st.getEntityName()) && staticDataCode.equalsIgnoreCase(st.getCode())){
                return st.getDescription();
            }
        }
        return null;
    }

}
