package com.retailbankapp.fgb.salesapp.dashboard;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.retailbankapp.fgb.salesapp.BuildConfig;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;

public class AboutUsActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppTextView versionTextView =  (AppTextView)findViewById(R.id.versionTextView);
        versionTextView.setText(String.valueOf(BuildConfig.VERSION_NAME+BuildConfig.VERSION_CODE));
    }

    public void displayLicenseDialog(View view){
        //Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        WebView wv = new WebView(this);
        //wv.setHorizontalScrollBarEnabled(false);
        wv.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        wv.loadUrl("file:///android_asset/licenses.html");
        alertDialogBuilder.setView(wv);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setTitle(this.getString(R.string.notices))
             //   .setIcon(R.drawable.ic_check_circle_blue_24dp)
                .setPositiveButton(this.getString(R.string.msg_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();

                            }
                        });
        alertDialogBuilder.create().show();
    }
}
