package com.retailbankapp.fgb.salesapp.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fgb on 11/24/16.
 */

public final class FetchLeadResponse extends  AbstractResponse{
    private List<Lead> leads;  // first 100 records

    private List<LeadByCategoryHolder> leadsByProduct;
    private List<LeadByCategoryHolder> leadsByStatus;
    private List<LeadByCategoryHolder> leadsByAssigneeStatus;

    public void setLeads(List<Lead> leads) {
        this.leads = leads;
    }

    public List<LeadByCategoryHolder> getLeadsByProduct() {
        return leadsByProduct;
    }

    public List<LeadByCategoryHolder> getLeadsByAssigneeStatus() {
        return leadsByAssigneeStatus;
    }

    public void setLeadsByAssigneeStatus(List<LeadByCategoryHolder> leadsByAssigneeStatus) {
        this.leadsByAssigneeStatus = leadsByAssigneeStatus;
    }

    public void setLeadsByProduct(List<LeadByCategoryHolder> leadsByProduct) {
        this.leadsByProduct = leadsByProduct;
    }

    public List<LeadByCategoryHolder> getLeadsByStatus() {
        return leadsByStatus;
    }

    public void setLeadsByStatus(List<LeadByCategoryHolder> leadsByStatus) {
        this.leadsByStatus = leadsByStatus;
    }

    public void addLead(final Lead lead){
        if(leads==null){
            leads = new ArrayList<>();
        }
        leads.add(lead);
    }

    public List<Lead> getLeads() {
        return leads;
    }
}
