package com.retailbankapp.fgb.salesapp.lead;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractPresenter;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;
import com.retailbankapp.fgb.salesapp.dashboard.NavigationActivity;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.login.LoginActivity;
import com.retailbankapp.fgb.salesapp.login.OtpActivity;
import com.retailbankapp.fgb.salesapp.models.AbstractResponse;
import com.retailbankapp.fgb.salesapp.models.CreateLeadRequest;
import com.retailbankapp.fgb.salesapp.models.CreateLeadResponse;
import com.retailbankapp.fgb.salesapp.models.Document;
import com.retailbankapp.fgb.salesapp.models.Lead;
import com.retailbankapp.fgb.salesapp.models.SendOTPResponse;

/**
 * Created by O7548 on 12/8/2016.
 */

public class LeadPresenter extends AbstractPresenter {

    public void saveLead(Lead lead) {
       // Intent dataIntent = new Intent(getmContext(), UploadDocActivity.class);
        CreateLeadRequest createLeadRequest = new CreateLeadRequest();
        createLeadRequest.setLead(lead);
        Intent dataIntent = new Intent(getmContext(), LeadOtpActivity.class);
        dataIntent.putExtra(Constants.LEAD_DETAILS, createLeadRequest);
        getmContext().startActivity(dataIntent);
      //  getmContext().finish();
    }

    public void launchDashboard(String firstName, CreateLeadResponse response) {

        Intent dataIntent = new Intent(getmContext(), LeadCreatedActivity.class);
        dataIntent.putExtra(Constants.FIRST_NAME, firstName);
        dataIntent.putExtra(Constants.LEAD_REF, response.getLeadRef());
        getmContext().startActivity(dataIntent);
        getmContext().finish();


        // get prompts.xml view
       /* final LinearLayout promptView = (LinearLayout) getmContext().getLayoutInflater()
                .inflate(R.layout.fragment_lead_created, null);
        ((AppTextView) promptView.findViewById(R.id.leadIdTextView)).setText("L-213484");
        ((AppTextView) promptView.findViewById(R.id.firstNameTextView)).setText(firstName);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getmContext());
        alertDialogBuilder.setView(promptView);

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setTitle(getmContext().getString(R.string.msg_lead_created))
                .setIcon(R.drawable.ic_check_circle_blue_24dp)
                .setPositiveButton(getmContext().getString(R.string.msg_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                Intent intent = new Intent(getmContext(), CreateLeadActivity.class);
                              //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                getmContext().startActivity(intent);
                                getmContext().finish();
                            }
                        });
        alertDialogBuilder.create().show();*/
    }

    public void setErrors(String response) {
         getmContext().showError(response);
       // getmContext().hideProgress();
    }

    public void saveDocument(Document document, Lead mLead) {
        CreateLeadRequest createLeadRequest = new CreateLeadRequest();
        createLeadRequest.setDocument(document);
        createLeadRequest.setLead(mLead);
        Intent dataIntent = new Intent(getmContext(), LeadOtpActivity.class);
        dataIntent.putExtra(Constants.LEAD_DETAILS, createLeadRequest);
        getmContext().startActivity(dataIntent);
    }

    public void startTimer(SendOTPResponse response) {
        ((LeadOtpActivity) getmContext()).startTimer(response);
    }

    public void launchMyLeads() {
        Toast.makeText(getmContext(), "Lead is updated", Toast.LENGTH_LONG).show();
        Intent dataIntent = new Intent(getmContext(), MyLeadsActivity.class);
        getmContext().startActivity(dataIntent);
    }

    public void loadMyLeadsData() {
        ((MyLeadsActivity)getmContext()).setupAdapter();
    }

    public void showLeadDetails(Lead lead, boolean isEditable) {
        Intent intent = new Intent( getmContext(), LeadDetailsActivity.class);
        intent.putExtra(Constants.LEAD_DETAILS, lead);
        intent.putExtra(Constants.EDITABLE,isEditable);
        //Toast.makeText(getContext(), "Submitted Item Clicked:"+position, Toast.LENGTH_SHORT).show();
        getmContext().startActivity(intent);
    }

    public void redirectLogin() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getmContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getmContext().startActivity(intent);
                getmContext().finish();
            }
        },3000);
    }

    public void regenerateOtp() {
        ((LeadOtpActivity) getmContext()).regenerateOtp();
    }

    public void finishOtpScreen() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ((LeadOtpActivity) getmContext()).finish();
            }
        },2000);
    }
}
