package com.retailbankapp.fgb.salesapp.models;

import com.retailbankapp.fgb.salesapp.helpers.AppConstants;

/**
 * Created by fgb on 12/25/16.
 */

public final class SearchFilter {
    private String leadRef;
    private String leadFirstName;
    private String leadLastName;
    private Integer pageNumber;
    private Integer numberOfLeadsPerPage = AppConstants.Values.LEADS_PER_PAGE;
    private Integer numberOfDaysPreceding;
    private String keyword;

    public Integer getNumberOfDaysPreceding() {
        return numberOfDaysPreceding;
    }

    public void setNumberOfDaysPreceding(Integer numberOfDaysPreceding) {
        this.numberOfDaysPreceding = numberOfDaysPreceding;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLeadRef() {
        return leadRef;
    }

    public void setLeadRef(String leadRef) {
        this.leadRef = leadRef;
    }

    public String getLeadFirstName() {
        return leadFirstName;
    }

    public void setLeadFirstName(String leadFirstName) {
        this.leadFirstName = leadFirstName;
    }

    public String getLeadLastName() {
        return leadLastName;
    }

    public void setLeadLastName(String leadLastName) {
        this.leadLastName = leadLastName;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getNumberOfLeadsPerPage() {
        return numberOfLeadsPerPage;
    }

    public void setNumberOfLeadsPerPage(Integer numberOfLeadsPerPage) {
        this.numberOfLeadsPerPage = numberOfLeadsPerPage;
    }
}
