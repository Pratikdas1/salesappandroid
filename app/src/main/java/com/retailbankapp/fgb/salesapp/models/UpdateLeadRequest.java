package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by fgb on 11/24/16.
 */

public class UpdateLeadRequest {
    private Lead lead;

    public Lead getLead() {
        return lead;
    }

    public void setLead(Lead lead) {
        this.lead = lead;
    }
}
