package com.retailbankapp.fgb.salesapp.lead;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.retailbankapp.fgb.salesapp.BR;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.customui.AppButton;
import com.retailbankapp.fgb.salesapp.customui.AppEditText;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.StaticData;
import com.retailbankapp.fgb.salesapp.models.ProductSelection;

import java.util.List;

/**
 * Created by o7548 on 12/15/2016.
 */


public class ProductTypeAdapter extends RecyclerView.Adapter<ProductTypeAdapter.BindingHolder> {
    private List<ProductSelection> mProductList;
    private CreateLeadHandler createLeadHandler;

    public ProductTypeAdapter(List<ProductSelection> recyclerUsers, CreateLeadHandler createLeadHandler) {
        this.mProductList = recyclerUsers;
        this.createLeadHandler = createLeadHandler;
    }

    public class BindingHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnTouchListener {
        private ViewDataBinding binding;

        public BindingHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
            binding.getRoot().findViewById(R.id.actionButton).setOnClickListener(this);
            binding.getRoot().findViewById(R.id.productType).setOnTouchListener(this);
            binding.getRoot().findViewById(R.id.productInterest).setOnTouchListener(this);
            binding.getRoot().findViewById(R.id.philosophy).setOnTouchListener(this);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == mProductList.size() - 1) {
                ProductTypeAdapter.this.add(new ProductSelection(Constants.DELETE));
            } else {
                ProductTypeAdapter.this.remove(getAdapterPosition());
            }
            //  documentLisener.delete(getAdapterPosition());
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                switch (view.getId()) {
                  /*  case R.id.productType:
                        createLeadHandler.populateDropDown((AppEditText) view, Constants.SELECT_PRODUCT, StaticData.getProductType());
                        break;
                    case R.id.productInterest:
                        String selectedProduct = String.valueOf(((AppEditText)binding.getRoot().findViewById(R.id.productType)).getText());
                        if (selectedProduct.equalsIgnoreCase(Constants.AUTO)) {
                            createLeadHandler. populateDropDown((AppEditText) view, Constants.SELECT_PRODUCT_INTEREST, StaticData.getAutoProductNames());
                        } else if (selectedProduct.equalsIgnoreCase(Constants.CREDIT_CARD)) {
                            createLeadHandler.populateDropDown((AppEditText) view, Constants.SELECT_PRODUCT_INTEREST, StaticData.getCcProductNames());
                        } else if (selectedProduct.equalsIgnoreCase(Constants.PERSONAL)) {
                            createLeadHandler.populateDropDown((AppEditText) view, Constants.SELECT_PRODUCT_INTEREST, StaticData.getPersonalProductNames());
                        } else {
                            createLeadHandler.populateDropDown((AppEditText) view, Constants.SELECT_PRODUCT_INTEREST, StaticData.getPersonalBundleProductNames());
                        }
                        break;
                    case R.id.philosophy:
                        createLeadHandler.populateDropDown((AppEditText) view, Constants.SELECT_PHILOSOPHY, StaticData.getPhilosophy());
                        break;*/
                }
            }
            return false;
        }
    }

    public void add(ProductSelection item) {
        mProductList.add(mProductList.size() - 1, item);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        mProductList.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.producttype_layout, parent, false);
        BindingHolder holder = new BindingHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        final ProductSelection productSelection = mProductList.get(position);
        if (productSelection.getAction().equalsIgnoreCase("delete")) {
            ((AppButton) holder.getBinding().getRoot().findViewById(R.id.actionButton))
                    .setTextColor(ContextCompat.getColor(holder.getBinding().getRoot().getContext(), R.color.redColor));
        } else {
            ((AppButton) holder.getBinding().getRoot().findViewById(R.id.actionButton))
                    .setTextColor(ContextCompat.getColor(holder.getBinding().getRoot().getContext(), R.color.colorPrimaryDark));
        }
        holder.getBinding().setVariable(BR.productSelection, productSelection);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }
}