package com.retailbankapp.fgb.salesapp.lead;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;

import com.retailbankapp.fgb.salesapp.AppApplication;
import com.retailbankapp.fgb.salesapp.common.AbstractInteractor;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.AppError;
import com.retailbankapp.fgb.salesapp.models.CreateLeadRequest;
import com.retailbankapp.fgb.salesapp.models.CreateLeadResponse;
import com.retailbankapp.fgb.salesapp.models.Document;
import com.retailbankapp.fgb.salesapp.models.FetchLeadRequest;
import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.Lead;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.models.SearchFilter;
import com.retailbankapp.fgb.salesapp.models.SendOTPRequest;
import com.retailbankapp.fgb.salesapp.models.SendOTPResponse;
import com.retailbankapp.fgb.salesapp.stores.LeadStore;
import com.retailbankapp.fgb.salesapp.stores.StoreFactory;

import java.io.ByteArrayOutputStream;

/**
 * Created by O7548 on 12/8/2016.
 */

public class LeadInteractor extends AbstractInteractor {

    private static boolean isDecoded = false;
    private LeadStore mLeadStore;

    public LeadInteractor() {
        this.mLeadStore = StoreFactory.getStore().getLeadStore();
    }

    public void saveLead(Lead lead) {
        LeadPresenter leadPresenter = (LeadPresenter) getPresenter();
        leadPresenter.saveLead(lead);
    }

    public void createLeadData(final CreateLeadRequest createLeadRequest) {
        final LeadPresenter leadPresenter = (LeadPresenter) getPresenter();

        if (!isDecoded) {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    leadPresenter.showProgress(Constants.PROCESSING);
                }

                @Override
                protected String doInBackground(Void... voids) {
                    Document document = createLeadRequest.getDocument();
                    if(document != null && document.getContents() != null){
                        Bitmap bm = BitmapFactory.decodeFile(document.getContents());
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                        byte[] byteArrayImage = baos.toByteArray();
                        String encodedImageString = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                        return encodedImageString;
                    }
                    return  null ;
                }

                @Override
                protected void onPostExecute(String bitmap) {
                    super.onPostExecute(bitmap);
                    leadPresenter.hideProgress();
                    // decode image only once
                    if(bitmap != null){
                        isDecoded = true ;
                        Document document = createLeadRequest.getDocument();
                        document.setContents(bitmap);
                        createLeadRequest.setDocument(document);
                    }
                    createLead(createLeadRequest);
                }
            }.execute();
        } else {
            createLead(createLeadRequest);
        }
    }

    public void createLead(final CreateLeadRequest createLeadRequest) {
        final LeadPresenter leadPresenter = (LeadPresenter) getPresenter();
        leadPresenter.showProgress(Constants.PROCESSING);
        mLeadStore.createLead(createLeadRequest, new ResponseListener<CreateLeadResponse>() {
            @Override
            public void onSuccess(CreateLeadResponse response) {
                leadPresenter.hideProgress();
                isDecoded = false ;
                if(AppConstants.Values.SUCCESS.equalsIgnoreCase(response.getResult())){
                    leadPresenter.launchDashboard(createLeadRequest.getLead().getFirstName(), response);
                }else{
                    AppError appError = response.getError();
                    if(AppConstants.ErrorCodes.OTP_ATTEMPT_EXCEEDED.equalsIgnoreCase(appError.getCode())){
                        leadPresenter.setErrors(appError.getDescription());
                        leadPresenter.regenerateOtp();
                    }else if(AppConstants.ErrorCodes.OTP_REGENARATION_EXCEEDED.equalsIgnoreCase(appError.getCode())){
                        leadPresenter.setErrors(appError.getDescription());
                        //leadPresenter.redirectLogin();
                        leadPresenter.finishOtpScreen();
                    }else{
                        leadPresenter.setErrors(appError.getDescription());
                    }
                }

            }

            @Override
            public void onFailure(CreateLeadResponse response) {

                Logger.i("Create Lead failure:", response.toString());
                //LeadOtpActivity activity =  (LeadOtpActivity) getActivity();
                leadPresenter.hideProgress();
                AppError appError = response.getError();
                leadPresenter.setErrors(appError.getDescription());
            }
        });
    }

    public  void updateLead(final CreateLeadRequest updateLeadRequest) {
        final LeadPresenter leadPresenter = (LeadPresenter) getPresenter();
        leadPresenter.showProgress(Constants.PROCESSING);
        mLeadStore.updateLead(updateLeadRequest, new ResponseListener<CreateLeadResponse>() {
            @Override
            public void onSuccess(CreateLeadResponse response) {
                leadPresenter.hideProgress();
                if(AppConstants.Values.SUCCESS.equalsIgnoreCase(response.getResult())){
                    leadPresenter.launchMyLeads();
                }else{
                    AppError appError = response.getError();
                    if(AppConstants.ErrorCodes.OTP_ATTEMPT_EXCEEDED.equalsIgnoreCase(appError.getCode())){
                        leadPresenter.setErrors(appError.getDescription());
                        leadPresenter.regenerateOtp();
                    }else if(AppConstants.ErrorCodes.OTP_REGENARATION_EXCEEDED.equalsIgnoreCase(appError.getCode())){
                        leadPresenter.setErrors(appError.getDescription());
                        //leadPresenter.redirectLogin();
                        leadPresenter.finishOtpScreen();
                    }else{
                        leadPresenter.setErrors(appError.getDescription());
                    }
                }

            }

            @Override
            public void onFailure(CreateLeadResponse response) {

                Logger.i("Update Lead failure:", response.toString());
                //LeadOtpActivity activity =  (LeadOtpActivity) getActivity();
                leadPresenter.hideProgress();
                AppError appError = response.getError();
                leadPresenter.setErrors(appError.getDescription());
            }
        });
    }

    public void saveDocument(Document document, Lead mLead) {
        final LeadPresenter leadPresenter = (LeadPresenter) getPresenter();
        leadPresenter.saveDocument(document, mLead);
        // ((NavigationActivity)getActivity()).onReplaceFragment(new LeadCreatedFragment(),false);
    }

    public void generateOTP(SendOTPRequest sendOTPRequest) {
        final LeadPresenter leadPresenter = (LeadPresenter) getPresenter();
        leadPresenter.showProgress(Constants.PROCESSING);

        StoreFactory.getStore().getUserStore().sendOTP(sendOTPRequest, new ResponseListener<SendOTPResponse>() {
            @Override
            public void onSuccess(SendOTPResponse response) {
                leadPresenter.hideProgress();
                if(AppConstants.Values.SUCCESS.equalsIgnoreCase(response.getResult())){
                    leadPresenter.startTimer(response);
                }else{
                    AppError appError = response.getError();
                    if(AppConstants.ErrorCodes.OTP_ATTEMPT_EXCEEDED.equalsIgnoreCase(appError.getCode())){
                        leadPresenter.setErrors(appError.getDescription());
                        leadPresenter.regenerateOtp();
                    }else if(AppConstants.ErrorCodes.OTP_REGENARATION_EXCEEDED.equalsIgnoreCase(appError.getCode())){
                        leadPresenter.setErrors(appError.getDescription());
                        //leadPresenter.redirectLogin();
                        leadPresenter.finishOtpScreen();
                    }else{
                        leadPresenter.setErrors(appError.getDescription());
                    }
                }
            }

            @Override
            public void onFailure(SendOTPResponse response) {
                leadPresenter.hideProgress();
                AppError appError = response.getError();
                leadPresenter.setErrors(appError.getDescription());
            }
        });
    }

    // This fetch lead is for My lead screen
    public void fetchLeads(FetchLeadRequest fetchLeadRequest , final boolean isRefresh){
        // My Lead data
        final LeadPresenter leadPresenter = (LeadPresenter) getPresenter();
        if(!isRefresh)
            leadPresenter.showProgress(Constants.LOADING);
        mLeadStore.fetchLeads(fetchLeadRequest , new ResponseListener<FetchLeadResponse>() {
            @Override
            public void onSuccess(FetchLeadResponse response) {
                if(!isRefresh)
                    leadPresenter.hideProgress();
                if(AppConstants.Values.SUCCESS.equalsIgnoreCase(response.getResult())){
                    AppApplication appApplication = (AppApplication) leadPresenter.getmContext().getApplication();
                    appApplication.setmMyLeadData(response);
                    if(isRefresh){
                        getAbstractFragment().refreshData();
                    }else{
                        leadPresenter.loadMyLeadsData();
                    }
                }else{
                    if(isRefresh){
                        getAbstractFragment().refreshData();
                    }else{
                        leadPresenter.loadMyLeadsData();
                    }
                    AppError appError = response.getError();
                    leadPresenter.setErrors(appError.getDescription());
                }

            }

            @Override
            public void onFailure(FetchLeadResponse response) {
                if(!isRefresh) {
                    leadPresenter.hideProgress();
                    leadPresenter.loadMyLeadsData();
                }
                AppError appError = response.getError();
                leadPresenter.setErrors(appError.getDescription());
            }
        });
    }


    public void searchLeads(String query){
        // My Lead data
        FetchLeadRequest fetchLeadRequest  = new FetchLeadRequest();
        SearchFilter searchFilter = new SearchFilter();
        searchFilter.setKeyword(query);
        searchFilter.setPageNumber(0);
        searchFilter.setNumberOfLeadsPerPage(0);
        fetchLeadRequest.setSearchFilter(searchFilter);
        final LeadPresenter leadPresenter = (LeadPresenter) getPresenter();
        leadPresenter.showProgress(Constants.LOADING);
        mLeadStore.fetchLeads(fetchLeadRequest , new ResponseListener<FetchLeadResponse>() {
            @Override
            public void onSuccess(FetchLeadResponse response) {
                leadPresenter.hideProgress();
                if(AppConstants.Values.SUCCESS.equalsIgnoreCase(response.getResult())){
                    getAbstractFragment().setFilterResult(response);
                }else{
                    AppError appError = response.getError();
                    leadPresenter.setErrors(appError.getDescription());
                }
            }

            @Override
            public void onFailure(FetchLeadResponse response) {
                leadPresenter.hideProgress();
                AppError appError = response.getError();
                leadPresenter.setErrors(appError.getDescription());
            }
        });
    }

    public void showLeadDetails(Lead lead, boolean isEditable) {
        final LeadPresenter leadPresenter = (LeadPresenter) getPresenter();
        leadPresenter.showLeadDetails(lead,isEditable);
    }

    public void fetchLeadsByPage(FetchLeadRequest fetchLeadRequest) {
        // My Lead data
        final LeadPresenter leadPresenter = (LeadPresenter) getPresenter();
        mLeadStore.fetchLeads(fetchLeadRequest , new ResponseListener<FetchLeadResponse>() {
            @Override
            public void onSuccess(FetchLeadResponse response) {
                if(!AppConstants.Values.SUCCESS.equalsIgnoreCase(response.getResult())){
                    AppError appError = response.getError();
                    leadPresenter.setErrors(appError.getDescription());
                }
                getAbstractFragment().leadsFetched(response);
            }
            @Override
            public void onFailure(FetchLeadResponse response) {
                AppError appError = response.getError();
                leadPresenter.setErrors(appError.getDescription());
                getAbstractFragment().leadsFetched(response);
            }
        });
    }
}
