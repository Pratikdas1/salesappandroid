package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by fgb on 12/14/16.
 */

public enum StaticDataTypes {

    COUNTRY, GENDER, EMIRATES, PRODUCT_TYPE,TIER, PRODUCT_INTEREST,ACC_CC_TYPE, PHILOSOPHY, DOCUMENT_TYPES;
}
