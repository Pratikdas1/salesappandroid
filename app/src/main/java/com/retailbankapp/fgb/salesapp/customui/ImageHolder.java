package com.retailbankapp.fgb.salesapp.customui;

/**
 * Created by O7548 on 2/5/2017.
 */

public enum ImageHolder {

    INSTANCE;
    private byte[] image ;

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
