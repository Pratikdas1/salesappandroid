package com.retailbankapp.fgb.salesapp.common;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by fgb on 11/22/16.
 */

public abstract class AbstractPresenter {

    private AbstractRouter abstractRouter ;
    private AbstractActivity mContext ;

    public AbstractActivity getmContext() {
        return mContext;
    }

    public void setmContext(AbstractActivity mContext) {
        this.mContext = mContext;
    }

    public AbstractRouter getAbstractRouter() {
        return abstractRouter;
    }

    public void setAbstractRouter(AbstractRouter abstractRouter) {
        this.abstractRouter = abstractRouter;
    }

    public void showProgress(String progressMsg){
        mContext.showProgress(progressMsg);
    }

    public void hideProgress(){
        mContext.hideProgress();
    }
}
