package com.retailbankapp.fgb.salesapp.dashboard;

import com.retailbankapp.fgb.salesapp.AppApplication;
import com.retailbankapp.fgb.salesapp.common.AbstractInteractor;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.AppError;
import com.retailbankapp.fgb.salesapp.models.FetchLeadRequest;
import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.LogoutResponse;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.stores.LeadStore;
import com.retailbankapp.fgb.salesapp.stores.StoreFactory;
import com.retailbankapp.fgb.salesapp.stores.UserStore;

/**
 * Created by fgb on 12/14/16.
 */

public class NavigationInteractor extends AbstractInteractor {

    private UserStore mUserStore;

    private LeadStore mLeadStore;

    public NavigationInteractor(){
        mLeadStore = StoreFactory.getStore().getLeadStore();
        mUserStore = StoreFactory.getStore().getUserStore();
    }

    public void fetchLeads(FetchLeadRequest fetchLeadRequest){
        // Dashboard data
        final NavigationPresenter navigationPresenter = (NavigationPresenter) getPresenter();
        //navigationPresenter.showProgress(Constants.LOADING);
        mLeadStore.fetchLeads(fetchLeadRequest , new ResponseListener<FetchLeadResponse>() {
            @Override
            public void onSuccess(FetchLeadResponse response) {
               // navigationPresenter.hideProgress();
                if(AppConstants.Values.SUCCESS.equalsIgnoreCase(response.getResult())){
                    AppApplication appApplication = (AppApplication) navigationPresenter.getmContext().getApplication();
                    appApplication.setmDashboardData(response);
                }else{
                    AppError errors = response.getError();
                    navigationPresenter.setErrors(errors.getDescription());
                }
                navigationPresenter.loadDashboard();

            }

            @Override
            public void onFailure(FetchLeadResponse response) {
               // navigationPresenter.hideProgress();
                navigationPresenter.hideProgress();
                AppError appError = response.getError();
                navigationPresenter.setErrors(appError.getDescription());
            }
        });
    }

    public void logout(){
        final NavigationPresenter navigationPresenter = (NavigationPresenter) getPresenter();
        mUserStore.logout(new ResponseListener<LogoutResponse>() {
            @Override
            public void onSuccess(LogoutResponse response) {
                Logger.e("logout","logout success");
                navigationPresenter.logout();
            }

            @Override
            public void onFailure(LogoutResponse response) {
                Logger.e("logout","logout failure");
                navigationPresenter.logout();
            }
        });

    }

    public void startCreateLeadScreen() {
        NavigationPresenter navigationPresenter = (NavigationPresenter) getPresenter();
        navigationPresenter.startCreateLeadScreen();
    }

    public void startMyLeadScreen() {
        NavigationPresenter navigationPresenter = (NavigationPresenter) getPresenter();
        navigationPresenter.startMyLeadScreen();
    }
}
