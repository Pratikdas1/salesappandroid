package com.retailbankapp.fgb.salesapp.service;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.ServerPayload;

import java.util.Map;

/**
 * Created by Pratik Das on 1/21/17.
 */

public class FBMessagingService extends FirebaseMessagingService{
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Map<String,String> map = remoteMessage.getData();
            Logger.i("Message data payload: {}", map.get("otp"));
            String otp = map.get("otp")!=null?String.valueOf(map.get("otp")):null;
            ServerPayload.INSTANCE.setValue(otp);
        }
    }
}
