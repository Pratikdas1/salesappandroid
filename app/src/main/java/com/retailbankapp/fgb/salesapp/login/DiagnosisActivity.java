package com.retailbankapp.fgb.salesapp.login;

import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.retailbankapp.fgb.salesapp.BuildConfig;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.helpers.NetworkUtils;
import com.retailbankapp.fgb.salesapp.lead.CreateLeadActivity;
import com.retailbankapp.fgb.salesapp.models.AppError;
import com.retailbankapp.fgb.salesapp.models.PingResponse;
import com.retailbankapp.fgb.salesapp.models.RegisterDeviceRequest;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.models.SessionContext;
import com.retailbankapp.fgb.salesapp.models.Setting;
import com.retailbankapp.fgb.salesapp.models.SettingsConfig;
import com.retailbankapp.fgb.salesapp.models.SettingsResponse;
import com.retailbankapp.fgb.salesapp.stores.ServiceBuilder;
import com.retailbankapp.fgb.salesapp.stores.StoreFactory;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DiagnosisActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnosis);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppTextView versionTextView =  (AppTextView)findViewById(R.id.versionTextView);
        versionTextView.setText(String.valueOf(BuildConfig.VERSION_NAME+BuildConfig.VERSION_CODE));
        ((AppTextView)findViewById(R.id.deviceNameTextView)).setText(getDeviceName());
        ((AppTextView)findViewById(R.id.androidVerTextView)).setText(getVersion());
        final String networkAvl = NetworkUtils.isConnectedToInternet(this) ? Constants.YES : Constants.NO ;
        ((AppTextView)findViewById(R.id.networkAvlTextView)).setText(networkAvl);
     //   String networkQual = NetworkUtils.isConnectedFast(this)? Constants.GOOD : Constants.WEAK;
     //   ((AppTextView)findViewById(R.id.networkQualTextView)).setText(networkQual);

         SessionContext.INSTANCE.setContext(this);
         final long startTimeInMs = (new Date()).getTime();

        if(NetworkUtils.isConnectedToInternet(this)){
            StoreFactory.getStore().getUserStore().pingServer(new ResponseListener<PingResponse>() {
                @Override
                public void onSuccess(PingResponse response) {
                    (findViewById(R.id.serviceProgressBar)).setVisibility(View.GONE);
                    ((AppTextView)findViewById(R.id.serviceAvlTextView)).setVisibility(View.VISIBLE);
                    ((AppTextView)findViewById(R.id.serviceAvlTextView)).setText( Constants.YES);
                     setNetworkQuality(startTimeInMs);
                }

                @Override
                public void onFailure(PingResponse response) {
                    AppError errors =  response.getError();
                    (findViewById(R.id.serviceProgressBar)).setVisibility(View.GONE);
                    if(errors.getCode() == AppConstants.ErrorCodes.SERVICE_UNAVAILABLE || errors.getCode() == AppConstants.ErrorCodes.NETWORK_UNAVAILABLE ){
                        ((AppTextView)findViewById(R.id.serviceAvlTextView)).setVisibility(View.VISIBLE);
                        ((AppTextView)findViewById(R.id.serviceAvlTextView)).setText( Constants.NO);
                        setNetworkQuality(startTimeInMs);
                        if(response != null){
                            AppError error = response.getError();
                            if(error != null){
                                showError(error.getDescription());
                               // ((TextView)findViewById(R.id.textView)).setText(ServiceBuilder.logVar);
                            }
                        }
                    }
                }
            });
        }else{
            (findViewById(R.id.serviceProgressBar)).setVisibility(View.GONE);
            ((AppTextView)findViewById(R.id.serviceAvlTextView)).setVisibility(View.VISIBLE);
            ((AppTextView)findViewById(R.id.serviceAvlTextView)).setText(Constants.NO);
        }
    }

    public void showError(String msg) {
        // Snackbar.make(view, ge\tString(R.string.network_connection_error), Snackbar.LENGTH_LONG).setAction("Action", null).show();
        Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), msg, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this,R.color.textColor));
        snackbarView.setBackgroundColor(ContextCompat.getColor(this,android.R.color.holo_red_dark));
        snackbar.setDuration(10000);
        snackbar.show();
    }
    private void setNetworkQuality(long startTimeInMs) {
        long endTimeInMs = (new Date()).getTime();
        long diffInSec =    TimeUnit.MILLISECONDS.toSeconds(endTimeInMs-startTimeInMs);
        Logger.e("Network quality:","In sercond :"+diffInSec);
        String networkQuality = "";
        if(diffInSec < 15){
            networkQuality  = Constants.VERY_GOOD;
        }else if (diffInSec >= 15 && diffInSec <= 30){
            networkQuality  = Constants.GOOD;
        }else{
            networkQuality = Constants.POOR;
        }
        ((AppTextView)findViewById(R.id.networkQualTextView)).setText(networkQuality);
    }


    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
            return (model);
        }
        return (manufacturer) + " " + model;
    }

    public static String getVersion(){
        return Build.VERSION.RELEASE;
    }


    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(DiagnosisActivity.this);
    }
}
