package com.retailbankapp.fgb.salesapp.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fgb on 11/24/16.
 */
public class Errors {
    private List<AppError> errorList;

    public void add(final String code, final  String description){
        if(errorList==null){
            errorList = new ArrayList<>();
        }
        errorList.add(new AppError(code,description));
    }
}
