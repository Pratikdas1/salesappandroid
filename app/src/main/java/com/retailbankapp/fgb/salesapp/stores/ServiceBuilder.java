package com.retailbankapp.fgb.salesapp.stores;


import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.retailbankapp.fgb.salesapp.BuildConfig;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.CryptoHelper;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.helpers.PropertyReader;
import com.retailbankapp.fgb.salesapp.models.AbstractResponse;
import com.retailbankapp.fgb.salesapp.models.AppError;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;

import com.retailbankapp.fgb.salesapp.models.SessionContext;
import com.retailbankapp.fgb.salesapp.models.SettingsConfig;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import java.net.ConnectException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.CertificatePinner;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
//import retrofit2.converter.fastjson.FastJsonConverterFactory;


/**
 * Created by Pratik Das on 12/5/16.
 */

public enum ServiceBuilder {
    INSTANCE;

    private OkHttpClient.Builder httpClient;
    private final Gson gson = new Gson();
    private static String TAG = ServiceBuilder.class.getCanonicalName();
    private  String url ;

    private  String generateAccessToken(){
        String token = null;
        String macKey = SessionContext.INSTANCE.getMacKey();

        String convertedDate = getFormattedDate();
        Logger.e("ec {}",String.valueOf(SessionContext.INSTANCE.getEc()));
        String clearMsg = String.format("%s%s%s",SessionContext.INSTANCE.getDeviceID(),convertedDate,SessionContext.INSTANCE.getEc() );
        String msg = macKey!=null?generateHMAC(macKey,clearMsg):"";

        try {

            token = SessionContext.INSTANCE.getConvID()  + msg ;
        }catch(Exception e){

        }
        return token;
    }

    private  String generateHMAC(String secret, String data) {
        String result = null;
        if(secret.length()==0){return "";}
        try {

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            result =  byteArrayToHexString(sha256_HMAC.doFinal(data.getBytes("UTF-8")));
        } catch (Exception e) {
            System.out.println("Unexpected error while creating hash: " + e.getMessage());

        }

        return result;
    }

    private  String byteArrayToHexString(byte[] array) {
        StringBuffer hexString = new StringBuffer();
        for (byte b : array) {
            int intVal = b & 0xff;
            if (intVal < 0x10)
                hexString.append("0");
            hexString.append(Integer.toHexString(intVal));
        }
        return hexString.toString();
    }

    private static X509TrustManager tm = new X509TrustManager() {
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    };


    public static X509TrustManager provideX509TrustManager() {
        try {
            TrustManagerFactory factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            factory.init((KeyStore) null);
            TrustManager[] trustManagers = factory.getTrustManagers();
            return (X509TrustManager) trustManagers[0];
        } catch (NoSuchAlgorithmException | KeyStoreException exception) {
            Log.e("Retrofit", "not trust manager available", exception);
        }

        return null;
    }

    public static SSLSocketFactory provideSSLSocketFactory() {
        //Uncomment below code to skip SSL Pinning . comment uncommented code
       /* try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{tm}, null);
            return sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException exception) {
            Log.e("Retrofit", "not tls ssl socket factory available", exception);
        }
        return (SSLSocketFactory) SSLSocketFactory.getDefault();*/

        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X.509");
            //AssetManager assetManager = SessionContext.INSTANCE.getContext().getAssets();
            //InputStream inputStream = assetManager.open(PropertyReader.INSTANCE.getProperty("cer.name"));
            int resId = SessionContext.INSTANCE.getContext().getResources()
                    .getIdentifier(PropertyReader.INSTANCE.getProperty("cer.name"), null, SessionContext.INSTANCE.getContext().getPackageName());
            InputStream inputStream =  SessionContext.INSTANCE.getContext().getResources().openRawResource(resId);
            Certificate ca;
            try {
                ca = cf.generateCertificate(inputStream);
                Logger.i(TAG,"ca=" + ((X509Certificate) ca).getSubjectDN());
                Logger.i(TAG,"ca=" + ca.getPublicKey());
            } finally {
                inputStream.close();
            }
            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);
            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);
            // Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);
            return context.getSocketFactory() ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null ;
    }

    private static HostnameVerifier sHostnameVerifier = new HostnameVerifier() {
        @Override
        public boolean verify(String s, SSLSession sslSession) {
            HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
            //Uncomment below line to skip SSL Pinning . comment next line
            //return  true ;
            return hv.verify(PropertyReader.INSTANCE.getProperty("cer.hostname"), sslSession);
        }
    };

    private  String getFormattedDate(){
        Date date = new Date();
        String str = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").format(date);
        return str;
    }
    private  <S> S create(Class<S> serviceClass, final String opName) {
        {
            final String serviceURL = SettingsConfig.getValue(AppConstants.SettingKeys.SERVICE_URL);
            //final String url = serviceURL==null?PropertyReader.INSTANCE.getProperty("service.default.url") : serviceURL;
            final String url = PropertyReader.INSTANCE.getProperty("service.default.url") ;
            //httpClient = new OkHttpClient.Builder().sslSocketFactory(provideSSLSocketFactory(tm),provideX509TrustManager());
            httpClient = new OkHttpClient.Builder();
            final String token = generateAccessToken();


            CertificatePinner certificatePinner = new CertificatePinner.Builder()
                    .add("ftsitpvihsinf14.fgb.ae", "sha256/y6eqRUYOpDtvpysVAS3y7jOfX1IVY8LnWUbK0ykwkmM=")
                    .build();


            httpClient.addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        Request.Builder requestBuilder = original.newBuilder()
                                .header("Accept",  "application/json")
                                .header("Authorization", token)
                                .header("Date", getFormattedDate())
                                .header("op", opName)
                                .header("app_version",String.valueOf(BuildConfig.VERSION_NAME+BuildConfig.VERSION_CODE))
                                .header("os_version",String.valueOf(Build.VERSION.SDK_INT))
                                .header("source", AppConstants.APP_NAME)
                                .header("latitude", String.valueOf(SessionContext.INSTANCE.getLatitude()))
                                .header("longitude", String.valueOf(SessionContext.INSTANCE.getLongitude()))
                                .header("dv", "a")
                                .url(url)
                                .method(original.method(), original.body());


                        Request request = requestBuilder.build();

                        return chain.proceed(request);
                    }
                });
           // }
            httpClient.connectTimeout(60, TimeUnit.SECONDS);
            httpClient.readTimeout(60,TimeUnit.SECONDS);
            httpClient.addInterceptor(addLogging());
            //TODO figure out how to implement with certificatePinner API
           // httpClient.certificatePinner(certificatePinner);
            httpClient.sslSocketFactory(provideSSLSocketFactory(),provideX509TrustManager());
            httpClient.hostnameVerifier(sHostnameVerifier);

            final OkHttpClient client = httpClient.build();
            Retrofit.Builder builder =
                    new Retrofit.Builder()
                            .baseUrl(url)
                            .addConverterFactory(ToStringConverterFactory.create());

            Retrofit retrofit = builder.client(client).build();
            return retrofit.create(serviceClass);
        }
    }



    private static HttpLoggingInterceptor addLogging(){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }




    public String encryptRequest(final Object jsonReq){
        if(jsonReq==null) return null;
        final String iv = SessionContext.INSTANCE.getEncIv();
        final String key = SessionContext.INSTANCE.getEncKey();

        String request = gson.toJson(jsonReq);
        Logger.e(TAG,"Request:"+request);

        String encRequest = CryptoHelper.encrypt(request,key,iv);
        return encRequest;

    }
    public  <T> T decryptResponse(final String response, final  Class<T> responseClass){
        if(response==null) return null;
        final String iv = SessionContext.INSTANCE.getEncIv();
        final String key = SessionContext.INSTANCE.getEncKey();

        String resp = CryptoHelper.decrypt(response,key, iv);
        Logger.i("response after decrypt {}",resp);
        return gson.fromJson(resp, responseClass);
    }

    public <T,U> void invoke(final String op, final U request, final ResponseListener<T> serviceResponse, Class<T> t) {

        ServiceInvoker service = create(ServiceInvoker.class,op);

        Call<String> resp = null;
        if(Arrays.asList(AppConstants.EncNotReqUrls).contains(op)){
            String jsonRequest = request!=null?gson.toJson(request):null;

            resp = service.invoke(jsonRequest);
        }else{

            String encRequest = encryptRequest(request);
            resp = service.invoke(encRequest);
        }

       final Class<T> type = t;
        resp.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                SessionContext.INSTANCE.incrementEC();
                String resp = response.body();
                try {
                    T objResponse = null;
                    if(Arrays.asList(AppConstants.EncNotReqUrls).contains(op)) {
                        String decodedResp = new String(Base64.decode(resp, Base64.NO_WRAP), "UTF-8");
                        objResponse = gson.fromJson(decodedResp, type);
                    }else {

                        if(TextUtils.isEmpty(resp) || "null".equalsIgnoreCase(resp)){
                            try {
                                objResponse = type.newInstance();
                                AbstractResponse errorResp = (AbstractResponse)objResponse;
                                AppError err = AppError.getDefaultError();
                                errorResp.setError(err);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }else {
                            objResponse = decryptResponse(response.body(), type);
                            if(objResponse == null){
                                try {
                                    objResponse = type.newInstance();
                                    AbstractResponse errorResp = (AbstractResponse)objResponse;
                                    AppError err = AppError.getDefaultError();
                                    errorResp.setError(err);
                                } catch (InstantiationException e) {
                                    e.printStackTrace();
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
                    AbstractResponse errorResp = (AbstractResponse)objResponse;
                    if(errorResp != null){
                        AppError err = errorResp.getError();
                        if(err != null && AppConstants.ErrorCodes.SESSION_EXPIRED.equalsIgnoreCase(err.getCode())){
                            SessionContext.INSTANCE.getContext().sessionExpired(err.getDescription());
                           // return ;
                        }
                    }
                    serviceResponse.onSuccess(objResponse);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                T objResponse = null;
                try {
                    objResponse = type.newInstance();
                    AbstractResponse errorResp = (AbstractResponse)objResponse;
                    if(t instanceof ConnectException){
                        AppError err = new AppError(AppConstants.ErrorCodes.NETWORK_UNAVAILABLE, Constants.NETWORK_UNAVAILABLE);
                        errorResp.setError(err);
                        serviceResponse.onFailure(objResponse);
                    }else if(t instanceof SSLHandshakeException){
                        AppError err = new AppError(AppConstants.ErrorCodes.SERVICE_UNAVAILABLE, t.getMessage());
                        errorResp.setError(err);
                    }else{
                        AppError err = new AppError(AppConstants.ErrorCodes.NETWORK_UNAVAILABLE, t.getMessage());
                        errorResp.setError(err);
                        serviceResponse.onFailure(objResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


}
