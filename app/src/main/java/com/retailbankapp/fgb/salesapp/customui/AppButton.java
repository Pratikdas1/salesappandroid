package com.retailbankapp.fgb.salesapp.customui;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

/**
 * Created by Anwar on 11/26/2016.
 */

public class AppButton extends AppCompatButton {
    public AppButton(Context context) {
        super(context);
    }

    public AppButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        AppFontHelper.setCustomFont(this, context, attrs);
    }

    public AppButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        AppFontHelper.setCustomFont(this, context, attrs);
    }
}
