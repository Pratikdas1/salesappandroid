package com.retailbankapp.fgb.salesapp.stores;

import com.retailbankapp.fgb.salesapp.helpers.PropertyReader;
import com.retailbankapp.fgb.salesapp.stores.stubs.LeadStoreStub;
import com.retailbankapp.fgb.salesapp.stores.stubs.UserStoreStub;

/**
 * Created by Pratik Das on 12/5/16.
 */

public final class StoreFactory {


    public static Store getStore(){
      String stubEnabled = PropertyReader.INSTANCE.getProperty("stub.enabled");

      return "Y".equalsIgnoreCase(stubEnabled)? ClientStub.INSTANCE: APIStore.INSTANCE;

    }

    public interface Store{
        UserStore getUserStore();
        LeadStore getLeadStore();
        StaticDataStore getStaticDataStore();
        MenuStore getMenuStore();
    }

    public enum APIStore implements Store{
        INSTANCE;
        @Override
        public UserStore getUserStore() {
            return UserAPIStore.INSTANCE;
        }

        @Override
        public LeadStore getLeadStore() {
            return LeadAPIStore.INSTANCE;
        }

        @Override
        public StaticDataStore getStaticDataStore() {
            return null;
        }

        @Override
        public MenuStore getMenuStore() {
            return null;
        }
    }

    enum ClientStub implements Store{
        INSTANCE;
        @Override
        public UserStore getUserStore() {
            return UserStoreStub.INSTANCE;
        }

        @Override
        public LeadStore getLeadStore() {
            return LeadStoreStub.INSTANCE;
        }

        @Override
        public StaticDataStore getStaticDataStore() {
            return null;
        }

        @Override
        public MenuStore getMenuStore() {
            return null;
        }
    }
}


