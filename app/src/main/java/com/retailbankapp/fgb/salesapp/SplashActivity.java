package com.retailbankapp.fgb.salesapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.RootUtils;
import com.retailbankapp.fgb.salesapp.lead.LeadOtpActivity;
import com.retailbankapp.fgb.salesapp.login.LoginActivity;

public class SplashActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView imageView = (ImageView) findViewById(R.id.logoImageView);

        imageView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.splashlogo_anim));
        //launchLoginScreen();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               launchLoginScreen();
            }
        },2500);
    }

    private void launchLoginScreen() {
        if(!RootUtils.isDeviceRooted()){
            startActivity(new Intent(SplashActivity.this,LoginActivity.class));
            finish();
        }else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showError(Constants.ROOTED_DEVICE_MSG);
                    finish();
                }
            },2000);

        }
        //startActivity(new Intent(SplashActivity.this,LoginActivity.class));
        //finish();

        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }
}
