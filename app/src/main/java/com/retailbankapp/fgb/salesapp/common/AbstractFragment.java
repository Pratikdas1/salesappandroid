package com.retailbankapp.fgb.salesapp.common;

import android.support.v4.app.Fragment;

import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.Lead;

import java.util.List;

/**
 * Created by o7548 on 12/28/2016.
 */

public abstract  class AbstractFragment extends Fragment {
    private volatile AbstractInteractor interactor;

    public <T> T getInteractor(Class interactorClass) {
        return (T) interactor;
    }

    public void setInteractor(AbstractInteractor interactor) {
        this.interactor = interactor;
        this.interactor.setAbstractFragment(this);
    }

    public abstract  void setFilterResult(FetchLeadResponse response);
    public abstract  void leadsFetched(FetchLeadResponse response);
    public abstract  void refreshData();

}
