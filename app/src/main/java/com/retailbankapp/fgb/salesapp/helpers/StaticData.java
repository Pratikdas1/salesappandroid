package com.retailbankapp.fgb.salesapp.helpers;

import java.util.HashMap;

/**
 * Created by O7548 on 12/4/2016.
 */

public class StaticData {

    public static HashMap<String,String>  countryList ;



    public static HashMap<String,String>  getCountryList() {
        if(countryList == null){
            countryList = new HashMap<String,String>();
        }
        countryList.put("UAE","UE");
        countryList.put("INDIA","IN");
        countryList.put("PAKISTAN","PK");
        countryList.put("SHRILANKA","SR");
        countryList.put("BANGLADESH","BN");
        countryList.put("COLUMBIA","CO");
        countryList.put("COSTA RICA","CR");
        countryList.put("CUBA","CU");
        countryList.put("CAPE VERDE","CV");
        countryList.put("CHRISTMAS ISLANDS","CX");
        countryList.put("CYPRUS","CY");
        countryList.put("CZECH REPUBLIC","CZ");
        countryList.put("GERMANY","DE");
        countryList.put("DJIBOUTI","DJ");
        countryList.put("DENMARK","DK");
        countryList.put("DOMINICA","DM");
        countryList.put("DOMINICAN REPUBLIC","DO");
        countryList.put("ALGERIA","DZ");

        return countryList ;
    }


    public static String[] getEmirates(){
        //Ajman,Ras al-Khaimah,Umm al-Quwain,Sharjah,Dubai,Abu Dhabi,Fujairah
        return new String[]{"Ajman", "Ras al-Khaimah" , "Umm al-Quwain" , "Sharjah" , "Dubai" , "Abu Dhabi" , "Fujairah"};
    }

    public static String[] getProductType(){
        return new String[]{"Auto", "Credit Card" , "Personal" , "Product Bundle"};
    }

    public static String[] getPhilosophy(){
        return new String[]{"Islamic", "Conventional"};
    }

    public static String[] getAutoProductNames(){
        return new String[]{"Auto"};
    }
    public static String[] getCcProductNames(){
        return new String[]{"Supplementary Card" , "Abu Dhabi Platinum Card" ,  "Abu Dhabi Titanium Card" ,
        "Ferrari Card" , "Gold Card" , "Masdar Card" , "MY Card" ,"FGB - Du Cards" , "MCFC Card","Platinum Card" ,"Standard Card"};
    }
    public static String[] getPersonalProductNames(){
        return new String[]{"National Loans" , "Personal Installment Loans" , "Personal Loans - STL" , "Personal Loans - TOP UP" };
    }
    public static String[] getPersonalBundleProductNames(){
        return new String[]{"Accelerator" , "PBEB" };
    }


    public static String[] getAccountProductInterest(){
        return new String[]{"Current Account", "Savings Account" };
    }
    public static String[] getCreditCardProductInterest(){
        return new String[]{"New Card", "Quick Cash" , "Balance Transfer"};
    }

    public static String[] getGender(){
        return new String[]{"Male", "Female" };
    }

}
