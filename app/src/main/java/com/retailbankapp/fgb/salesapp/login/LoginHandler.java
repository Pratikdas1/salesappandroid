package com.retailbankapp.fgb.salesapp.login;

import android.databinding.BindingAdapter;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;

import com.retailbankapp.fgb.salesapp.databinding.ActivityLoginBinding;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.User;
import com.retailbankapp.fgb.salesapp.models.VerifyUserRequest;

/**
 * Created by O7548 on 12/5/2016.
 */

public class LoginHandler {

    private String TAG = LoginHandler.class.getName() ;
    public String LOGIN_STRING = Constants.LOGIN ;
    private  ActivityLoginBinding mActvityLoginBinding ;

    public LoginHandler(ActivityLoginBinding mActvityLoginBinding) {
        this.mActvityLoginBinding = mActvityLoginBinding ;
    }

    public  boolean validate(){
        Logger.v(TAG,"Login Validate");
        boolean isValid = true ;
        VerifyUserRequest user = mActvityLoginBinding.getUser() ;
        mActvityLoginBinding.layoutEmail.setError(null);
        mActvityLoginBinding.layoutPassword.setError(null);
        if(TextUtils.isEmpty(user.getUserName())){
            mActvityLoginBinding.editTextUserName.requestFocus();
            mActvityLoginBinding.layoutEmail.setError(Constants.USERNAME_EMPTY);
            isValid = false ;
        }
        if(TextUtils.isEmpty(user.getPwd())){
            mActvityLoginBinding.editTextPassword.requestFocus();
            mActvityLoginBinding.layoutPassword.setError(Constants.PASSWORD_EMPTY);
            isValid = false ;
        }
        return  isValid ;
    }

    public void setErrors() {
        mActvityLoginBinding.editTextUserName.requestFocus();
        mActvityLoginBinding.layoutEmail.setError(Constants.INVALID_USER);
        mActvityLoginBinding.layoutPassword.setError(Constants.INVALID_USER);
    }
}
