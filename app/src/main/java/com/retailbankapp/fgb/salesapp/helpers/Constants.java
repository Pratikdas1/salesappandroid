package com.retailbankapp.fgb.salesapp.helpers;

/**
 * Created by O7548 on 12/5/2016.
 */

public interface Constants {
    String USERNAME_EMPTY = "User name is required.";
    String PASSWORD_EMPTY = "Password is required.";
    String LOGIN = "Login";
    String LOGIN_RESPONSE = "login_response";
    String REGENERATE_OTP = "Regenerate OTP";
    String VERIFY = "verify";
    String OTP_REQUIRED = "OTP is required.";
    String INVALID_USER = "Invalid Username or Password";

    String PLEASE_WAIT = "Please wait..." ;
    String PROCESSING = "Processing..." ;
    String INVALID_OTP = "Entered OTP is invalid";
    String PROCEED = "Proceed";
    String NATIONALITY = "Nationality";
    String DOB_MANDAROTY = "Date of Birth is required";
    String FIRST_NAME_MANDATORY = "First name is required";
    String LAST_NAME_MANDATORY = "Last name is required";
    String PHONE_MANDATORY = "Mobile number is required";
    String MONTHLY_INCOME_MANDATORY = "Monthly Income is requied";
    String NATIONALITY_MANDAROTY = "Nationality is required ";
    String CANCEL = "Cancel";
    String SELECT_EMIRATES = "Select Emirate";
    String PRODUCT_TYPE_MANDAROTY = "Product type is required";
    String PRODUCT_INTEREST_MANDAROTY = "Product Interest is required";
    String PHILOSOPY_MANDAROTY = "Philosophy type is required";
    String SELECT_PHILOSOPHY = "Select Philosophy";
    String SELECT_PRODUCT = "Select Product";
    String AUTO = "Auto";
    String SELECT_PRODUCT_INTEREST = "Select product interest";
    String CREDIT_CARD = "Credit Card";
    String PERSONAL  = "Personal";
    String PRODUCT_BUNDLE = "Product Bundle";

    String SELECT_GENDER = "Select Gender";
    String GENDER_MANDAROTY = "Gender is required";
    String FIELD_REQUIRED = "This field is required";
    String EMIRATES_MANDATORY = "Emirates is required";
    String EMPNAME_REQUIRED = "Employer Name is required";
    String PASSPORT_MANDATORY = "Passport is required";
    String EMIRATES_ID_MANDATORY = "Emirates Id required";
    String LEAD_DETAILS = "lead_details";
    String CREATE_LEAD = "create_lead";
    String INVALID_EMAIL = "Email is not valid";
    String PLS_ACCEPT_ECB_CONSENT = "Please accept ECB Consent";
    String FIRST_NAME = "first_name";
    String TAB_SUBMITTED = "Submitted";
    String TAB_ASSIGNED = "Assigned";
    String LEAD_STATUS = "Lead Status";
    String LOGOUT = "Logout";
    String LOADING = "Loading...";
    String UPDATE_LEAD = "Update Lead";
    String SAVE = "Save";
    String ADD_MORE = "Add More...";
    String DELETE = "Delete";
    String SELECT_TIER = "Select tier";
    String NETWORK_UNAVAILABLE = "Please check your internet connection.";
    String VALIDATING = "Validating...";
    String SIGNING_IN = "Signing In...";
    String LEAD_REF = "lead_ref";
    String EDITABLE = "editable";
    String EMAIL_MANDATORY = "Email is required";
    String NO = "No";
    String YES = "Yes";
    String GOOD = "Good";
    String WEAK = "Weak";
    String ROOTED_DEVICE_MSG = "Your phone is rooted, terminating application...";
    String PLS_SELECT_CONSENT = "Please  authorize FGB to perform credit bureau check";
    String VERY_GOOD = "Very good";
    String POOR = "Poor";
}
