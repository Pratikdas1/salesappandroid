package com.retailbankapp.fgb.salesapp.customui;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by Anwar on 11/26/2016.
 */

public class AppEditText extends AppCompatEditText {
    public AppEditText(Context context) {
        super(context);
    }

    public AppEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        AppFontHelper.setCustomFont(this, context, attrs);
    }

    public AppEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        AppFontHelper.setCustomFont(this, context, attrs);
    }
}
