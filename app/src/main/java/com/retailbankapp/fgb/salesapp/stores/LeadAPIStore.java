package com.retailbankapp.fgb.salesapp.stores;

import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.models.CreateLeadRequest;
import com.retailbankapp.fgb.salesapp.models.CreateLeadResponse;
import com.retailbankapp.fgb.salesapp.models.FetchLeadRequest;
import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;

/**
 * Created by Pratik Das on 11/22/16.
 */

public enum LeadAPIStore implements LeadStore {
    INSTANCE;

    private ServiceBuilder serviceBuilder = ServiceBuilder.INSTANCE;


    @Override
    public void createLead(final CreateLeadRequest request, final ResponseListener<CreateLeadResponse> serviceResponse) {
        serviceBuilder.invoke(AppConstants.ServiceURLs.CREATE_LEAD, request, serviceResponse, CreateLeadResponse.class);

    }

    @Override
    public void updateLead(CreateLeadRequest request, final ResponseListener<CreateLeadResponse> serviceResponse) {
        serviceBuilder.invoke(AppConstants.ServiceURLs.UPDATE_LEAD, request, serviceResponse, CreateLeadResponse.class);

    }

    @Override
    public void fetchLeads(FetchLeadRequest request, final ResponseListener<FetchLeadResponse> serviceResponse) {
        serviceBuilder.invoke(AppConstants.ServiceURLs.FETCH_LEADS, request, serviceResponse, FetchLeadResponse.class);
    }
}
