package com.retailbankapp.fgb.salesapp.lead;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.common.VipConfigurator;
import com.retailbankapp.fgb.salesapp.customui.CameraActivity;
import com.retailbankapp.fgb.salesapp.customui.ImageHolder;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.models.Document;
import com.retailbankapp.fgb.salesapp.models.Lead;

import java.util.ArrayList;

public class UploadDocActivity extends AbstractActivity implements DocumentAdapter.DocumentLisener {

    private static final String TAG = UploadDocActivity.class.getCanonicalName();
    private static final int REQUEST_TAKE_PHOTO = 1;
    // private ImageView mImageView;
    private String mCurrentPhotoPath;
    private Lead mLead;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private DocumentAdapter mAdapter;
    private ArrayList<Document> documentArrayList;
    private static int docNumber = 1;
    private int CAPTURE_IMAGE = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_doc);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent() != null) {
            mLead = getIntent().getParcelableExtra(Constants.LEAD_DETAILS);
        }
        //  mImageView = (ImageView)findViewById(R.id.leadPicImageView);
        VipConfigurator.INSTANCE.configure(this, new LeadPresenter(), new LeadInteractor());


        mRecyclerView = (RecyclerView) findViewById(R.id.captureDocView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        documentArrayList = new ArrayList<Document>();
        // specify an adapter (see also next example)
        mAdapter = new DocumentAdapter(documentArrayList, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void launchCamera(View view) {
        startActivityForResult(new Intent(this, CameraActivity.class), CAPTURE_IMAGE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check which request we're responding to
        if (requestCode == CAPTURE_IMAGE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                // Do something with the contact here (bigger example below)
                Document document = new Document();
                document.setFileName( getString(R.string.doc_count,docNumber++));
                document.setType("Customer Declartion Form");
                document.setData(ImageHolder.INSTANCE.getImage());
                mAdapter.add(document);
            }
        }


    }

    /* private void setPhoto() {
         // Get the dimensions of the View
         int targetW = mImageView.getWidth();
         int targetH = mImageView.getHeight();

         // Get the dimensions of the bitmap
         BitmapFactory.Options bmOptions = new BitmapFactory.Options();
         bmOptions.inJustDecodeBounds = true;
         BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
         int photoW = bmOptions.outWidth;
         int photoH = bmOptions.outHeight;

         // Determine how much to scale down the image
         int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

         // Decode the image file into a Bitmap sized to fill the View
         bmOptions.inJustDecodeBounds = false;
         bmOptions.inSampleSize = scaleFactor;
         bmOptions.inPurgeable = true;

         Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
         mImageView.setImageBitmap(bitmap);
         // galleryAddPic();


     }*/

    @Override
    public void clicked(final int position, int operation) {
        // view op 1
        if (operation == DocumentAdapter.Operation.VIEW.ordinal()) {
            Document document = documentArrayList.get(position);
            Bitmap bitmap = BitmapFactory.decodeByteArray(document.getData(), 0, document.getData().length);
            final RelativeLayout promptView = (RelativeLayout) getLayoutInflater()
                    .inflate(R.layout.imageview_layout, null);
            ((ImageView) promptView.findViewById(R.id.imagePreview)).setImageBitmap(bitmap);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UploadDocActivity.this);
            alertDialogBuilder.setView(promptView);
            final Dialog dialog = alertDialogBuilder.create();
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            ((ImageView) promptView.findViewById(R.id.cancelButton)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

        } else {
            new AlertDialog.Builder(UploadDocActivity.this)
                    .setIcon(R.drawable.ic_delete_black_24dp)
                    .setTitle(getString(R.string.title_delete))
                    .setMessage(getString(R.string.msg_delete))
                    .setPositiveButton(getString(R.string.msg_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            mAdapter.remove(position);
                            if (mAdapter.getItemCount() == 0) {
                                docNumber = 0;
                            }
                        }
                    })
                    .setNegativeButton(getString(R.string.msg_cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .create()
                    .show();
        }

    }



    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
