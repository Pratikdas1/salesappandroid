package com.retailbankapp.fgb.salesapp.customui;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Anwar on 11/26/2016.
 */

public class AppTextView extends AppCompatTextView {
    public AppTextView(Context context) {
        super(context);
    }

    public AppTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        AppFontHelper.setCustomFont(this, context, attrs);
    }

    public AppTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        AppFontHelper.setCustomFont(this, context, attrs);
    }
}
