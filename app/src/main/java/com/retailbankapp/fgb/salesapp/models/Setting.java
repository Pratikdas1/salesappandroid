package com.retailbankapp.fgb.salesapp.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pratik Das on 12/15/16.
 */

public final class Setting {
    private String name;
    private String value;

    public Setting() {
    }

    public Setting(String name, String value) {
            this.name = name;
            this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
