package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by fgb on 11/23/16.
 */

public class CreateLeadResponse extends  AbstractResponse{
    private String leadRef;


    public String getLeadRef() {
        return leadRef;
    }

    public void setLeadRef(String leadRef) {
        this.leadRef = leadRef;
    }


}
