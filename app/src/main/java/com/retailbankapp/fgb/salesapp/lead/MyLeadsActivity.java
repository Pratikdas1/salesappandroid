package com.retailbankapp.fgb.salesapp.lead;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.common.VipConfigurator;
import com.retailbankapp.fgb.salesapp.customui.FontCache;
import com.retailbankapp.fgb.salesapp.dashboard.DashboardAdapter;
import com.retailbankapp.fgb.salesapp.dashboard.NavigationInteractor;
import com.retailbankapp.fgb.salesapp.dashboard.NavigationPresenter;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.FetchLeadRequest;
import com.retailbankapp.fgb.salesapp.models.SearchFilter;

public class MyLeadsActivity extends AbstractActivity {

    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private DashboardAdapter mDashboardAdapter;
    private static final String TAG = MyLeadsActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_leads);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(null);
        VipConfigurator.INSTANCE.configure(this,new LeadPresenter(),new LeadInteractor());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        //setupAdapter();
        tabLayout = ((TabLayout)findViewById(R.id.tabLayout));
        tabLayout.setupWithViewPager(mViewPager);
       // setupTabIcons();


        //fetch dashboard data
        LeadInteractor leadInteractor = getInteractor(LeadInteractor.class);
        SearchFilter searchFilter = new SearchFilter();
        //Fetching first page
        searchFilter.setPageNumber(1);
        FetchLeadRequest fetchLeadRequest = new FetchLeadRequest();
        fetchLeadRequest.setSearchFilter(searchFilter);
        // second parameter is whether you want to refresh data when pull to refresh is done
        leadInteractor.fetchLeads(fetchLeadRequest,false);

    }

    public void setupAdapter() {
        mDashboardAdapter = new DashboardAdapter(getSupportFragmentManager());
        mDashboardAdapter.addFragment(new SubmittedLeadFragment(), Constants.TAB_SUBMITTED);
        mDashboardAdapter.addFragment(new AssignedLeadFragment(), Constants.TAB_ASSIGNED);
        mViewPager.setAdapter(mDashboardAdapter);
        changeTabsFont();
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface
                            (FontCache.get(getString(R.string.app_font),this), Typeface.NORMAL);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }
    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(MyLeadsActivity.this);
    }
}
