package com.retailbankapp.fgb.salesapp.helpers;

/**
 * Created by Pratik Das on 1/1/17.
 */

public final class AppConstants {

    public  interface  Values {
        String SUCCESS = "success";
        String FAILURE = "failure";
        String OTP_MATCH_FAILED_ERR = "";
        String OTP_RETRY_FAILED_ERR = "";
        String SUBPROD_INTEREST_ACC = "accelerator";
        int LEADS_PER_PAGE = 15 ;
        Integer LAST_7_DAYS = 7;
    }

    public  interface  TxnCodes {
        String LOGIN = "LOGIN";
        String CREATE_LEAD = "CREATE_LEAD";
        String UPDATE_LEAD = "UPDATE_LEAD";
    }

    public  interface   MenuKeys {
        String CREATE_LEAD = "CREATE_LEAD";
        String MY_LEADS = "MY_LEADS";
        String DASHBOARD = "DASHBOARD";
        String LOGOUT = "Logout" ;
        String ABOUT = "About";
    }

    public  interface   MenuDesc {
        String CREATE_LEAD = "CREATE LEAD";
        String MY_LEADS = "MY LEADS";
        String DASHBOARD = "DASHBOARD";
        String LOGOUT = "LOGOUT" ;
        String ABOUT = "ABOUT";
    }

    public  interface   ProductType {
        String PERSONAL = "personal";
        String ACCOUNT = "account";
        String CREDIT_CARDS = "credit cards";
        String PRODUCT_BUNDLE = "product bundle" ;
        String LOAN = "loan" ;
    }

    public  interface   LeadStatus {
        String APPROVED = "APPROVED";
        String REJECTED = "REJECTED";
        String PENDING = "PENDING";
    }

    public interface LeadCategory{
        String ASSIGNED = "ASSIGNED";
        String SUBMITTED = "SUBMITTED";
    }
    public  interface ServiceURLs {
        String VERIFY_USER = "/user/verify";
        String VERIFY_OTP = "/user/otp/verify";
        String RESEND_OTP = "/user/otp/send";
        String FETCH_LEADS = "/leads/fetch";
        String CREATE_LEAD = "/lead/create";
        String UPDATE_LEAD = "/lead/update";
        String FETCH_SETTINGS = "/settings";
        String PING = "/ping";
        String CHECK_SERVICE_AVAILABILITY = "/service/available";
        String REGISTER_DEVICE = "/device/register";
        String UPDATE_REGISTER_DEVICE = "/device/register/update";
        String LOGOUT = "/user/logout";
    }

    public interface ErrorCodes{
        String OTP_ATTEMPT_EXCEEDED = "OTP003";
        String OTP_REGENARATION_EXCEEDED = "OTP006";
        String NETWORK_UNAVAILABLE = "NET001";
        String SERVICE_UNAVAILABLE ="SER001";
        String SESSION_EXPIRED  = "SESSION_EXPIRED";
    }

    public interface Gender{
        String MALE_CODE = "M";
        String MALE = "Male";
        String FEMALE = "Female";
    }

    public enum SettingKeys{
        SERVICE_URL, INACTIVITY_TIMEOUT_MINS;
       // String SERVICE_URL = "SERVICE_URL";
        //String INACTIVITY_TIMEOUT = "INACTIVITY_TIMEOUT";

    }
    public static  String[] EncNotReqUrls={ServiceURLs.FETCH_SETTINGS, ServiceURLs.REGISTER_DEVICE ,ServiceURLs.UPDATE_REGISTER_DEVICE, ServiceURLs.PING};

    public static final String APP_NAME = "SALESAPP";
}
