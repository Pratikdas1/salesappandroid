package com.retailbankapp.fgb.salesapp.stores.stubs;

import com.retailbankapp.fgb.salesapp.models.FetchStaticDataRequest;
import com.retailbankapp.fgb.salesapp.models.FetchStaticDataResponse;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.stores.StaticDataStore;

/**
 * Created by fgb on 12/5/16.
 */

public enum StaticDataStoreStub implements StaticDataStore {
    INSTANCE;


    @Override
    public void fetchStaticData(FetchStaticDataRequest request, ResponseListener<FetchStaticDataResponse> serviceResponse) {
        FetchStaticDataResponse fetchStaticDataResponse = new FetchStaticDataResponse();


        fetchStaticDataResponse.setResult("success");
        serviceResponse.onSuccess(fetchStaticDataResponse);
    }
}
