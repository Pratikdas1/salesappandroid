package com.retailbankapp.fgb.salesapp.customui;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

public class DatePickerUtility implements
        DatePickerDialog.OnDateSetListener {

    private EditText editText;

    private  DatePickerUtility sDatePickerUtility ;
    private  DatePickerDialog sDatePickerDialog;

    public DatePickerUtility() {

    }

    public  void  setDate(EditText editText, Context ctx){
        // Use the current date as the default date in the picker
        this.editText = editText;

        if(sDatePickerDialog == null){
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            sDatePickerDialog =  new DatePickerDialog(ctx, this, year, month, day);
        }

        if(!sDatePickerDialog.isShowing())
            sDatePickerDialog.show();
    }




    public void onDateSet(DatePicker view, int year, int month, int day) {
     // Do something with the date chosen by the user
        editText.setText(String.format("%02d/%02d/%s", month+1, day, year));
    }
}
