package com.retailbankapp.fgb.salesapp.login;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.common.VipConfigurator;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;
import com.retailbankapp.fgb.salesapp.databinding.ActivityLoginBinding;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.helpers.PermissionUtils;
import com.retailbankapp.fgb.salesapp.helpers.PropertyReader;
import com.retailbankapp.fgb.salesapp.lead.CreateLeadActivity;
import com.retailbankapp.fgb.salesapp.models.RegisterDeviceRequest;
import com.retailbankapp.fgb.salesapp.models.SessionContext;
import com.retailbankapp.fgb.salesapp.models.VerifyUserRequest;
import com.retailbankapp.fgb.salesapp.service.GPSTracker;

import java.util.UUID;


public class LoginActivity extends AbstractActivity {

    private ActivityLoginBinding mActvityLoginBinding;
    private int PERMISSION_ALL = 1;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION };//,Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private GPSTracker mGpsTracker ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_login);
        //String uniqueId = Setting.Secure.getString(getApplicationContext().getContentResolver(), Setting.Secure.ANDROID_ID);
        FirebaseApp firebaseApp = FirebaseApp.initializeApp(getApplicationContext());
        String token = FirebaseInstanceId.getInstance().getToken();
        SessionContext.INSTANCE.setDeviceToken(token);
        Logger.i("token11 {}", token);

        PropertyReader.INSTANCE.loadProperties(getApplicationContext());

        mActvityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        VerifyUserRequest user = new VerifyUserRequest();
        mActvityLoginBinding.setUser(user);

        LoginHandler loginHandler = new LoginHandler(mActvityLoginBinding);
        mActvityLoginBinding.setLoginHandler(loginHandler);
        //textNoSuggestions|textVisiblePassword
        //mActvityLoginBinding.editTextPassword.setTransformationMethod(new PasswordTransformationMethod());

        VipConfigurator.INSTANCE.configure(this, new LoginPresenter(), new LoginInteractor());

        LoginInteractor interactor = getInteractor(LoginInteractor.class);
        SessionContext.INSTANCE.setContext(this);
        String deviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        SessionContext.INSTANCE.setDeviceID(deviceID);

        interactor.fetchSettings();

        if (!PermissionUtils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        mGpsTracker = new GPSTracker(this);

        // check if GPS enabled
        if(mGpsTracker.canGetLocation()){

            double latitude = mGpsTracker.getLatitude();
            double longitude = mGpsTracker.getLongitude();
            SessionContext.INSTANCE.setLatitude(latitude);
            SessionContext.INSTANCE.setLongitude(longitude);
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            mGpsTracker.showSettingsAlert();
        }

    }


    public void processSubmit(View view) {
        hideKeyboard();
        LoginHandler loginHandler = mActvityLoginBinding.getLoginHandler();
        if (loginHandler != null && loginHandler.validate()) {
            LoginInteractor interactor = getInteractor(LoginInteractor.class);
            VerifyUserRequest request = mActvityLoginBinding.getUser();
            String token = FirebaseInstanceId.getInstance().getToken();
            Logger.i("token ", token);
            interactor.verifyUser(request);
        }

    }

    public void redirectDiagnosisScreen(View view) {
        LoginInteractor interactor = getInteractor(LoginInteractor.class);
        interactor.redirectDiagnosisScreen();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGpsTracker.stopUsingGPS();
        mActvityLoginBinding = null;
    }

    /*public void setErrors(String msg) {
        //LoginHandler loginHandler = mActvityLoginBinding.getLoginHandler();
        //loginHandler.setErrors();
        showError(Constants.INVALID_USER);
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ALL) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        requestPermissionRational(grantResults[i],Manifest.permission.WRITE_EXTERNAL_STORAGE ,
                                getString(R.string.permission_storage_rationale));
                    } else if (permissions[i].equals(Manifest.permission.CAMERA)) {
                        requestPermissionRational(grantResults[i],Manifest.permission.CAMERA ,
                                getString(R.string.permission_camera_rationale));
                    } else if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        requestPermissionRational(grantResults[i],Manifest.permission.ACCESS_FINE_LOCATION,
                                getString(R.string.permission_location_rationale));
                    }


                }

            }
        }
    }

    private void requestPermissionRational(final int grantResult ,final String permission, String dialogMsg) {
        if (grantResult != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(permission)) {
                showMessageOKCancel(dialogMsg,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(new String[] {permission},PERMISSION_ALL);
                            }
                        });
            }
        }
    }
    // TODO: MOVE TO UTILS
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LoginActivity.this)
                .setMessage(message)
                .setPositiveButton(getString(R.string.msg_ok), okListener)
                .setNegativeButton(getString(R.string.msg_cancel), null)
                .create()
                .show();
    }
}
