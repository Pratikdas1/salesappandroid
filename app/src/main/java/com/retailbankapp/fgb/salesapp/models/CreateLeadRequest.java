package com.retailbankapp.fgb.salesapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by fgb on 11/23/16.
 */

public final class CreateLeadRequest implements Parcelable{
    private Lead lead;
    private Document document;
    private String otp;
    private String ecbConsent;
   // private String convID;

    public CreateLeadRequest(Parcel in) {
        lead = in.readParcelable(getClass().getClassLoader());
        document = in.readParcelable(getClass().getClassLoader());
        otp = in.readString();
    }
    public CreateLeadRequest() {
    }


    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Lead getLead() {
        return lead;
    }

    public void setLead(Lead lead) {
        this.lead = lead;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(lead,i);
        parcel.writeParcelable(document,i);
        parcel.writeString(otp);
    }

    public static final Parcelable.Creator<CreateLeadRequest> CREATOR
            = new Parcelable.Creator<CreateLeadRequest>() {
        public CreateLeadRequest createFromParcel(Parcel in) {
            return new CreateLeadRequest(in);
        }

        public CreateLeadRequest[] newArray(int size) {
            return new CreateLeadRequest[size];
        }
    };

}
