package com.retailbankapp.fgb.salesapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by fgb on 11/23/16.
 */

public class Lead implements Parcelable{

    private String firstName ;
    private String lastName ;
    private String gender ;
    private String mobileNumber ;
    private String email;
    private String emirate;
    private String dateOfBirth;
    private String nationality;
    private String emiratesID;
    private String passportNumber;
    private String monthlySalary;
    private String employerName;
    private String productType;
    private String productInterest;
    private String philosophy;
    private boolean consentForECB;
    private String salesComments;
    private String createDate;
    private String leadRef;
    private String daysCreatedBefore;
    private String leadApplicationStatus;
    private String subProductInterest ;
    private String tier ;
    private String userId ;
    private String sourceBy ;

    public Lead() {

    }

    public String getDaysCreatedBefore() {
        return daysCreatedBefore;
    }

    public void setDaysCreatedBefore(String daysCreatedBefore) {
        this.daysCreatedBefore = daysCreatedBefore;
    }

    public String getSourceBy() {
        return sourceBy;
    }

    public void setSourceBy(String sourceBy) {
        this.sourceBy = sourceBy;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getSubProductInterest() {
        return subProductInterest;
    }

    public void setSubProductInterest(String subProductInterest) {
        this.subProductInterest = subProductInterest;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getLeadRef() {
        return leadRef;
    }

    public void setLeadRef(String leadRef) {
        this.leadRef = leadRef;
    }

    public String getLeadApplicationStatus() {
        return leadApplicationStatus;
    }

    public void setLeadApplicationStatus(String leadApplicationStatus) {
        this.leadApplicationStatus = leadApplicationStatus;
    }

    public Lead(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        gender = in.readString();
        mobileNumber = in.readString();
        email = in.readString();
        emirate = in.readString();
        dateOfBirth = in.readString();
        nationality = in.readString();
        emiratesID = in.readString();
        passportNumber = in.readString();
        monthlySalary = in.readString();
        employerName = in.readString();
        productType = in.readString();
        productInterest = in.readString();
        philosophy = in.readString();
        consentForECB = in.readByte() != 0;
        salesComments = in.readString();
        createDate = in.readString();
        leadRef = in.readString();
        leadApplicationStatus = in.readString();
        subProductInterest = in.readString();
        tier = in.readString();
        sourceBy = in.readString();
        userId = in.readString() ;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmirate() {
        return emirate;
    }

    public void setEmirate(String emirate) {
        this.emirate = emirate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getEmiratesID() {
        return emiratesID;
    }

    public void setEmiratesID(String emiratesID) {
        this.emiratesID = emiratesID;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(String monthlySalary) {
        this.monthlySalary = monthlySalary;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductInterest() {
        return productInterest;
    }

    public void setProductInterest(String productInterest) {
        this.productInterest = productInterest;
    }

    public String getPhilosophy() {
        return philosophy;
    }

    public void setPhilosophy(String philosophy) {
        this.philosophy = philosophy;
    }

    public boolean isConsentForECB() {
        return consentForECB;
    }

    public void setConsentForECB(boolean consentForECB) {
        this.consentForECB = consentForECB;
    }

    public String getSalesComments() {
        return salesComments;
    }

    public void setSalesComments(String salesComments) {
        this.salesComments = salesComments;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(gender);
        parcel.writeString(mobileNumber);
        parcel.writeString(email);
        parcel.writeString(emirate);
        parcel.writeString(dateOfBirth);
        parcel.writeString(nationality);
        parcel.writeString(emiratesID);
        parcel.writeString(passportNumber);
        parcel.writeString(monthlySalary);
        parcel.writeString(employerName);
        parcel.writeString(productType);
        parcel.writeString(productInterest);
        parcel.writeString(philosophy);
        parcel.writeByte((byte) (consentForECB ? 1 : 0));
        parcel.writeString(salesComments);
        parcel.writeString(createDate);
        parcel.writeString(leadRef);
        parcel.writeString(leadApplicationStatus);
        parcel.writeString(subProductInterest);
        parcel.writeString(tier);
        parcel.writeString(sourceBy);
        parcel.writeString(userId);
    }


    public static final Parcelable.Creator<Lead> CREATOR
            = new Parcelable.Creator<Lead>() {
        public Lead createFromParcel(Parcel in) {
            return new Lead(in);
        }

        public Lead[] newArray(int size) {
            return new Lead[size];
        }
    };

}
