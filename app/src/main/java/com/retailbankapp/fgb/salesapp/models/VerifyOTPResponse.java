package com.retailbankapp.fgb.salesapp.models;



import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Pratik Das on 12/5/16.
 */
public class VerifyOTPResponse extends AbstractResponse{
    private String encKey;
    private String encIV;
    private String macKey;
    private String userId ;
    private String userName ;
    private String userRole ;
    private String userEmail ;
    private Integer eventCounter;

    private List<MenuItem> menuItems;
    private List<StaticData> staticDataList;

    private StaticDataHolder staticDataHolder;


    public List<StaticData> getStaticDataList() {
        return staticDataList;
    }

    public void addStaticData(StaticData staticData){
        if(staticDataList==null){
            staticDataList = new ArrayList<>();
        }
        staticDataList.add(staticData);
    }

    public Integer getEventCounter() {
        return eventCounter;
    }

    public void setEventCounter(Integer eventCounter) {
        this.eventCounter = eventCounter;
    }

    public void setStaticDataList(List<StaticData> staticDataList) {
        this.staticDataList = staticDataList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEncKey() {
        return encKey;
    }

    public void setEncKey(String encKey) {
        this.encKey = encKey;
    }

    public String getEncIV() {
        return encIV;
    }

    public void setEncIV(String encIV) {
        this.encIV = encIV;
    }

    public String getMacKey() {
        return macKey;
    }

    public void setMacKey(String macKey) {
        this.macKey = macKey;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public StaticDataHolder getStaticDataHolder() {
        return staticDataHolder;
    }

    public void setStaticDataHolder(StaticDataHolder staticDataHolder) {
        this.staticDataHolder = staticDataHolder;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
