package com.retailbankapp.fgb.salesapp.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by fgb on 11/24/16.
 */

public final class StaticDataContainer {
    private String staticDataType;
    private List<StaticData> staticDataList;



    public String getStaticDataType() {
        return staticDataType;
    }

    public void setStaticDataType(String staticDataType) {
        this.staticDataType = staticDataType;
    }

    public List<StaticData> getStaticDataList() {
        return staticDataList;
    }

    public void setStaticDataList(List<StaticData> staticDataList) {
        this.staticDataList = staticDataList;
    }

    public void addStaticData(final String entityName, StaticData staticData) {
        if(staticDataList==null){
            staticDataList = new ArrayList<>();
        }
        staticDataList.add(staticData);
    }




}
