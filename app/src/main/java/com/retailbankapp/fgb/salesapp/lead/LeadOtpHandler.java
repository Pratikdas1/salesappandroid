package com.retailbankapp.fgb.salesapp.lead;

import android.text.TextUtils;

import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.databinding.ActivityLeadOtpBinding;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.login.OtpActivity;

/**
 * Created by Anwar on 12/10/2016.
 */

public class LeadOtpHandler {
    private static final String TAG = LeadOtpHandler.class.getCanonicalName();
    private final LeadOtpActivity otpActivity;
    public String VERIFY_STRING = Constants.VERIFY ;
    private ActivityLeadOtpBinding mOtpBinding;

    public LeadOtpHandler(ActivityLeadOtpBinding mOtpBinding,LeadOtpActivity otpActivity) {
        this.mOtpBinding = mOtpBinding ;
        this.otpActivity = otpActivity ;
    }

    public  boolean validate(){
        Logger.v(TAG,"Otp Validate");
        mOtpBinding.layoutOtp.setError(null);
        if(TextUtils.isEmpty(String.valueOf(mOtpBinding.otpEditText.getText()))) {
            mOtpBinding.layoutOtp.setError(Constants.OTP_REQUIRED);
            return  false;
        }
        if(!mOtpBinding.consentCheckBox.isChecked()){
            otpActivity.showError(Constants.PLS_SELECT_CONSENT);
            return  false;
        }
        return  true ;
    }

    public void setErros() {
        mOtpBinding.layoutOtp.setError(Constants.INVALID_OTP);
    }

}
