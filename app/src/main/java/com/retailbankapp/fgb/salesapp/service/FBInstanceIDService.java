package com.retailbankapp.fgb.salesapp.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.models.SessionContext;
import com.retailbankapp.fgb.salesapp.models.UpdateRegisterDeviceRequest;
import com.retailbankapp.fgb.salesapp.models.UpdateRegisterDeviceResponse;
import com.retailbankapp.fgb.salesapp.stores.StoreFactory;
import com.retailbankapp.fgb.salesapp.stores.UserAPIStore;
import com.retailbankapp.fgb.salesapp.stores.UserStore;

/**
 * Created by Pratik Das on 1/21/17.
 */

public class FBInstanceIDService extends FirebaseInstanceIdService {
    UserStore userStore = StoreFactory.getStore().getUserStore();
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Logger.i( "Refreshed token: {}",refreshedToken);


        if (SessionContext.INSTANCE.getConvID()==null) return;
        
        final UpdateRegisterDeviceRequest request = new UpdateRegisterDeviceRequest();
        request.setDeviceToken(refreshedToken);
        userStore.updateDeviceRegistration(request, new ResponseListener<UpdateRegisterDeviceResponse>() {
            @Override
            public void onSuccess(UpdateRegisterDeviceResponse response) {
                SessionContext.INSTANCE.setDeviceToken(request.getDeviceToken());
            }

            @Override
            public void onFailure(UpdateRegisterDeviceResponse response) {

            }
        });
    }
}
