package com.retailbankapp.fgb.salesapp.dashboard;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.retailbankapp.fgb.salesapp.BR;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.models.Lead;
import com.retailbankapp.fgb.salesapp.models.MyLead;

import java.util.List;

/**
 * Created by o7548 on 12/18/2016.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.BindingHolder> {
    private static List<Lead> mLeadList;

    public NotificationAdapter(List<Lead> recyclerUsers) {
        this.mLeadList = recyclerUsers;
    }

    public static class BindingHolder extends RecyclerView.ViewHolder{
        private ViewDataBinding binding;

        public BindingHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }


    }

    public void add(Lead item) {
        mLeadList.add(item);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        mLeadList.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_table_row, parent, false);

        BindingHolder holder = new BindingHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
       /* if((position % 2 == 0)){
            holder.getBinding().getRoot().setBackgroundColor(ContextCompat.getColor(holder.getBinding().getRoot().getContext(),R.color.windowBackgroundColor));
        }else{
            holder.getBinding().getRoot().setBackgroundColor(ContextCompat.getColor(holder.getBinding().getRoot().getContext(),R.color.lightGreyColor));
        }*/
        final Lead lead = mLeadList.get(position);
        ImageView imageView = (ImageView)(holder.getBinding().getRoot().findViewById(R.id.statusImageView));
        if(lead.getLeadApplicationStatus().equalsIgnoreCase(AppConstants.LeadStatus.APPROVED)){
            imageView.setImageResource(R.drawable.ic_check_circle_green_24dp);
        }
        if(lead.getLeadApplicationStatus().equalsIgnoreCase(AppConstants.LeadStatus.REJECTED)){
            imageView.setImageResource(R.drawable.ic_rejected_red_24dp);
        }
        if(lead.getLeadApplicationStatus().equalsIgnoreCase(AppConstants.LeadStatus.PENDING)){
            imageView.setImageResource(R.drawable.ic_pending_yellow_24dp);
        }
       // lead.setFirstName(lead.getFirstName()+" "+lead.getLastName());
        holder.getBinding().setVariable(BR.lead, lead);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mLeadList.size();
    }
}