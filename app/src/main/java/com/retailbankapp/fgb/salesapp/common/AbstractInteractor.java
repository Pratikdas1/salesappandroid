package com.retailbankapp.fgb.salesapp.common;

import android.app.Activity;

/**
 * Created by fgb on 11/22/16.
 */

public abstract class AbstractInteractor {
    private AbstractPresenter presenter;

    private AbstractActivity activity;

    private  AbstractFragment abstractFragment ;

    public AbstractPresenter getPresenter() {
        return presenter;
    }

    public void setPresenter(AbstractPresenter presenter) {

        this.presenter = presenter;

    }

    public AbstractActivity getActivity() {
        return activity;
    }

    public void setActivity(AbstractActivity activity) {
        this.activity = activity;
    }

    public AbstractFragment getAbstractFragment() {
        return abstractFragment;
    }

    public void setAbstractFragment(AbstractFragment abstractFragment) {
        this.abstractFragment = abstractFragment;
    }
}
