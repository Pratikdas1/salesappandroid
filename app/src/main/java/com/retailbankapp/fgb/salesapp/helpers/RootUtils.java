package com.retailbankapp.fgb.salesapp.helpers;

/**
 * Created by O7548 on 2/8/2017.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class RootUtils {
    public static boolean isDeviceRooted() {
        boolean checkRoot1 = checkRootMethod1();
        boolean checkRoot2 = checkRootMethod2();
        boolean checkRoot3 = checkRootMethod3();
        return checkRoot1 || checkRoot2 || checkRoot3;
    }

    private static boolean checkRootMethod1() {
        String buildTags = android.os.Build.TAGS;
        boolean testKeysPresent = buildTags != null &&  buildTags.indexOf("test-keys")>-1;
        return testKeysPresent;
    }

    private static boolean checkRootMethod2() {
        String[] paths = { "/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    private static boolean checkRootMethod3() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[] { "/system/xbin/which", "su" });
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            if (in.readLine() != null) return true;
            return false;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }
}
