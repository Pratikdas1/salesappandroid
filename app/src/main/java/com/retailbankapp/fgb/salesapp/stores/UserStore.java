package com.retailbankapp.fgb.salesapp.stores;

import com.retailbankapp.fgb.salesapp.models.LogoutResponse;
import com.retailbankapp.fgb.salesapp.models.PingResponse;
import com.retailbankapp.fgb.salesapp.models.RegisterDeviceRequest;
import com.retailbankapp.fgb.salesapp.models.RegisterDeviceResponse;
import com.retailbankapp.fgb.salesapp.models.SendOTPRequest;
import com.retailbankapp.fgb.salesapp.models.SendOTPResponse;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.models.SettingsResponse;
import com.retailbankapp.fgb.salesapp.models.UpdateRegisterDeviceRequest;
import com.retailbankapp.fgb.salesapp.models.UpdateRegisterDeviceResponse;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPRequest;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPResponse;
import com.retailbankapp.fgb.salesapp.models.VerifyUserRequest;
import com.retailbankapp.fgb.salesapp.models.VerifyUserResponse;


/**
 * Created by Pratik Das on 11/22/16.
 */

public interface UserStore {

    void verifyUser(final VerifyUserRequest verifyUserRequest, final ResponseListener<VerifyUserResponse> serviceResponse);

    void verifyOTP(VerifyOTPRequest request, final ResponseListener<VerifyOTPResponse> serviceResponse);

    void sendOTP(SendOTPRequest request, ResponseListener<SendOTPResponse> responseListener);

    void registerDevice(RegisterDeviceRequest request, ResponseListener<RegisterDeviceResponse> responseListener);

    void logout(ResponseListener<LogoutResponse> responseListener);

    void updateDeviceRegistration(UpdateRegisterDeviceRequest request, ResponseListener<UpdateRegisterDeviceResponse> responseListener);

    void fetchSettings(ResponseListener<SettingsResponse> responseListener);

    void pingServer(final ResponseListener<PingResponse> serviceResponse);
}
