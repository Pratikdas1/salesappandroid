package com.retailbankapp.fgb.salesapp.models;

import java.util.List;

public class SettingsResponse extends AbstractResponse{
	private List<Setting> settings;

	public List<Setting> getSettings() {
		return settings;
	}

	public void setSettings(List<Setting> settings) {
		this.settings = settings;
	}
}
