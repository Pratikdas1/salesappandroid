package com.retailbankapp.fgb.salesapp.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fgb on 12/25/16.
 */

public final class LeadByCategoryHolder {

    private String category;
    private List<Lead> leads;
    private Float percentageTotal;

    public void addLead(Lead lead){
        if(leads==null){
            leads = new ArrayList<>();
        }
        leads.add(lead);
    }

    public Float getPercentageTotal() {
        return percentageTotal;
    }

    public void setPercentageTotal(Float percentageTotal) {
        this.percentageTotal = percentageTotal;
    }

    public Integer getNumberOfLeads(){
        return leads.isEmpty()? 0 : leads.size();
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Lead> getLeads() {
        return leads;
    }

    public void setLeads(List<Lead> leads) {
        this.leads = leads;
    }
}
