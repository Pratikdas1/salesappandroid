package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by fgb on 12/5/16.
 */

public  interface ResponseListener<T> {
    void  onSuccess(T response);

    void  onFailure(T response);
}
