package com.retailbankapp.fgb.salesapp.lead;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.microblink.activity.ScanCard;
import com.microblink.recognizers.BaseRecognitionResult;
import com.microblink.recognizers.RecognitionResults;
import com.microblink.recognizers.blinkid.mrtd.MRTDRecognitionResult;
import com.microblink.recognizers.blinkid.mrtd.MRTDRecognizerSettings;
import com.microblink.recognizers.settings.RecognitionSettings;
import com.microblink.recognizers.settings.RecognizerSettings;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.common.VipConfigurator;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;
import com.retailbankapp.fgb.salesapp.databinding.ActivityCreateLeadBinding;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.helpers.PropertyReader;
import com.retailbankapp.fgb.salesapp.models.Lead;
import com.retailbankapp.fgb.salesapp.models.SessionContext;
import com.retailbankapp.fgb.salesapp.service.GPSTracker;

public class UpdateLeadActivity extends AbstractActivity {
    private static final int SCAN_CODE = 3;
    private static final String TAG = UpdateLeadActivity.class.getCanonicalName();
    private ActivityCreateLeadBinding mActivityCreateLeadBinding;
    private GPSTracker mGpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_create_lead);
        mActivityCreateLeadBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_lead);

        setSupportActionBar(mActivityCreateLeadBinding.toolbar);
        ((AppTextView)mActivityCreateLeadBinding.toolbar.findViewById(R.id.toolbarTitle)).setText(Constants.UPDATE_LEAD);
        setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mActivityCreateLeadBinding.setLeadHandler(new CreateLeadHandler(mActivityCreateLeadBinding, this));
        Lead lead = null;
        if(getIntent() != null){
            lead = getIntent().getParcelableExtra(Constants.LEAD_DETAILS);
        }
        mActivityCreateLeadBinding.setLead(lead);
        VipConfigurator.INSTANCE.configure(this, new LeadPresenter(), new LeadInteractor());

        mGpsTracker = new GPSTracker(this);

        // check if GPS enabled
        if(mGpsTracker.canGetLocation()){

            double latitude = mGpsTracker.getLatitude();
            double longitude = mGpsTracker.getLongitude();
            SessionContext.INSTANCE.setLatitude(latitude);
            SessionContext.INSTANCE.setLongitude(longitude);
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            mGpsTracker.showSettingsAlert();
        }
    }


    public void saveLead(View view) {
        Logger.e(TAG,"In Save lead");
        CreateLeadHandler createLeadHandler = mActivityCreateLeadBinding.getLeadHandler();
        if (createLeadHandler != null && createLeadHandler.validate()) {
            LeadInteractor interactor = getInteractor(LeadInteractor.class);
            interactor.saveLead(mActivityCreateLeadBinding.getLead());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.createlead_menu, menu);
        return true;
    }


    private void scanEmiratesCard() {
        // Intent for ScanCard Activity
        Intent intent = new Intent(this, ScanCard.class);
        // set your licence key
        // obtain your licence key at http://microblink.com/login or
        // contact us at http://help.microblink.com
        intent.putExtra(ScanCard.EXTRAS_LICENSE_KEY, PropertyReader.INSTANCE.getProperty("blink.key"));

        RecognitionSettings settings = new RecognitionSettings();
        // setup array of recognition settings (described in chapter "Recognition
        // settings and results")
        settings.setRecognizerSettingsArray(setupSettingsArray());
        intent.putExtra(ScanCard.EXTRAS_RECOGNITION_SETTINGS, settings);
        // Starting Activity
        startActivityForResult(intent, SCAN_CODE);
    }

    private RecognizerSettings[] setupSettingsArray() {
        MRTDRecognizerSettings sett = new MRTDRecognizerSettings();
        // now add sett to recognizer settings array that is used to configure
        // recognition
        return new RecognizerSettings[] { sett };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SCAN_CODE) {
            if (resultCode == ScanCard.RESULT_OK && data != null) {
                // perform processing of the data here

                // for example, obtain parcelable recognition result
                Bundle extras = data.getExtras();
                RecognitionResults result = data.getParcelableExtra(ScanCard.EXTRAS_RECOGNITION_RESULTS);

                // get array of recognition results
                BaseRecognitionResult[] resultArray = result.getRecognitionResults();

                MRTDRecognitionResult mrtdRecognitionResult = null;
                if(resultArray.length > 0  && resultArray[0] instanceof MRTDRecognitionResult) {
                    mrtdRecognitionResult = (MRTDRecognitionResult) resultArray[0];
                    mActivityCreateLeadBinding.getLeadHandler().populateDataFromBlink(mrtdRecognitionResult);
                }
                //Logger.e("anwar",mrtdRecognitionResult.toString());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_scan:
                scanEmiratesCard();
                return true;
           /* case R.id.action_ocr:
                showError("Coming soon...");
                return true;*/
            case R.id.action_clear:
                mActivityCreateLeadBinding.getLeadHandler().clearFields();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGpsTracker.stopUsingGPS();
    }

    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(UpdateLeadActivity.this);
    }
}
