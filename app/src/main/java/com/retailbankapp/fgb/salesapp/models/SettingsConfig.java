package com.retailbankapp.fgb.salesapp.models;

import com.retailbankapp.fgb.salesapp.helpers.AppConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Pratik Das on 12/15/16.
 */

public enum SettingsConfig {
    INSTANCE;

    private Map<String,String> settings = new HashMap<>();

    private String url;

    public static String getValue(final AppConstants.SettingKeys key){
        return INSTANCE.settings.get(key.name());
    }


    public void populate(final List<Setting> settingList){
        if(settingList!=null) {
            for (Setting setting : settingList) {
                settings.put(setting.getName(), setting.getValue());
            }
        }
    }
}
