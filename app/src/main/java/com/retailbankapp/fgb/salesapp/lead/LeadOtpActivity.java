package com.retailbankapp.fgb.salesapp.lead;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.common.VipConfigurator;
import com.retailbankapp.fgb.salesapp.dashboard.NavigationActivity;
import com.retailbankapp.fgb.salesapp.databinding.ActivityLeadOtpBinding;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.CreateLeadRequest;
import com.retailbankapp.fgb.salesapp.models.SendOTPRequest;
import com.retailbankapp.fgb.salesapp.models.SendOTPResponse;
import com.retailbankapp.fgb.salesapp.models.SessionContext;

public class LeadOtpActivity extends AbstractActivity implements View.OnClickListener{

    private static final String TAG = LeadOtpActivity.class.getCanonicalName();

    private ActivityLeadOtpBinding mActivityLeadOtpBinding;
    private int mOtpDuration ;
    private LeadOtpActivity.OtpTimer mOtpTimer ;
    private CreateLeadRequest mCreateLeadRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_otp);
        mActivityLeadOtpBinding = DataBindingUtil.setContentView(this, R.layout.activity_lead_otp);
        VipConfigurator.INSTANCE.configure(this, new LeadPresenter(), new LeadInteractor());

        setSupportActionBar(mActivityLeadOtpBinding.toolbar);
        setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LeadInteractor leadInteractor = getInteractor(LeadInteractor.class);

        Intent intent = getIntent();
        if (null != intent) {
            mCreateLeadRequest = intent.getExtras().getParcelable(Constants.LEAD_DETAILS);
            mActivityLeadOtpBinding.setCreateLeadRequest(mCreateLeadRequest);
        }

        LeadOtpHandler otpHandler = new LeadOtpHandler(mActivityLeadOtpBinding,this);
        mActivityLeadOtpBinding.setOtpHandler(otpHandler);

        SendOTPRequest sendOTPRequest = new SendOTPRequest();
        if(TextUtils.isEmpty(mCreateLeadRequest.getLead().getLeadRef())){
            sendOTPRequest.setTxnCode(AppConstants.TxnCodes.CREATE_LEAD);
        }else{
            sendOTPRequest.setTxnCode(AppConstants.TxnCodes.UPDATE_LEAD);
        }
       // sendOTPRequest.setConvID(SessionContext.INSTANCE.getConvID());

        leadInteractor.generateOTP(sendOTPRequest);

        mActivityLeadOtpBinding.consentCheckBox.setOnClickListener(this);
    }

    public void startTimer(SendOTPResponse response){
        if(String.valueOf(mActivityLeadOtpBinding.verifyButton.getText()).equalsIgnoreCase(Constants.REGENERATE_OTP)){
            //restart timer
            restartTimer(response.getOtpDuration());
        }else {
            //Start timer for the first time
            mOtpDuration = response.getOtpDuration();
            mOtpDuration = mOtpDuration*1000 ;
            mOtpTimer = new LeadOtpActivity.OtpTimer(mOtpDuration,1000);
            mOtpTimer.start();
        }

    }
    public void regenerateOtp() {
        //mActivityOtpBinding.secondsTextView.setText("0");
        //mCircleProgressBar.setProgress(0f);
        mOtpTimer.cancel();
        mActivityLeadOtpBinding.verifyButton.setText(Constants.REGENERATE_OTP);
    }

    public void restartTimer(Integer newOtpDuration) {
        Logger.e(TAG,"reset timer");
        Logger.e(TAG,"In resend OTP METHOD");

        mCreateLeadRequest.setOtp(String.valueOf(newOtpDuration));
        //RESET FIELDS
        mActivityLeadOtpBinding.layoutOtp.setError(null);
        mActivityLeadOtpBinding.otpEditText.setText(null);
        mActivityLeadOtpBinding.verifyButton.setText(Constants.VERIFY);
        mActivityLeadOtpBinding.progressBar.setProgress(0);
        mOtpTimer = null ;
        mOtpDuration = newOtpDuration ;
        mOtpDuration = mOtpDuration*1000 ;
        mOtpTimer = new LeadOtpActivity.OtpTimer(mOtpDuration,1000);
        mOtpTimer.start();

    }

    public void setErrors() {
        mActivityLeadOtpBinding.getOtpHandler().setErros();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.consentCheckBox:
                if(mActivityLeadOtpBinding.consentCheckBox.isChecked()){
                    mActivityLeadOtpBinding.otpEditText.setFocusableInTouchMode(true);
                    mActivityLeadOtpBinding.otpEditText.setFocusable(true);
                    mActivityLeadOtpBinding.otpEditText.requestFocus();
                    showKeyboarForView(mActivityLeadOtpBinding.otpEditText);

                }else{
                    mActivityLeadOtpBinding.otpEditText.setFocusable(false);
                    hideKeyboarFromView(mActivityLeadOtpBinding.otpEditText);
                }
        }
    }


    private class OtpTimer extends CountDownTimer {

        private float mProgress = 0 ;
        private float mInterval = 0 ;

        public OtpTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            float divider = (mOtpDuration/1000f) ;
            this.mInterval = 100 / divider;
            mProgress = mInterval ;
            //Logger.e(TAG,mProgress+" mInterval:"+mInterval);
        }

        @Override
        public void onTick(long l) {
            //Logger.e(TAG,mProgress+"");
            mProgress = mInterval + mProgress ;
            //Logger.e(TAG,mProgress+"");
            mActivityLeadOtpBinding.progressBar.setProgress((int)mProgress);
            mActivityLeadOtpBinding.secondsTextView.setText(String.valueOf(l/1000));
        }

        @Override
        public void onFinish() {
            mActivityLeadOtpBinding.secondsTextView.setText("0");
            mActivityLeadOtpBinding.progressBar.setProgress(100);
            mActivityLeadOtpBinding.verifyButton.setText(Constants.REGENERATE_OTP);
        }
    }

    public void createLead(View view) {
        LeadInteractor leadInteractor = getInteractor(LeadInteractor.class);
        //If OTP Is expired verify button text will change RESEND_OTP
        if(String.valueOf(mActivityLeadOtpBinding.verifyButton.getText()).equalsIgnoreCase(Constants.REGENERATE_OTP)){
            SendOTPRequest sendOTPRequest = new SendOTPRequest();
            if(TextUtils.isEmpty(mCreateLeadRequest.getLead().getLeadRef())){
                sendOTPRequest.setTxnCode(AppConstants.TxnCodes.CREATE_LEAD);
            }else{
                sendOTPRequest.setTxnCode(AppConstants.TxnCodes.UPDATE_LEAD);
            }
           // sendOTPRequest.setConvID(SessionContext.INSTANCE.getConvID());

            leadInteractor.generateOTP(sendOTPRequest);
        }else{
            if(mActivityLeadOtpBinding.getOtpHandler().validate()){
                // convert imagePath to  encoded string if not decoded already
                /*if(!isDecoded){
                    new BitmapWorkerTask().execute();
                }else{
                    leadInteractor.createLead(mCreateLeadRequest);
                }*/
               // mCreateLeadRequest.setConvID(SessionContext.INSTANCE.getConvID());
                if(TextUtils.isEmpty(mCreateLeadRequest.getLead().getLeadRef())){
                    leadInteractor.createLead(mCreateLeadRequest);
                }else{
                    leadInteractor.updateLead(mCreateLeadRequest);
                }


            }
        }
    }

   // private boolean isDecoded = false ;
   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()) {
           case android.R.id.home:
               finish();
               return true;

           default:
               return super.onOptionsItemSelected(item);
       }
   }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mOtpTimer != null){
            mOtpTimer.cancel();
        }
       // mProgressBar = null ;
        mActivityLeadOtpBinding = null ;
    }
}
