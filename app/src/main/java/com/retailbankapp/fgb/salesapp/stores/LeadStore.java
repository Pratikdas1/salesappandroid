package com.retailbankapp.fgb.salesapp.stores;

import com.retailbankapp.fgb.salesapp.models.CreateLeadRequest;
import com.retailbankapp.fgb.salesapp.models.CreateLeadResponse;
import com.retailbankapp.fgb.salesapp.models.FetchLeadRequest;
import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.models.UpdateLeadRequest;
import com.retailbankapp.fgb.salesapp.models.UpdateLeadResponse;

/**
 * Created by fgb on 12/5/16.
 */

public interface LeadStore {
     void createLead(CreateLeadRequest createLeadRequest, ResponseListener<CreateLeadResponse> serviceResponse);
     void updateLead(CreateLeadRequest updateLeadRequest, ResponseListener<CreateLeadResponse> serviceResponse);
     void fetchLeads(FetchLeadRequest request, ResponseListener<FetchLeadResponse> serviceResponse);

}
