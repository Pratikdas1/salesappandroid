package com.retailbankapp.fgb.salesapp.stores.stubs;

import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.models.AppError;
import com.retailbankapp.fgb.salesapp.models.LogoutResponse;
import com.retailbankapp.fgb.salesapp.models.MenuItem;
import com.retailbankapp.fgb.salesapp.models.PingResponse;
import com.retailbankapp.fgb.salesapp.models.RegisterDeviceRequest;
import com.retailbankapp.fgb.salesapp.models.RegisterDeviceResponse;
import com.retailbankapp.fgb.salesapp.models.SendOTPRequest;
import com.retailbankapp.fgb.salesapp.models.SendOTPResponse;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.models.SessionContext;
import com.retailbankapp.fgb.salesapp.models.Setting;
import com.retailbankapp.fgb.salesapp.models.SettingsResponse;
import com.retailbankapp.fgb.salesapp.models.StaticData;
import com.retailbankapp.fgb.salesapp.models.UpdateRegisterDeviceRequest;
import com.retailbankapp.fgb.salesapp.models.UpdateRegisterDeviceResponse;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPRequest;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPResponse;
import com.retailbankapp.fgb.salesapp.models.VerifyUserRequest;
import com.retailbankapp.fgb.salesapp.models.VerifyUserResponse;
import com.retailbankapp.fgb.salesapp.stores.UserStore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fgb on 12/5/16.
 */

public enum  UserStoreStub implements UserStore {
    INSTANCE;

    static  List<StaticData> staticDataList = null;
    static {
        staticDataList = new ArrayList<>();
        staticDataList.add(new StaticData("PRODUCT_TYPE","PRS","Personal"));
        staticDataList.add(new StaticData("PRODUCT_TYPE","ACC","Accounts"));
        staticDataList.add(new StaticData("PRODUCT_TYPE","CC","Credit cards"));
        staticDataList.add(new StaticData("PRODUCT_TYPE","PBN","Product Bundle"));
        staticDataList.add(new StaticData("PRODUCT_TYPE","LN","Loan"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.PRS","PLN_STL","National Loans"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.PRS","PLN_TP","Personal Installment Loans"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.PRS","EMPTY","Personal Loans - STL"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.PRS","EMPTY","Personal Loans - TOP UP"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.PRS","EMPTY","National Loans"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.PRS","EMPTY","Personal Installment Loans"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.PRS","EMPTY","Personal Loans - STL"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.PRS","EMPTY","Personal Loans - TOP UP"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Supplementary Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Abu Dhabi Platinum Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Abu Dhabi Titanium Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Ferrari Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Gold Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Masdar Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","MY Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","FGB - Du Cards"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","MCFC Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Platinum Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Standard Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Supplementary Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Abu Dhabi Platinum Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Abu Dhabi Titanium Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Ferrari Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","FGB - Du Cards"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Gold Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Masdar Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","MCFC Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Platinum Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","MY Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.CC","EMPTY","Standard Card"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.PBN","EMPTY","Accelerator"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.ACC","EMPTY","Current Account"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.ACC","EMPTY","Savings Account"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.LN","EMPTY","Personal Installment Loan"));
        staticDataList.add(new StaticData("PRODUCT_TYPE.LN","EMPTY","Mortgage Loan"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","Titanium"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","General Platinum"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","Abu Dhabi Platinum"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","Masdar Infinite"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","Masdar Platinum"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","Ferrari Signature"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","Ferrari Infinite"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","Manchester City FC Titanium"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","Manchester City FC Platinum"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","Manchester City FC Infinite"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","FGB du Titanium Card"));
        staticDataList.add(new StaticData("ACC_CC_TYPE","EMPTY","FGB du Platinum Card"));
        staticDataList.add(new StaticData("COUNTRY","AND","ANDORRA"));
        staticDataList.add(new StaticData("COUNTRY","ARE","UAE"));
        staticDataList.add(new StaticData("COUNTRY","AFG","AFGANISTAN"));
        staticDataList.add(new StaticData("COUNTRY","ATG","ANTIGUA AND BARBUDA"));
        staticDataList.add(new StaticData("COUNTRY","AIA","ANGUILLA"));
        staticDataList.add(new StaticData("COUNTRY","ALB","ALBANIA"));
        staticDataList.add(new StaticData("COUNTRY","ARM","ARMENIA"));
        staticDataList.add(new StaticData("COUNTRY","AGO","ANGOLA"));
        staticDataList.add(new StaticData("COUNTRY","ATA","ANTARCTICA"));
        staticDataList.add(new StaticData("COUNTRY","ARG","ARGENTINA"));
        staticDataList.add(new StaticData("COUNTRY","ASM","AMERICAN SAMOA"));
        staticDataList.add(new StaticData("COUNTRY","AUT","AUSTRIA"));
        staticDataList.add(new StaticData("COUNTRY","AUS","AUSTRALIA"));
        staticDataList.add(new StaticData("COUNTRY","ABW","ARUBA"));
        staticDataList.add(new StaticData("COUNTRY","AZE","AZERBAIJAN"));
        staticDataList.add(new StaticData("COUNTRY","BIH","BOSNIA-HERZEGOVINA"));
        staticDataList.add(new StaticData("COUNTRY","BRB","BARBADOS"));
        staticDataList.add(new StaticData("COUNTRY","BGD","BANGLADESH"));
        staticDataList.add(new StaticData("COUNTRY","BEL","BELGIUM"));
        staticDataList.add(new StaticData("COUNTRY","BFA","BURKINA FASO"));
        staticDataList.add(new StaticData("COUNTRY","BGR","BULGARIA"));
        staticDataList.add(new StaticData("COUNTRY","BHR","BAHRAIN"));
        staticDataList.add(new StaticData("COUNTRY","BDI","BURUNDI"));
        staticDataList.add(new StaticData("COUNTRY","BEN","BENIN"));
        staticDataList.add(new StaticData("COUNTRY","BMU","BERMUDA"));
        staticDataList.add(new StaticData("COUNTRY","BRN","BRUNEI DARUSSALAM"));
        staticDataList.add(new StaticData("COUNTRY","BOL","BOLIVIA"));
        staticDataList.add(new StaticData("COUNTRY","BRA","BRAZIL"));
        staticDataList.add(new StaticData("COUNTRY","BHS","BAHAMAS"));
        staticDataList.add(new StaticData("COUNTRY","BTN","BHUTAN"));
        staticDataList.add(new StaticData("COUNTRY","BVT","BOUVET ISLAND"));
        staticDataList.add(new StaticData("COUNTRY","BWA","BOTSWANA"));
        staticDataList.add(new StaticData("COUNTRY","BLR","BELARUS"));
        staticDataList.add(new StaticData("COUNTRY","BLZ","BELIZE"));
        staticDataList.add(new StaticData("COUNTRY","CAN","CANADA"));
        staticDataList.add(new StaticData("COUNTRY","CCK","COCOS (KEELING) ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","COD","CONGO DEMOCRATIC REPUBLIC OF THE"));
        staticDataList.add(new StaticData("COUNTRY","CAF","CENTRAL AFRICAN REPUBLIC"));
        staticDataList.add(new StaticData("COUNTRY","COG","CONGO"));
        staticDataList.add(new StaticData("COUNTRY","CHE","SWITZERLAND"));
        staticDataList.add(new StaticData("COUNTRY","CIV","IVORY COAST"));
        staticDataList.add(new StaticData("COUNTRY","COK","COOK ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","CHL","CHILE"));
        staticDataList.add(new StaticData("COUNTRY","CMR","CAMEROON"));
        staticDataList.add(new StaticData("COUNTRY","CHN","PEOPLES REPUBLIC OF CHINA"));
        staticDataList.add(new StaticData("COUNTRY","COL","COLUMBIA"));
        staticDataList.add(new StaticData("COUNTRY","CRI","COSTA RICA"));
        staticDataList.add(new StaticData("COUNTRY","SRB","SERBIA"));
        staticDataList.add(new StaticData("COUNTRY","CUB","CUBA"));
        staticDataList.add(new StaticData("COUNTRY","CPV","CAPE VERDE"));
        staticDataList.add(new StaticData("COUNTRY","CXR","CHRISTMAS ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","CYP","CYPRUS"));
        staticDataList.add(new StaticData("COUNTRY","CZE","CZECH REPUBLIC"));
        staticDataList.add(new StaticData("COUNTRY","DEU","GERMANY"));
        staticDataList.add(new StaticData("COUNTRY","DJI","DJIBOUTI"));
        staticDataList.add(new StaticData("COUNTRY","DNK","DENMARK"));
        staticDataList.add(new StaticData("COUNTRY","DMA","DOMINICA"));
        staticDataList.add(new StaticData("COUNTRY","DOM","DOMINICAN REPUBLIC"));
        staticDataList.add(new StaticData("COUNTRY","DZA","ALGERIA"));
        staticDataList.add(new StaticData("COUNTRY","ECU","ECUADOR"));
        staticDataList.add(new StaticData("COUNTRY","EST","ESTONIA"));
        staticDataList.add(new StaticData("COUNTRY","EGY","EGYPT"));
        staticDataList.add(new StaticData("COUNTRY","ESH","WESTERN SAHARA"));
        staticDataList.add(new StaticData("COUNTRY","ERI","ERITREA"));
        staticDataList.add(new StaticData("COUNTRY","ESP","SPAIN"));
        staticDataList.add(new StaticData("COUNTRY","ETH","ETHIOPIA"));
        staticDataList.add(new StaticData("COUNTRY","FIN","FINLAND"));
        staticDataList.add(new StaticData("COUNTRY","FJI","FIJI"));
        staticDataList.add(new StaticData("COUNTRY","FLK","FALKLAND ISLANDS (MALVINAS)"));
        staticDataList.add(new StaticData("COUNTRY","FSM","MICRONESIA FEDERATED STATES OF"));
        staticDataList.add(new StaticData("COUNTRY","FRO","FAROE ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","FRA","FRANCE"));
        staticDataList.add(new StaticData("COUNTRY","GAB","GABON"));
        staticDataList.add(new StaticData("COUNTRY","GBR","GREAT BRITAIN"));
        staticDataList.add(new StaticData("COUNTRY","GRD","GRENADA"));
        staticDataList.add(new StaticData("COUNTRY","GEO","GEORGIA"));
        staticDataList.add(new StaticData("COUNTRY","GUF","FRENCH GUIANA"));
        staticDataList.add(new StaticData("COUNTRY","GGY","CHANNEL ISLANDS AND GUERNSEY"));
        staticDataList.add(new StaticData("COUNTRY","GHA","GHANA"));
        staticDataList.add(new StaticData("COUNTRY","GIB","GIBRALTAR"));
        staticDataList.add(new StaticData("COUNTRY","GRL","GREENLAND"));
        staticDataList.add(new StaticData("COUNTRY","GMB","GAMBIA"));
        staticDataList.add(new StaticData("COUNTRY","GIN","GUINEA"));
        staticDataList.add(new StaticData("COUNTRY","GLP","GUADELOUPE"));
        staticDataList.add(new StaticData("COUNTRY","GNQ","EQUATORIAL GUINEA"));
        staticDataList.add(new StaticData("COUNTRY","GRC","GREECE"));
        staticDataList.add(new StaticData("COUNTRY","SGS","SOUTH GEORGIA a SANDWICH IS"));
        staticDataList.add(new StaticData("COUNTRY","GTM","GUATEMALA"));
        staticDataList.add(new StaticData("COUNTRY","GUM","GUAM"));
        staticDataList.add(new StaticData("COUNTRY","GNB","GUINEA-BISSAU"));
        staticDataList.add(new StaticData("COUNTRY","GUY","GUYANA"));
        staticDataList.add(new StaticData("COUNTRY","HKG","HONG KONG"));
        staticDataList.add(new StaticData("COUNTRY","HMD","HEARD AND MCDONALD ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","HND","HONDURAS"));
        staticDataList.add(new StaticData("COUNTRY","HRV","CROATIA"));
        staticDataList.add(new StaticData("COUNTRY","HTI","HAITI"));
        staticDataList.add(new StaticData("COUNTRY","HUN","HUNGARY"));
        staticDataList.add(new StaticData("COUNTRY","IDN","INDONESIA"));
        staticDataList.add(new StaticData("COUNTRY","IRL","IRELAND"));
        staticDataList.add(new StaticData("COUNTRY","IMN","ISLE OF MAN"));
        staticDataList.add(new StaticData("COUNTRY","IND","INDIA"));
        staticDataList.add(new StaticData("COUNTRY","IOT","BRITISH INDIAN OCEAN TERRITORY"));
        staticDataList.add(new StaticData("COUNTRY","IRQ","IRAQ"));
        staticDataList.add(new StaticData("COUNTRY","IRN","IRAN (ISLAMIC REPUBLIC OF)"));
        staticDataList.add(new StaticData("COUNTRY","ISL","ICELAND"));
        staticDataList.add(new StaticData("COUNTRY","ITA","ITALY"));
        staticDataList.add(new StaticData("COUNTRY","IRN","IRAN (ISLAMIC REPUBLIC OF)"));
        staticDataList.add(new StaticData("COUNTRY","JEY","JERSEY"));
        staticDataList.add(new StaticData("COUNTRY","JAM","JAMAICA"));
        staticDataList.add(new StaticData("COUNTRY","JOR","JORDAN"));
        staticDataList.add(new StaticData("COUNTRY","JPN","JAPAN"));
        staticDataList.add(new StaticData("COUNTRY","KEN","KENYA"));
        staticDataList.add(new StaticData("COUNTRY","KGZ","KYRGYZSTAN"));
        staticDataList.add(new StaticData("COUNTRY","KHM","CAMBODIA"));
        staticDataList.add(new StaticData("COUNTRY","KIR","KIRIBATI"));
        staticDataList.add(new StaticData("COUNTRY","COM","COMORO ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","KNA","SAINT KITTS AND NEVIS"));
        staticDataList.add(new StaticData("COUNTRY","PRK","KOREA DEMOCRATIC PEOPLES REP. OF"));
        staticDataList.add(new StaticData("COUNTRY","KOR","KOREA REPUBLIC OF"));
        staticDataList.add(new StaticData("COUNTRY","KWT","KUWAIT"));
        staticDataList.add(new StaticData("COUNTRY","CYM","CAYMAN ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","KAZ","KAZAKSTAN"));
        staticDataList.add(new StaticData("COUNTRY","LAO","LAO PEOPLES DEMOCRATIC REPUBLIC"));
        staticDataList.add(new StaticData("COUNTRY","LBN","LEBANON"));
        staticDataList.add(new StaticData("COUNTRY","LCA","SAINT LUCIA"));
        staticDataList.add(new StaticData("COUNTRY","LIE","LIECHTENSTEIN"));
        staticDataList.add(new StaticData("COUNTRY","LKA","SRI LANKA"));
        staticDataList.add(new StaticData("COUNTRY","LBR","LIBERIA"));
        staticDataList.add(new StaticData("COUNTRY","LSO","LESOTHO"));
        staticDataList.add(new StaticData("COUNTRY","LTU","LITHUANIA"));
        staticDataList.add(new StaticData("COUNTRY","LUX","LUXEMBOURG"));
        staticDataList.add(new StaticData("COUNTRY","LVA","LATIVA"));
        staticDataList.add(new StaticData("COUNTRY","LBY","LIBYAN ARAB JAMAHIRIYA"));
        staticDataList.add(new StaticData("COUNTRY","MAR","MOROCCO"));
        staticDataList.add(new StaticData("COUNTRY","MCO","MONACO"));
        staticDataList.add(new StaticData("COUNTRY","MDA","MOLDOVA REPUBLIC OF"));
        staticDataList.add(new StaticData("COUNTRY","MNE","MONTENEGRO"));
        staticDataList.add(new StaticData("COUNTRY","MDG","MADAGASCAR"));
        staticDataList.add(new StaticData("COUNTRY","MHL","MARSHALL ISLANDS REPUBLIC OF"));
        staticDataList.add(new StaticData("COUNTRY","MKD","MACEDONIA"));
        staticDataList.add(new StaticData("COUNTRY","MLI","MALI"));
        staticDataList.add(new StaticData("COUNTRY","MMR","MYANMAR"));
        staticDataList.add(new StaticData("COUNTRY","MNG","MONGOLIA"));
        staticDataList.add(new StaticData("COUNTRY","MAC","MACAU"));
        staticDataList.add(new StaticData("COUNTRY","MNP","NORTHERN MARIANA ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","MTQ","MARTINIQUE"));
        staticDataList.add(new StaticData("COUNTRY","MRT","MAURITANIA"));
        staticDataList.add(new StaticData("COUNTRY","MSR","MONSERRAT"));
        staticDataList.add(new StaticData("COUNTRY","MLT","MALTA"));
        staticDataList.add(new StaticData("COUNTRY","MUS","MAURITIUS"));
        staticDataList.add(new StaticData("COUNTRY","MDV","MALDIVES"));
        staticDataList.add(new StaticData("COUNTRY","MWI","MALAWI"));
        staticDataList.add(new StaticData("COUNTRY","MEX","MEXICO"));
        staticDataList.add(new StaticData("COUNTRY","MYS","MALAYSIA"));
        staticDataList.add(new StaticData("COUNTRY","MOZ","MOZAMBIQUE"));
        staticDataList.add(new StaticData("COUNTRY","NAM","NAMIBIA"));
        staticDataList.add(new StaticData("COUNTRY","NCL","NEW CALEDONIA"));
        staticDataList.add(new StaticData("COUNTRY","NER","NIGER"));
        staticDataList.add(new StaticData("COUNTRY","NFK","NORFOLK ISLAND"));
        staticDataList.add(new StaticData("COUNTRY","NGA","NIGERIA"));
        staticDataList.add(new StaticData("COUNTRY","NIC","NICARAGUA"));
        staticDataList.add(new StaticData("COUNTRY","NLD","NETHERLANDS"));
        staticDataList.add(new StaticData("COUNTRY","NOR","NORWAY"));
        staticDataList.add(new StaticData("COUNTRY","NPL","NEPAL"));
        staticDataList.add(new StaticData("COUNTRY","NRU","NAURU"));
        staticDataList.add(new StaticData("COUNTRY","NIU","NIUE"));
        staticDataList.add(new StaticData("COUNTRY","NZL","NEW ZEALAND"));
        staticDataList.add(new StaticData("COUNTRY","OMN","OMAN"));
        staticDataList.add(new StaticData("COUNTRY","PAN","PANAMA"));
        staticDataList.add(new StaticData("COUNTRY","PER","PERU"));
        staticDataList.add(new StaticData("COUNTRY","PYF","FRENCH POLYNESIA"));
        staticDataList.add(new StaticData("COUNTRY","PNG","PAPUA NEW GUINEA"));
        staticDataList.add(new StaticData("COUNTRY","PHL","PHILIPPINES"));
        staticDataList.add(new StaticData("COUNTRY","PAK","PAKISTAN"));
        staticDataList.add(new StaticData("COUNTRY","POL","POLAND"));
        staticDataList.add(new StaticData("COUNTRY","SPM","ST. PIERRE AND MIQUELON"));
        staticDataList.add(new StaticData("COUNTRY","PCN","PITCAIRN"));
        staticDataList.add(new StaticData("COUNTRY","PRI","PUERTO RICO"));
        staticDataList.add(new StaticData("COUNTRY","PSE","PALESTINE"));
        staticDataList.add(new StaticData("COUNTRY","PRT","PORTUGAL"));
        staticDataList.add(new StaticData("COUNTRY","PLW","PALAU"));
        staticDataList.add(new StaticData("COUNTRY","PRY","PARAGUAY"));
        staticDataList.add(new StaticData("COUNTRY","QAT","QATAR"));
        staticDataList.add(new StaticData("COUNTRY","REU","REUNION"));
        staticDataList.add(new StaticData("COUNTRY","ROU","ROMANIA"));
        staticDataList.add(new StaticData("COUNTRY","RUS","RUSSIAN FEDERATION"));
        staticDataList.add(new StaticData("COUNTRY","RWA","RWANDA"));
        staticDataList.add(new StaticData("COUNTRY","SAU","SAUDI ARABIA"));
        staticDataList.add(new StaticData("COUNTRY","SLB","SOLOMON ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","SYC","SEYCHELLES"));
        staticDataList.add(new StaticData("COUNTRY","SDN","SUDAN"));
        staticDataList.add(new StaticData("COUNTRY","SWE","SWEDEN"));
        staticDataList.add(new StaticData("COUNTRY","SGP","SINGAPORE"));
        staticDataList.add(new StaticData("COUNTRY","SHN","ST. HELENA"));
        staticDataList.add(new StaticData("COUNTRY","SVN","SLOVENIA"));
        staticDataList.add(new StaticData("COUNTRY","SJM","SVALBARD AND JAN MAYEN ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","SVK","SLOVAKIA"));
        staticDataList.add(new StaticData("COUNTRY","SLE","SIERRA LEONE"));
        staticDataList.add(new StaticData("COUNTRY","SMR","SAN MARINO"));
        staticDataList.add(new StaticData("COUNTRY","SEN","SENEGAL"));
        staticDataList.add(new StaticData("COUNTRY","SOM","SOMALIA"));
        staticDataList.add(new StaticData("COUNTRY","SUR","SURINAME"));
        staticDataList.add(new StaticData("COUNTRY","STP","SAO TOME AND PRINCIPE"));
        staticDataList.add(new StaticData("COUNTRY","SLV","EL SALVADOR"));
        staticDataList.add(new StaticData("COUNTRY","SYR","SYRIAN ARAB REPUBLIC"));
        staticDataList.add(new StaticData("COUNTRY","SWZ","SWAZILAND"));
        staticDataList.add(new StaticData("COUNTRY","TCA","TURKS AND CAICOS ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","TCD","CHAD"));
        staticDataList.add(new StaticData("COUNTRY","ATF","FRENCH SOUTHERN TERRITORIES"));
        staticDataList.add(new StaticData("COUNTRY","TGO","TOGO"));
        staticDataList.add(new StaticData("COUNTRY","THA","THAILAND"));
        staticDataList.add(new StaticData("COUNTRY","TJK","TAJIKISTAN"));
        staticDataList.add(new StaticData("COUNTRY","TKL","TOKELAU"));
        staticDataList.add(new StaticData("COUNTRY","TKM","TURKMENISTAN"));
        staticDataList.add(new StaticData("COUNTRY","TUN","TUNISIA"));
        staticDataList.add(new StaticData("COUNTRY","TON","TONGA"));
        staticDataList.add(new StaticData("COUNTRY","TLS","EAST TIMOR"));
        staticDataList.add(new StaticData("COUNTRY","TUR","TURKEY"));
        staticDataList.add(new StaticData("COUNTRY","TTO","TRINIDAD AND TOBAGO"));
        staticDataList.add(new StaticData("COUNTRY","TUV","TUVALU"));
        staticDataList.add(new StaticData("COUNTRY","TWN","REPUBLIC OF CHINA (TAIWAN)"));
        staticDataList.add(new StaticData("COUNTRY","TZA","TANZANIA UNITED REPUBLIC OF"));
        staticDataList.add(new StaticData("COUNTRY","UKR","UKRAINE"));
        staticDataList.add(new StaticData("COUNTRY","UGA","UGANDA"));
        staticDataList.add(new StaticData("COUNTRY","UMI","UNITED STATES MINOR OUTLYING IS."));
        staticDataList.add(new StaticData("COUNTRY","USA","UNITED STATES OF AMERICA"));
        staticDataList.add(new StaticData("COUNTRY","URY","URUGUAY"));
        staticDataList.add(new StaticData("COUNTRY","UZB","UZBEKISTAN"));
        staticDataList.add(new StaticData("COUNTRY","VAT","HOLY SEE (VATICAN CITY STATE)"));
        staticDataList.add(new StaticData("COUNTRY","VCT","ST. VINCENT AND THE GRENADINES"));
        staticDataList.add(new StaticData("COUNTRY","VEN","VENEZUELA"));
        staticDataList.add(new StaticData("COUNTRY","VGB","BRITISH VIRGIN ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","VIR","VIRGIN ISLANDS US"));
        staticDataList.add(new StaticData("COUNTRY","VNM","VIETNAM"));
        staticDataList.add(new StaticData("COUNTRY","VUT","VANUATU"));
        staticDataList.add(new StaticData("COUNTRY","WLF","WALLIS AND FUTUNA ISLANDS"));
        staticDataList.add(new StaticData("COUNTRY","WSM","SAMOA"));
        staticDataList.add(new StaticData("COUNTRY","KOS","KOSOVO"));
        staticDataList.add(new StaticData("COUNTRY","YEM","YEMEN"));
        staticDataList.add(new StaticData("COUNTRY","MYT","MAYOTTE"));
        staticDataList.add(new StaticData("COUNTRY","ZAF","SOUTH AFRICA"));
        staticDataList.add(new StaticData("COUNTRY","ZMB","ZAMBIA"));
        staticDataList.add(new StaticData("COUNTRY","ZWE","ZIMBABWE"));
        staticDataList.add(new StaticData("EMIRATES","EMPTY","DUBAI"));
        staticDataList.add(new StaticData("EMIRATES","EMPTY","ABU DHABI"));
        staticDataList.add(new StaticData("EMIRATES","EMPTY","Ajman"));
        staticDataList.add(new StaticData("EMIRATES","EMPTY","Ras al-Khaimah"));
        staticDataList.add(new StaticData("EMIRATES","EMPTY","Umm al-Quwain"));
        staticDataList.add(new StaticData("EMIRATES","EMPTY","Sharjah"));
        staticDataList.add(new StaticData("EMIRATES","EMPTY","Fujairah"));
        staticDataList.add(new StaticData("GENDER","EMPTY","Male"));
        staticDataList.add(new StaticData("GENDER","EMPTY","Female"));
        staticDataList.add(new StaticData("PHILOSOPHY","EMPTY","Islamic"));
        staticDataList.add(new StaticData("PHILOSOPHY","EMPTY","Conventional"));
        staticDataList.add(new StaticData("TIER","EMPTY","Booster"));
        staticDataList.add(new StaticData("TIER","EMPTY","Booster Plus"));
        staticDataList.add(new StaticData("TIER","EMPTY","Turbo Booster"));
    }

    @Override
    public void verifyUser(VerifyUserRequest verifyUserRequest, ResponseListener<VerifyUserResponse> serviceResponse) {
        VerifyUserResponse verifyUserResponse = new VerifyUserResponse();

        if(verifyUserRequest.getUserName().equalsIgnoreCase("fgb") && verifyUserRequest.getPwd().equalsIgnoreCase("fgb123")){
            verifyUserResponse.setConvID("convid");
            verifyUserResponse.setOtpDuration(180);
            verifyUserResponse.setResult("success");
            serviceResponse.onSuccess(verifyUserResponse);
        }else{
            verifyUserResponse.setError(new AppError("INV","Invalid username or password"));
            serviceResponse.onFailure(verifyUserResponse);
        }

    }

    @Override
    public void verifyOTP(VerifyOTPRequest request, ResponseListener<VerifyOTPResponse> serviceResponse) {
        VerifyOTPResponse verifyOtpResponse = new VerifyOTPResponse();
        if(request.getOtp().equalsIgnoreCase("123456")){

            verifyOtpResponse.setUserEmail("test@gmail.com");
            verifyOtpResponse.setUserName("test");
            verifyOtpResponse.setUserId("5488");
            verifyOtpResponse.setEncIV("iv");
            verifyOtpResponse.setEncKey("key");
            verifyOtpResponse.setMacKey("mackey");
            verifyOtpResponse.setResult("success");

            List<MenuItem> menuItems = new ArrayList<>();
            MenuItem menuItem = new MenuItem();
            menuItem.setName(AppConstants.MenuKeys.CREATE_LEAD);
            menuItem.setIconID(0);
            menuItem.setDescription("Create Lead");
            menuItems.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setName(AppConstants.MenuKeys.MY_LEADS);
            menuItem.setIconID(1);
            menuItem.setDescription("My Leads");
            menuItems.add(menuItem);

            verifyOtpResponse.setMenuItems(menuItems);
            verifyOtpResponse.setStaticDataList(staticDataList);

            /*StaticDataHolder staticDataHolder = new StaticDataHolder();

            List<StaticDataContainer> staticData = new ArrayList<StaticDataContainer>();

            StaticDataContainer countryDataContainer = new StaticDataContainer();
            countryDataContainer.setStaticDataType("country");

            List<StaticData> countryList = new ArrayList<>();

            StaticDataContainer prodTypeDataContainer = new StaticDataContainer();
            prodTypeDataContainer.setStaticDataType("product_type");

            List<StaticData> productTypeList = new ArrayList<>();

            StaticData st = new StaticData("Auto","Auto");


            productTypeList.add(new StaticData("Auto","Auto"));
            productTypeList.add(new StaticData("Credit Card","Credit Card"));
            productTypeList.add(new StaticData("Personal","Personal"));
            productTypeList.add(new StaticData("Product Bundle","Product Bundle"));

            countryList.add(new StaticData("UAE","UE"));
            countryList.add(new StaticData("INDIA","IN"));
            countryList.add(new StaticData("PAKISTAN","PK"));
            countryList.add(new StaticData("SHRILANKA","SR"));
            countryList.add(new StaticData("BANGLADESH","BN"));
            countryList.add(new StaticData("COLUMBIA","CO"));
            countryList.add(new StaticData("COSTA RICA","CR"));
            countryList.add(new StaticData("CUBA","CU"));
            countryList.add(new StaticData("CAPE VERDE","CV"));
            countryList.add(new StaticData("CHRISTMAS ISLANDS","CX"));
            countryList.add(new StaticData("CYPRUS","CY"));
            countryList.add(new StaticData("CZECH REPUBLIC","CZ"));
            countryList.add(new StaticData("GERMANY","DE"));
            countryList.add(new StaticData("DJIBOUTI","DJ"));
            countryList.add(new StaticData("DENMARK","DK"));
            countryList.add(new StaticData("DOMINICA","DM"));
            countryList.add(new StaticData("DOMINICAN REPUBLIC","DO"));
            countryList.add(new StaticData("ALGERIA","DZ"));

            countryDataContainer.setStaticDataList(countryList);


            StaticDataContainer emiratesDataContainer = new StaticDataContainer();
            emiratesDataContainer.setStaticDataType("emirates");

            List<StaticData> emiratesList = new ArrayList<StaticData>();
            emiratesList.add(new StaticData("Abu Dhabi","Abu Dhabi"));
            emiratesList.add(new StaticData("Ajman","Ajman"));
            emiratesList.add(new StaticData("Dubai","Dubai"));
            emiratesList.add(new StaticData("Fujairah","Fujairah"));
            emiratesList.add(new StaticData("Ras al-Khaimah","Ras al-Khaimah"));
            emiratesList.add(new StaticData("Sharjah","Sharjah"));
            emiratesList.add(new StaticData("Umm al-Quwain","Umm al-Quwain"));
            emiratesDataContainer.setStaticDataList(emiratesList);


            staticData.add(countryDataContainer);
            staticData.add(emiratesDataContainer);
            staticData.add(prodTypeDataContainer);

            staticDataHolder.setStaticData(staticData);
            verifyOtpResponse.setStaticDataHolder(staticDataHolder);*/
            serviceResponse.onSuccess(verifyOtpResponse);
        }else{
            verifyOtpResponse.setError(new AppError(AppConstants.ErrorCodes.OTP_ATTEMPT_EXCEEDED,"Invalid OTP"));
            serviceResponse.onFailure(verifyOtpResponse);
        }

    }

    @Override
    public void sendOTP(SendOTPRequest request, ResponseListener<SendOTPResponse> serviceResponse) {
        SendOTPResponse sendOTPResponse = new SendOTPResponse();
        sendOTPResponse.setOtpDuration(50);
        sendOTPResponse.setResult("success");
        serviceResponse.onSuccess(sendOTPResponse);
    }

    @Override
    public void registerDevice(RegisterDeviceRequest request, ResponseListener<RegisterDeviceResponse> serviceResponse) {
        RegisterDeviceResponse registerDeviceResponse = new RegisterDeviceResponse();
        registerDeviceResponse.setEncKey("encKey");
        registerDeviceResponse.setEncIV("encIv");
        serviceResponse.onSuccess(registerDeviceResponse);
    }

    @Override
    public void updateDeviceRegistration(UpdateRegisterDeviceRequest request, ResponseListener<UpdateRegisterDeviceResponse> serviceResponse){
        UpdateRegisterDeviceResponse updateRegisterDeviceResponse = new UpdateRegisterDeviceResponse();
        updateRegisterDeviceResponse.setResult(AppConstants.Values.SUCCESS);
        serviceResponse.onSuccess(updateRegisterDeviceResponse);
    }

    @Override
    public void logout(ResponseListener<LogoutResponse> serviceResponse) {
        LogoutResponse logoutResponse = new LogoutResponse();
        logoutResponse.setResult("success");
        serviceResponse.onSuccess(logoutResponse);
    }

    @Override
    public void fetchSettings(ResponseListener<SettingsResponse> serviceResponse) {
        Setting setting = new Setting();
        SettingsResponse settingsResponse = new SettingsResponse();

        List<Setting> settingList = new ArrayList<>();
        settingList.add(new Setting(AppConstants.SettingKeys.INACTIVITY_TIMEOUT_MINS.name(),"2"));
        settingList.add(setting);
        settingsResponse.setSettings(settingList);
        serviceResponse.onSuccess(settingsResponse);

    }

    @Override
    public void pingServer(ResponseListener<PingResponse> serviceResponse) {

    }
}
