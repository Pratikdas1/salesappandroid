package com.retailbankapp.fgb.salesapp.lead;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;
import com.retailbankapp.fgb.salesapp.helpers.Constants;

public class LeadCreatedActivity extends AbstractActivity {

    private TabLayout mTabLayout ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_created);
        setTitle(null);
        if(getIntent() != null){
            String firstName  = getIntent().getStringExtra(Constants.FIRST_NAME);
            String leadRef  = getIntent().getStringExtra(Constants.LEAD_REF);
          //  ((AppTextView)findViewById(R.id.firstNameTextView)).setText(firstName);
            ((AppTextView)findViewById(R.id.leadIdTextView)).setText(leadRef);
        }
        mTabLayout = (TabLayout)findViewById(R.id.tabLayout);
        //mTabLayout.setSelectedTabIndicatorHeight(0);
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setTabSelection(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                setTabSelection(tab);
            }
        });
    }

    private void setTabSelection(TabLayout.Tab tab) {
        switch (tab.getPosition()){
            case 0 :
                startActivity(new Intent(LeadCreatedActivity.this,MyLeadsActivity.class));
                break;
            case  1 :
                startActivity(new Intent(LeadCreatedActivity.this,CreateLeadActivity.class));
                break;
        }
    }
}
