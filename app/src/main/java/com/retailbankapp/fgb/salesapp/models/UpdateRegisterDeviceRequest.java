package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by Pratik Das on 1/21/17.
 */

public final class UpdateRegisterDeviceRequest {
    private String deviceToken;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}
