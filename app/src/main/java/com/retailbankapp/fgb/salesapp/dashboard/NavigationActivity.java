package com.retailbankapp.fgb.salesapp.dashboard;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.common.VipConfigurator;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;
import com.retailbankapp.fgb.salesapp.customui.CustomTypefaceSpan;
import com.retailbankapp.fgb.salesapp.customui.FontCache;
import com.retailbankapp.fgb.salesapp.databinding.ActivityNavigationBinding;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.lead.CreateLeadActivity;
import com.retailbankapp.fgb.salesapp.models.FetchLeadRequest;
import com.retailbankapp.fgb.salesapp.models.SearchFilter;
import com.retailbankapp.fgb.salesapp.models.SessionContext;

import java.util.HashMap;
import java.util.List;

public class NavigationActivity extends AbstractActivity implements NavigationView.OnNavigationItemSelectedListener
        , TabLayout.OnTabSelectedListener {

    private static final String TAG = NavigationActivity.class.getCanonicalName();
    // FloatingActionButton fab ;
    private ActionBarDrawerToggle mActionBarToggle;
    private Handler mHandler;
    private ActivityNavigationBinding mActivityNavigationBinding;

    private boolean isActivityPaused;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_navigation);
        mActivityNavigationBinding = DataBindingUtil.setContentView(this, R.layout.activity_navigation);
        setSupportActionBar(mActivityNavigationBinding.toolbar);

        getSupportActionBar().setTitle(null);

        VipConfigurator.INSTANCE.configure(this, new NavigationPresenter(), new NavigationInteractor());
        mHandler = new Handler();
        mActionBarToggle = new ActionBarDrawerToggle(
                this, mActivityNavigationBinding.drawerLayout, mActivityNavigationBinding.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mActivityNavigationBinding.drawerLayout.addDrawerListener(mActionBarToggle);
        mActionBarToggle.syncState();

        mActivityNavigationBinding.navView.setNavigationItemSelectedListener(this);
        mActivityNavigationBinding.tabLayout.addOnTabSelectedListener(this);
        //Fetch menu based on role
        setMenu();

        fetchDashboardData();

        mActivityNavigationBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if (currentFragment instanceof Tab1Fragment) {
                    mActivityNavigationBinding.drawerLayout.openDrawer(GravityCompat.START);
                } else {
                    onBackPressed();
                }*/
                mActivityNavigationBinding.drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    private void fetchDashboardData() {
        //fetch dashboard data
        NavigationInteractor navigationInteractor = getInteractor(NavigationInteractor.class);
        FetchLeadRequest fetchLeadRequest = new FetchLeadRequest();
        SearchFilter searchFilter = new SearchFilter();
        searchFilter.setPageNumber(0);
        searchFilter.setNumberOfLeadsPerPage(0);
//        searchFilter.setNumberOfDaysPreceding(AppConstants.Values.LAST_7_DAYS);
        fetchLeadRequest.setSearchFilter(searchFilter);
        navigationInteractor.fetchLeads(fetchLeadRequest);
    }

    public void loadDashboard() {
        mActivityNavigationBinding.loadingTextView.setVisibility(View.GONE);
        // onReplaceFragment(new DashboardFragment(), false);
        //onReplaceFragment(new Tab1Fragment(), false);
        mActivityNavigationBinding.tabLayout.getTabAt(0).select();
    }


    private static HashMap<String, Integer> menuMap;

    static {
        menuMap = new HashMap<String, Integer>();
        menuMap.put(AppConstants.MenuKeys.CREATE_LEAD, R.drawable.ic_createlead_black_24dp);
        menuMap.put(AppConstants.MenuKeys.DASHBOARD, R.drawable.ic_home_white_24dp);
        menuMap.put(AppConstants.MenuKeys.MY_LEADS, R.drawable.ic_view_list_black_24dp);
        menuMap.put(AppConstants.MenuKeys.LOGOUT, R.drawable.ic_logout_24dp);
    }

    private void setMenu() {
        Menu menuItems = mActivityNavigationBinding.navView.getMenu();
        //AppApplication appApplication = (AppApplication) getApplication();
        int i = 1;
        //List<com.retailbankapp.fgb.salesapp.models.MenuItem> menuItemList = appApplication.getmVerifyOTPResponse().getMenuItems();
        List<com.retailbankapp.fgb.salesapp.models.MenuItem> menuItemList = SessionContext.INSTANCE.getMenuItems();
        if(menuItemList != null && !menuItemList.isEmpty()){
            for (com.retailbankapp.fgb.salesapp.models.MenuItem menuItem : menuItemList) {
                //  menuItems.add(new MenuItem());add(int groupId, int itemId, int order, CharSequence title)
               /* Logger.e("menu 1",menuItem.getDescription());
                Logger.e("menu 1",menuItem.getName());
                Logger.e("menu 1",menuMap.toString());*/
                MenuItem item = menuItems.add(0, i++, 0, menuItem.getDescription());
               // Logger.e("menu 2",new Boolean(item==null).toString());
                item.setIcon(menuMap.get(menuItem.getName().toUpperCase()));
            }
            //Adding logout menu at the end
            menuItems.add(0, i, 0, AppConstants.MenuKeys.LOGOUT).setIcon(R.drawable.ic_logout_24dp);
            menuItems.add(0, i, 0, AppConstants.MenuKeys.ABOUT).setIcon(R.drawable.ic_aboutblack_24dp);
            //Logger.e(TAG,menuItemList.toString());
            applyFontToMenuItem();
            View headerView = mActivityNavigationBinding.navView.getHeaderView(0);
            ((AppTextView) headerView.findViewById(R.id.userNameTextView)).setText(SessionContext.INSTANCE.getUserName());
            ((AppTextView) headerView.findViewById(R.id.userEmailTextView)).setText(SessionContext.INSTANCE.getUserEmail());
        }
    }

    private void applyFontToMenuItem() {
        Menu menuItems = mActivityNavigationBinding.navView.getMenu();
        for (int i = 0; i < menuItems.size(); i++) {
            MenuItem menuItem = menuItems.getItem(i);
            SpannableString mNewTitle = new SpannableString(menuItem.getTitle());
            mNewTitle.setSpan(new CustomTypefaceSpan("", FontCache.get(getString(R.string.app_font), this)), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            menuItem.setTitle(mNewTitle);
        }
    }


    @Override
    public void onBackPressed() {
        if (mActivityNavigationBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mActivityNavigationBinding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fm = getSupportFragmentManager();
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fm.getBackStackEntryCount() > 0) {
                fm.popBackStack();
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);

            } else {
               /* if (fragment instanceof DashboardFragment) {
                    super.onBackPressed();
                } else {
                    onReplaceFragment(new DashboardFragment(), false);
                }*/
                super.onBackPressed();
            }
            hideKeyboard();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        final NavigationInteractor navigationInteractor = getInteractor(NavigationInteractor.class);
        mActivityNavigationBinding.drawerLayout.closeDrawer(GravityCompat.START);
        // Handle navigation view item clicks here.
        final String title = String.valueOf(item.getTitle());
        //item.setChecked(true);
        // Handler for removing jerky animation of open drawer
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (title.toUpperCase()) {
                    case AppConstants.MenuDesc.CREATE_LEAD:
                        navigationInteractor.startCreateLeadScreen();
                        break;
                    case AppConstants.MenuDesc.MY_LEADS:
                        navigationInteractor.startMyLeadScreen();
                        break;
                    case AppConstants.MenuDesc.DASHBOARD:
                        fetchDashboardData();
                        //navigationInteractor.startCreateLeadScreen();
                        break;
                    case AppConstants.MenuDesc.ABOUT:
                        startActivity(new Intent(NavigationActivity.this, AboutUsActivity.class));
                        //navigationInteractor.startCreateLeadScreen();
                        break;
                    default:
                        //navigationInteractor.startMyLeadScreen();
                        navigationInteractor.logout();
                        break;
                }
            }
        }, 300);
        return true;
    }


    public void onReplaceFragment(Fragment fragment, boolean flag) {
        FragmentManager fragmentmanager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentmanager.beginTransaction();
        if (flag)
            fragmentTransaction.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
       /* FragmentManager fragmentmanager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentmanager.beginTransaction();
        String backStateName = fragment.getClass().getName();
        if (flag) {
            String s = fragmentmanager.getClass().getName();
            if (!fragmentmanager.popBackStackImmediate(s, 0)) {
                fragmentTransaction.replace(R.id.fragment_container, fragment, backStateName);
                fragmentTransaction.addToBackStack(s);
                fragmentTransaction.commit();
            }
            return;
        } else {
            if(flag)
                fragmentTransaction.setCustomAnimations(R.anim.slidein, R.anim.slideout);
            fragmentTransaction.replace(R.id.fragment_container, fragment, backStateName);
            fragmentTransaction.commit();
            return;
        }*/
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActionBarToggle = null;
        mHandler = null;
        mActivityNavigationBinding = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Logger.e(TAG, "In onPause method ...");
        isActivityPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.e(TAG, "In OnResume method ...");
        if (isActivityPaused) {
            mActivityNavigationBinding.tabLayout.getTabAt(0).select();
            isActivityPaused = false;
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        setTabSelection(tab);
    }

    private void setTabSelection(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                onReplaceFragment(new Tab1Fragment(), true);
                break;
            case 1:
                onReplaceFragment(new Tab2Fragment(), true);
                break;
            case 2:
                onReplaceFragment(new Tab3Fragment(), true);
                break;
            case 3:
                startActivity(new Intent(this, CreateLeadActivity.class));
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        setTabSelection(tab);
    }
}
