package com.retailbankapp.fgb.salesapp.lead;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.retailbankapp.fgb.salesapp.BR;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.models.Document;

import java.util.List;

/**
 * Created by o7548 on 12/15/2016.
 */


public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.BindingHolder> {
    private static List<Document> mDocumentList;
    private static DocumentLisener documentLisener ;

    public DocumentAdapter(List<Document> recyclerUsers, DocumentLisener documentLisener) {
        this.mDocumentList = recyclerUsers;
        this.documentLisener = documentLisener ;
    }

    public interface DocumentLisener {
        void clicked(int position,int operation);
    }

    public static class BindingHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ViewDataBinding binding;

        public BindingHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
            binding.getRoot().findViewById(R.id.deleteDoc).setOnClickListener(this);
            binding.getRoot().findViewById(R.id.viewDoc).setOnClickListener(this);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

        @Override
        public void onClick(View view) {
            int operation = 1 ;
            switch (view.getId()){
                case R.id.deleteDoc :
                    operation = Operation.DELETE.ordinal() ;
                    break;
                case R.id.viewDoc :
                    operation = Operation.VIEW.ordinal() ;
                    break;
            }
            documentLisener.clicked(getAdapterPosition(),operation);
        }
    }

    public enum Operation{
        VIEW , DELETE
    }
    public void add(Document item) {
        mDocumentList.add(item);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        mDocumentList.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.uploaddoc_item, parent, false);
        BindingHolder holder = new BindingHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        final Document document = mDocumentList.get(position);
        holder.getBinding().setVariable(BR.document, document);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mDocumentList.size();
    }
}