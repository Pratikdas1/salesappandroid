package com.retailbankapp.fgb.salesapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by fgb on 12/8/16.
 */

public final class Document implements Parcelable{
    private String fileName;
    private String type;
    private String contents;
    private byte[] data;
    public Document(){}

    public Document(Parcel in) {
        type = in.readString();
        contents = in.readString();
    }

    public static final Creator<Document> CREATOR = new Creator<Document>() {
        @Override
        public Document createFromParcel(Parcel in) {
            return new Document(in);
        }

        @Override
        public Document[] newArray(int size) {
            return new Document[size];
        }
    };

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(type);
        parcel.writeString(contents);
    }
}
