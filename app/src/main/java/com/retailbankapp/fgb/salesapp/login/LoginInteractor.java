package com.retailbankapp.fgb.salesapp.login;

import android.text.TextUtils;

import com.google.firebase.iid.FirebaseInstanceId;
import com.retailbankapp.fgb.salesapp.common.AbstractInteractor;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.AppError;
import com.retailbankapp.fgb.salesapp.models.RegisterDeviceRequest;
import com.retailbankapp.fgb.salesapp.models.RegisterDeviceResponse;
import com.retailbankapp.fgb.salesapp.models.SendOTPRequest;
import com.retailbankapp.fgb.salesapp.models.SendOTPResponse;
import com.retailbankapp.fgb.salesapp.models.ResponseListener;
import com.retailbankapp.fgb.salesapp.models.SettingsConfig;
import com.retailbankapp.fgb.salesapp.models.SessionContext;
import com.retailbankapp.fgb.salesapp.models.Setting;
import com.retailbankapp.fgb.salesapp.models.SettingsResponse;
import com.retailbankapp.fgb.salesapp.models.StaticDataHolder;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPRequest;
import com.retailbankapp.fgb.salesapp.models.VerifyOTPResponse;
import com.retailbankapp.fgb.salesapp.models.VerifyUserRequest;
import com.retailbankapp.fgb.salesapp.models.VerifyUserResponse;
import com.retailbankapp.fgb.salesapp.stores.StoreFactory;
import com.retailbankapp.fgb.salesapp.stores.UserStore;

import java.util.List;


/**
 * Created by fgb on 11/21/16.
 */

public class LoginInteractor extends AbstractInteractor{
    private UserStore userStore ;

    public LoginInteractor() {
        this.userStore = StoreFactory.getStore().getUserStore();
    }

    public void fetchSettings(){
        final LoginPresenter loginPresenter = (LoginPresenter)getPresenter();
        userStore.fetchSettings(new ResponseListener<SettingsResponse>() {
            @Override
            public void onSuccess(SettingsResponse response) {
                List<Setting> settings = response.getSettings();

                SettingsConfig.INSTANCE.populate(settings);
                loginPresenter.getmContext().setDISCONNECT_TIMEOUT(Long.parseLong(SettingsConfig.getValue(AppConstants.SettingKeys.INACTIVITY_TIMEOUT_MINS)));
                RegisterDeviceRequest registerDeviceRequest = new RegisterDeviceRequest();
                registerDeviceRequest.setDeviceID(SessionContext.INSTANCE.getDeviceID());
                registerDevice(registerDeviceRequest);
            }

            @Override
            public void onFailure(SettingsResponse response) {
                AppError errors = response.getError();
                loginPresenter.setErrors(errors.getDescription());
            }
        });
    }
    public void verifyUser(final VerifyUserRequest request){
        final LoginPresenter loginPresenter = (LoginPresenter)getPresenter();

        loginPresenter.showProgress(Constants.SIGNING_IN);
        if(TextUtils.isEmpty(SessionContext.INSTANCE.getConvID()) && TextUtils.isEmpty(SessionContext.INSTANCE.getEncKey())){

            userStore.fetchSettings(new ResponseListener<SettingsResponse>() {
                @Override
                public void onSuccess(SettingsResponse response) {
                    List<Setting> settings = response.getSettings();
                    //SettingsConfig.INSTANCE.setUrl(settings.get(0).getValue());
                    RegisterDeviceRequest registerDeviceRequest = new RegisterDeviceRequest();
                    registerDeviceRequest.setDeviceID(SessionContext.INSTANCE.getDeviceID());

                    userStore.registerDevice(registerDeviceRequest, new ResponseListener<RegisterDeviceResponse>() {
                        @Override
                        public void onSuccess(RegisterDeviceResponse response) {
                            Logger.i("register device ",response.toString());
                            loginPresenter.hideProgress();
                            SessionContext.INSTANCE.set(response.getEncIV(),response.getEncKey(),"","", response.getConvID(),"","");
                            verifyUser(request);
                        }

                        @Override
                        public void onFailure(RegisterDeviceResponse response) {
                            Logger.i("verify user resp ",response.toString());
                            loginPresenter.hideProgress();
                            loginPresenter.setErrors("Device registration failed!");

                        }
                    });
                }

                @Override
                public void onFailure(SettingsResponse response) {
                    loginPresenter.hideProgress();
                    AppError errors = response.getError();
                    LoginActivity loginActivity = (LoginActivity) getActivity();
                    loginActivity.showNetworkError();

                }
            });

        }else {
            userStore.verifyUser(request, new ResponseListener<VerifyUserResponse>() {
                @Override
                public void onSuccess(VerifyUserResponse response) {
                    //Logger.i("verify user resp ", response.toString());
                    loginPresenter.hideProgress();
                    if (AppConstants.Values.SUCCESS.equalsIgnoreCase(response.getResult())) {
                        loginPresenter.redirectOTPActivity(response);
                    } else {
                       // loginPresenter.setErrors(Constants.INVALID_USER);
                        AppError errors = response.getError();
                        loginPresenter.setErrors(errors.getDescription());
                    }
                }

                @Override
                public void onFailure(VerifyUserResponse response) {
                    loginPresenter.hideProgress();
                    AppError errors = response.getError();
                    loginPresenter.setErrors(errors.getDescription());
                }
            });
        }
    }


    public void verifyOTP(final VerifyOTPRequest request){
        final LoginPresenter loginPresenter = (LoginPresenter)getPresenter();
        loginPresenter.showProgress(Constants.VALIDATING);

        userStore.verifyOTP(request, new ResponseListener<VerifyOTPResponse>() {
            @Override
            public void onSuccess(VerifyOTPResponse response) {
                Logger.i("verify otp resp ",response.toString());

                loginPresenter.hideProgress();
                if(AppConstants.Values.SUCCESS.equalsIgnoreCase(response.getResult())){
                    String convID = SessionContext.INSTANCE.getConvID();
                    SessionContext.INSTANCE.set(response.getEncIV(),response.getEncKey(),response.getUserName(),response.getUserId(), convID, response.getMacKey(),response.getUserEmail());
                    SessionContext.INSTANCE.setEc(response.getEventCounter());
                    StaticDataHolder staticDataHolder = new StaticDataHolder();
                    staticDataHolder.setStaticDataList(response.getStaticDataList());

                    SessionContext.INSTANCE.setStaticDataHolder(staticDataHolder);

                    SessionContext.INSTANCE.setMenuItems(response.getMenuItems());
                    loginPresenter.redirectDashboard(response);
                }else{
                    AppError errors = response.getError();
                    if(AppConstants.ErrorCodes.OTP_ATTEMPT_EXCEEDED.equalsIgnoreCase(errors.getCode())){
                        loginPresenter.setErrors(errors.getDescription());
                        loginPresenter.regenerateOtp();
                    }else if(AppConstants.ErrorCodes.OTP_REGENARATION_EXCEEDED.equalsIgnoreCase(errors.getCode())){
                        loginPresenter.setErrors(errors.getDescription());
                        loginPresenter.redirectLogin();
                    }else{
                        loginPresenter.setErrors(errors.getDescription());
                    }

                }
            }

            @Override
            public void onFailure(VerifyOTPResponse response) {
                Logger.i("verify user resp ",response.toString());
                AppError errors = response.getError();
                loginPresenter.hideProgress();
                loginPresenter.setErrors(errors.getDescription());
            }
        });

    }

    public void resendOTP(SendOTPRequest request){
        final LoginPresenter loginPresenter = (LoginPresenter)getPresenter();
        loginPresenter.showProgress(Constants.PROCESSING);
        userStore.sendOTP(request, new ResponseListener<SendOTPResponse>() {
            @Override
            public void onSuccess(SendOTPResponse response) {
                loginPresenter.hideProgress();
                if(AppConstants.Values.SUCCESS.equalsIgnoreCase(response.getResult())){
                    Logger.i("verify user resp ",response.toString());
                    loginPresenter.restartTimer(response.getOtpDuration());
                }else{
                    AppError errors = response.getError();
                    if(AppConstants.ErrorCodes.OTP_REGENARATION_EXCEEDED.equalsIgnoreCase(errors.getCode())){
                        loginPresenter.redirectLogin();
                        loginPresenter.setErrors(errors.getDescription());
                    }
                }
            }

            @Override
            public void onFailure(SendOTPResponse response) {
                AppError errors = response.getError();
                loginPresenter.hideProgress();
                loginPresenter.setErrors(errors.getDescription());
            }
        });

    }

    public void registerDevice(RegisterDeviceRequest request){
        final LoginPresenter loginPresenter = (LoginPresenter)getPresenter();
       // loginPresenter.showProgress(Constants.PROCESSING);
        String token = FirebaseInstanceId.getInstance().getToken();
        request.setDeviceToken(token);
        userStore.registerDevice(request, new ResponseListener<RegisterDeviceResponse>() {
            @Override
            public void onSuccess(RegisterDeviceResponse response) {
                Logger.i("register device ",response.toString());

           //     loginPresenter.hideProgress();
                SessionContext.INSTANCE.set(response.getEncIV(),response.getEncKey(),"","", response.getConvID(),"","");

            }

            @Override
            public void onFailure(RegisterDeviceResponse response) {
                Logger.i("verify user resp ",response.toString());
             //   loginPresenter.hideProgress();
                loginPresenter.setErrors("Device registration failed!");

            }
        });

    }


    public void redirectDiagnosisScreen() {
        final LoginPresenter loginPresenter = (LoginPresenter)getPresenter();
        loginPresenter.redirectDiagnosisScreen();
    }
}
