package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by Pratik Das on 12/10/16.
 */

public final class RegisterDeviceRequest {
    private String deviceID;
    private String deviceToken;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }
}
