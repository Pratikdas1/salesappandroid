package com.retailbankapp.fgb.salesapp.lead;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.retailbankapp.fgb.salesapp.BR;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.Document;
import com.retailbankapp.fgb.salesapp.models.Lead;
import com.retailbankapp.fgb.salesapp.models.MyLead;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anwar on 12/18/2016.
 */

public class LeadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final String TAG = LeadAdapter.class.getCanonicalName();
    private List<Lead> mLeadList;
    private List<Lead> mFilterLeadList;
    private LeadListener leadListener ;
    private LeadFilter leadFilter;
    private boolean isShowStatus;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean loading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount ;

    public LeadAdapter(List<Lead> leadList, LeadListener leadListener ,boolean isShowStatus , RecyclerView recyclerView) {
        this.mLeadList = leadList;
        this.mFilterLeadList = leadList ;
        this.leadListener = leadListener ;
        this.isShowStatus = isShowStatus ;
        if(recyclerView.getLayoutManager() instanceof LinearLayoutManager){
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    Logger.e(TAG,"totalItemCount = "+totalItemCount+", lastVisibleItem = "+lastVisibleItem+" , loading= "+loading);
                    if(!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
                        if(onLoadMoreListener != null){
                            onLoadMoreListener.onLoadMore();
                        }
                       // loading = true;
                    }
                }
            });
        }
    }

    public void setmLeadList(List<Lead> mLeadList) {
        this.mLeadList = mLeadList;
    }

    @Override
    public Filter getFilter() {
        if (leadFilter == null) {
            leadFilter = new LeadFilter();
        }
        return leadFilter;
    }

    private class LeadFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            String searchString = String.valueOf(constraint).toUpperCase();

            if (constraint != null && constraint.length() > 0) {
                List filterList = new ArrayList();

                for (int i = 0; i < mFilterLeadList.size(); i++) {
                    Lead lead = mFilterLeadList.get(i);
                    boolean isAvaible = false ;
                    if(null != lead){
                        isAvaible =
                                (lead.getFirstName() != null &&   (lead.getFirstName().toUpperCase()).contains(searchString))
                                        ||  (lead.getLeadApplicationStatus() != null && (lead.getLeadApplicationStatus().toUpperCase()).contains(searchString))
                                        ||  (lead.getLastName()!= null && (lead.getLastName().toUpperCase()).contains(searchString))
                                        ||  (lead.getLeadRef() != null && (lead.getLeadRef().toUpperCase()).contains(searchString))
                                        ||  (lead.getEmployerName()!= null && (lead.getEmployerName().toUpperCase()).contains(searchString))
                                        ||  (lead.getEmail()!= null && (lead.getEmail().toUpperCase()).contains(searchString))
                                        ||  (lead.getMobileNumber() != null && (lead.getMobileNumber().toUpperCase()).contains(searchString)) ;
                        if (isAvaible) {
                            filterList.add(lead);
                        }
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mFilterLeadList.size();
                results.values = mFilterLeadList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            mLeadList = (List) filterResults.values;
            notifyDataSetChanged();
        }
    }

    public interface LeadListener {
        void leadClicked(Lead lead);
    }

    public  class BindingHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ViewDataBinding binding;

        public BindingHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
            binding.getRoot().findViewById(R.id.leadItemLayout).setOnClickListener(this);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

        @Override
        public void onClick(View view) {
            //mLeadList.get(getAdapterPosition());
            Logger.e(TAG,"Position clicked::"+getAdapterPosition());
            leadListener.leadClicked(mLeadList.get(getAdapterPosition()));
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder{
        public ProgressBar progressBar ;
        public ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar)itemView.findViewById(R.id.progressBar);
        }
    }

    public void add(Lead item) {
        mLeadList.add(item);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        mLeadList.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        mLeadList.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<Lead> list) {
        mLeadList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null ;
        RecyclerView.ViewHolder viewHolder = null ;
        if(viewType == VIEW_TYPE_ITEM){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lead_item, parent, false);
            viewHolder = new BindingHolder(view);
        }else{
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_item, parent, false);
            viewHolder = new ProgressViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return mLeadList.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOADING;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof BindingHolder){
            final Lead lead = mLeadList.get(position);
            ImageView imageView = (ImageView)(((BindingHolder)holder).getBinding().getRoot().findViewById(R.id.statusImageView));
            //isShowStatus flag is for In Assign lead page we do not want lead status,
            // we want status only in submitted leads screen . we are using same adapter at both place
            if(isShowStatus){
                if(AppConstants.LeadStatus.APPROVED.equalsIgnoreCase(lead.getLeadApplicationStatus())){
                    imageView.setImageResource(R.drawable.ic_check_circle_green_24dp);
                }
                if(AppConstants.LeadStatus.REJECTED.equalsIgnoreCase(lead.getLeadApplicationStatus())){
                    imageView.setImageResource(R.drawable.ic_rejected_red_24dp);
                }
                if(AppConstants.LeadStatus.PENDING.equalsIgnoreCase(lead.getLeadApplicationStatus())){
                    imageView.setImageResource(R.drawable.ic_pending_yellow_24dp);
                }
            }else{
                //In Assign lead page we do not want lead status , hence setting background to null
                imageView.setImageResource(0);
            }
            AppTextView textView = (AppTextView)(((BindingHolder)holder).getBinding().getRoot().findViewById(R.id.productTypeTextView));
            if(AppConstants.ProductType.ACCOUNT.equalsIgnoreCase(lead.getProductType())){
                textView.setBackgroundResource(R.drawable.circle_green);
                textView.setText("A");
            }else if(AppConstants.ProductType.LOAN.equalsIgnoreCase(lead.getProductType())){
                textView.setBackgroundResource(R.drawable.circle_normal);
                textView.setText("L");
            }else if(AppConstants.ProductType.PRODUCT_BUNDLE.equalsIgnoreCase(lead.getProductType())){
                textView.setBackgroundResource(R.drawable.circle_blue);
                textView.setText("PB");
            }else if(AppConstants.ProductType.PERSONAL.equalsIgnoreCase(lead.getProductType())){
                textView.setBackgroundResource(R.drawable.circle_yellow);
                textView.setText("P");
            }else if(AppConstants.ProductType.CREDIT_CARDS.equalsIgnoreCase(lead.getProductType())){
                textView.setBackgroundResource(R.drawable.circle_orange);
                textView.setText("C");
            }else{
                textView.setBackgroundResource(R.drawable.circle_grey);
                textView.setText("?");
            }
            ((BindingHolder)holder).getBinding().setVariable(BR.lead, lead);
            ((BindingHolder)holder).getBinding().executePendingBindings();
        }else{
            ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mLeadList == null ? 0 : mLeadList.size();
    }


    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener){
        this.onLoadMoreListener = onLoadMoreListener;
    }
    public interface OnLoadMoreListener {
        void onLoadMore();
    }
    public void setLoaded(boolean loaded) {
        loading = loaded;
    }
}