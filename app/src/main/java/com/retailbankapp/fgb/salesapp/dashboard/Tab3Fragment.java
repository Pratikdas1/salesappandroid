package com.retailbankapp.fgb.salesapp.dashboard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.retailbankapp.fgb.salesapp.AppApplication;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.Lead;

import java.util.ArrayList;
import java.util.List;


public class Tab3Fragment extends Fragment {

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private NotificationAdapter mLeadAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_tab3, container, false);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.tableRecyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        AppApplication appApplication = (AppApplication) getActivity().getApplication();
        FetchLeadResponse fetchLeadResponse = appApplication.getmDashboardData();
        if(null != fetchLeadResponse){
            if(fetchLeadResponse.getLeads() != null && !fetchLeadResponse.getLeads().isEmpty()){
                view.findViewById(R.id.noDataFoundTextView).setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                List<Lead> leadList = new ArrayList<Lead>();
                leadList.addAll(fetchLeadResponse.getLeads());
                mLeadAdapter = new NotificationAdapter(leadList);
                mRecyclerView.setAdapter(mLeadAdapter);
            }
        }
        return  view ;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRecyclerView = null ;
        mLayoutManager = null ;
        mLeadAdapter = null ;
    }
}
