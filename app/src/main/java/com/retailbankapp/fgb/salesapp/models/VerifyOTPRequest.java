package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by Pratik Das on 12/5/16.
 */
public class VerifyOTPRequest {
   // private String convID;
    private String otp;
    private String txnCode;

    public String getTxnCode() {
        return txnCode;
    }

    public void setTxnCode(String txnCode) {
        this.txnCode = txnCode;
    }


    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Override
    public String toString() {
        return "VerifyOTPRequest{" +
            //    "conversationID='" + convID + '\'' +
                ", otp='" + otp + '\'' +
                '}';
    }
}
