package com.retailbankapp.fgb.salesapp.models;

import com.retailbankapp.fgb.salesapp.helpers.AppConstants;

/**
 * Created by fgb on 12/5/16.
 */

public abstract class AbstractResponse {
    private String result;
    private AppError error;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public AppError getError() {
        return error;
    }

    public void setError(AppError error) {
        this.error = error;
    }
}
