package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by o7548 on 1/15/2017.
 */

public class PingResponse extends AbstractResponse{

    private String entity ;
    private Integer status ;

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
