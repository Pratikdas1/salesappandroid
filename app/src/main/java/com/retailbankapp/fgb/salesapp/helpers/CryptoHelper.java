package com.retailbankapp.fgb.salesapp.helpers;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Pratik Das on 11/27/16.
 */

public final class CryptoHelper {

    private static final String TAG = CryptoHelper.class.getCanonicalName();


    private static byte[] getDecodedBytes(final String encodedText){
        byte[] decodedBytes = null;
        try {
           decodedBytes = Base64.decode(encodedText,Base64.NO_WRAP);

        }catch(Exception e){

        }
        return decodedBytes;
    }
    public static String encrypt(final String strToEncrypt, final String secret, final String encIv){
        String encResponse = null;
        try
        {
            //setKey(secret);
            byte[] keyBytes = getDecodedBytes(secret);
            byte[] ivBytes = getDecodedBytes(encIv);

            SecretKeySpec skeySpec = new SecretKeySpec(keyBytes, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(ivBytes);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec,iv);


            encResponse = Base64.encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")), Base64.NO_WRAP);
            Logger.i(TAG,"enc "+encResponse);
            String decodeStr = new String(Base64.decode(encResponse, Base64.NO_WRAP));

            Logger.i(TAG,"decodestr "+decodeStr);
        }catch (Exception e){
            Logger.e(TAG,"Error while encrypting: " + e.toString());
        }
        return encResponse;
    }

    public static String decrypt(String strToDecrypt, String secret,final String encIv) {
        String dcrResponse = null;
        try
        {

            byte[] keyBytes = getDecodedBytes(secret);
            byte[] ivBytes = getDecodedBytes(encIv);

            SecretKeySpec skeySpec = new SecretKeySpec(keyBytes, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            IvParameterSpec iv = new IvParameterSpec(ivBytes);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec,iv);
            dcrResponse = new String(cipher.doFinal(Base64.decode(strToDecrypt, Base64.DEFAULT)));
        }
        catch (Exception e){
            Logger.e(TAG,"Error while decrypting: " + e.toString());
        }
        return dcrResponse;
    }

    public static String hmacDigest(String msg, String keyString, String algo) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);
            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));
            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (Exception e) {
            Logger.e(TAG,"Error generating hmac: " + e.toString());
        }
        return digest;
    }


}
