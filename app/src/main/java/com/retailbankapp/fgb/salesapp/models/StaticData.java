package com.retailbankapp.fgb.salesapp.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pratik Das on 11/24/16.
 */

/*

id entity     desc     parent
1  prodtype  loan
2  prodtype  card
3  prodtype  personal
4  prodsub  loan 1      1
5  prodsub  loan 2      1
6  prodsub  card 1      2
 */

public class StaticData {
    private String entityName ;
    private String code;
    private String description;

    public StaticData(String entityName, String code, String description) {
        this.entityName = entityName;
        this.code = code;
        this.description = description;
    }

    public StaticData(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
