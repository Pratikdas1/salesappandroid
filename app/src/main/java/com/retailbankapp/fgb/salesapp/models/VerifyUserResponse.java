package com.retailbankapp.fgb.salesapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by fgb on 11/24/16.
 */

public final class VerifyUserResponse extends AbstractResponse implements Parcelable{

    private Integer otpDuration;
    private String convID ;

    private VerifyUserResponse(Parcel in) {
        otpDuration = in.readInt();
        convID = in.readString();
    }

    public VerifyUserResponse(){}

    public Integer getOtpDuration() {
        return otpDuration;
    }

    public void setOtpDuration(Integer otpDuration) {
        this.otpDuration = otpDuration;
    }

    public String getConvID() {
        return convID;
    }

    public void setConvID(String convID) {
        this.convID = convID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(otpDuration);
        parcel.writeString(convID);
    }


    public static final Parcelable.Creator<VerifyUserResponse> CREATOR
            = new Parcelable.Creator<VerifyUserResponse>() {
        public VerifyUserResponse createFromParcel(Parcel in) {
            return new VerifyUserResponse(in);
        }

        public VerifyUserResponse[] newArray(int size) {
            return new VerifyUserResponse[size];
        }
    };

    @Override
    public String toString() {
        return "VerifyUserResponse{" +
                "otpDuration=" + otpDuration +
                ", conversationID='" + convID + '\'' +
                '}';
    }
}



