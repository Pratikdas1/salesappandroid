package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by O7548 on 1/2/2017.
 */

public class ProductSelection {

    private String productType ;
    private  String productInterest ;
    private  String philosophy ;
    private String action ;

    public ProductSelection(){}

    public ProductSelection(String action){
        this.action = action ;
    }
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getProductType() {

        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductInterest() {
        return productInterest;
    }

    public void setProductInterest(String productInterest) {
        this.productInterest = productInterest;
    }

    public String getPhilosophy() {
        return philosophy;
    }

    public void setPhilosophy(String philosophy) {
        this.philosophy = philosophy;
    }
}
