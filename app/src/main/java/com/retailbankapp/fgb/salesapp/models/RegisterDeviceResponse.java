package com.retailbankapp.fgb.salesapp.models;



/**
 * Created by fgb on 12/10/16.
 */

public final class RegisterDeviceResponse extends AbstractResponse {
    private String encIV;
    private String encKey;
    private String convID;

    public String getConvID() {
        return convID;
    }

    public void setConvID(String convID) {
        this.convID = convID;
    }

    public String getEncIV() {
        return encIV;
    }

    public void setEncIV(String encIV) {
        this.encIV = encIV;
    }

    public String getEncKey() {
        return encKey;
    }

    public void setEncKey(String encKey) {
        this.encKey = encKey;
    }
}
