package com.retailbankapp.fgb.salesapp.customui;

import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;

public class CameraActivity extends AbstractActivity {


    private CameraPreview mCameraPreview;
    private Camera mCamera;
    private FrameLayout mCameraPreviewLayout;

    private FloatingActionButton mCaptureButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera);

        mCaptureButton = (FloatingActionButton) findViewById(R.id.captureButton);
        //initailizeCamera();
    }

    private void initailizeCamera() {
        mCamera = getCameraInstance();
        mCamera.startPreview();
        mCameraPreviewLayout = (FrameLayout) findViewById(R.id.camera_preview);
        mCameraPreview = new CameraPreview(this, mCamera);
        mCameraPreviewLayout.addView(mCameraPreview);
    }


    private Camera getCameraInstance() {
        Camera mCamera = null;
        try {
            mCamera = Camera.open();
            //mCamera.setDisplayOrientation(90);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mCamera;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
            mCameraPreview.getHolder().removeCallback(null);
            mCameraPreview.getHolder().removeCallback(mCameraPreview);
        }
        if(mCameraPreview != null){
            mCameraPreview.destroyDrawingCache();
            mCameraPreview = null ;
            mCameraPreviewLayout.destroyDrawingCache();
            mCameraPreviewLayout.invalidate();
            mCameraPreviewLayout = null ;
        }
    }


    public void takePicture(View view) {
        mCamera.takePicture(null, null, pictureCallback);
    }


    Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            Intent intent = new Intent();
            //intent.putExtra("pic",data);
            ImageHolder.INSTANCE.setImage(data);
            setResult(RESULT_OK,intent);
            finish();

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        initailizeCamera();
        mCameraPreview.setVisibility(View.VISIBLE);//this to fix freeze.
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mCameraPreview.setVisibility(View.GONE);//this to fix freeze.
        releaseCamera();
    }
}
