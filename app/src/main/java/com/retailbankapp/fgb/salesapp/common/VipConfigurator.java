package com.retailbankapp.fgb.salesapp.common;

/**
 * Created by fgb on 11/22/16.
 */

public enum  VipConfigurator {
    INSTANCE;
    public void configure(AbstractActivity activity, AbstractPresenter presenter, AbstractInteractor interactor){
        //router.activity = activity
       // presenter.router = router;
        interactor.setPresenter(presenter);
        presenter.setmContext(activity);
        activity.setInteractor(interactor);

    }

    public void configure(AbstractFragment abstractFragment, AbstractPresenter presenter, AbstractInteractor interactor){
        interactor.setPresenter(presenter);
         presenter.setmContext((AbstractActivity) abstractFragment.getContext());
        abstractFragment.setInteractor(interactor);
        interactor.setPresenter(presenter);
    }
}
