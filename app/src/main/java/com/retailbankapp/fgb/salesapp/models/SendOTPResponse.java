package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by Pratik Das on 11/24/16.
 */

public final class SendOTPResponse extends AbstractResponse{

    private Integer otpDuration;


    public Integer getOtpDuration() {
        return otpDuration;
    }

    public void setOtpDuration(Integer otpDuration) {
        this.otpDuration = otpDuration;
    }


}



