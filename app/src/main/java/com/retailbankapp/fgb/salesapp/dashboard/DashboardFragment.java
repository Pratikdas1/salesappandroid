package com.retailbankapp.fgb.salesapp.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.customui.AppViewPager;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.lead.CreateLeadActivity;

/**
 * Created by O7548 on 12/6/2016.
 */

public class DashboardFragment extends Fragment {

    private AppViewPager mViewPager;
    private  DashboardAdapter mDashboardAdapter;
    private TabLayout mTabLayout ;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        mViewPager = (AppViewPager) view.findViewById(R.id.dashboardPager);
        mViewPager.setPagingEnabled(false);
        setupAdapter();
        mTabLayout = ((TabLayout)view.findViewById(R.id.tabLayout));
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();
        return view ;
    }

    private void setupAdapter() {
        mDashboardAdapter = new DashboardAdapter(getChildFragmentManager());
        mDashboardAdapter.addFragment(new Tab1Fragment(), "");
        mDashboardAdapter.addFragment(new Tab2Fragment(), "");
        mDashboardAdapter.addFragment(new Tab3Fragment(), "");
   //     mDashboardAdapter.addFragment(new BlankFragment(), "");
        mViewPager.setAdapter(mDashboardAdapter);
    }


    @Override
    public void onResume() {
        super.onResume();
        if(mTabLayout != null){
            mTabLayout.getTabAt(0).select();
        }
    }

    private void setupTabIcons(){
        mTabLayout.getTabAt(0).setIcon(R.drawable.ic_donut_large_white_24dp);
        mTabLayout.getTabAt(1).setIcon(R.drawable.ic_bar_white_24dp);
        mTabLayout.getTabAt(2).setIcon(R.drawable.ic_view_list_white_24dp);
     //   mTabLayout.getTabAt(3).setIcon(R.drawable.ic_createlead_white_24dp);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewPager = null ;
        mDashboardAdapter = null;
        mTabLayout = null;
    }
}
