package com.retailbankapp.fgb.salesapp.helpers;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Pratik Das on 12/5/16.
 */

public enum PropertyReader {
    INSTANCE;

    private Properties props=null;

    public void loadProperties(Context context){
        try {
            if(props == null){
                props = new Properties();
                AssetManager assetManager = context.getAssets();
                InputStream inputStream = assetManager.open("config.properties");
                props.load(inputStream);
            }
        }catch(Exception e){

        }
    }

    public String getProperty(final String propertyName, Context context){
            if(props == null){
                loadProperties(context);
            }
            return props==null?null:props.getProperty(propertyName);
    }

    public String getProperty(final String propertyName){
        return props==null?null:props.getProperty(propertyName);
    }
}
