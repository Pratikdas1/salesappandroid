package com.retailbankapp.fgb.salesapp.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;
import com.retailbankapp.fgb.salesapp.dashboard.NavigationInteractor;
import com.retailbankapp.fgb.salesapp.dashboard.NavigationPresenter;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.login.LoginActivity;

/**
 * Created by fgb on 11/22/16.
 */

public abstract class AbstractActivity extends AppCompatActivity {

    private ProgressDialog mProgressBar;
    private String TAG = AbstractActivity.class.getCanonicalName();

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().getAttributes().windowAnimations = R.style.Fade;
    }

    private volatile AbstractInteractor interactor;

    public AbstractInteractor getInteractor() {
        return interactor;
    }

    public <T> T getInteractor(Class interactorClass) {
        return (T) interactor;
    }

    public void setInteractor(AbstractInteractor interactor) {

        this.interactor = interactor;
        this.interactor.setActivity(this);
    }

    public void showProgresDialog(String msg) {
        mProgressBar = new ProgressDialog(this);
        if(msg == null)
            mProgressBar.setMessage(getString(R.string.processing_please_wait));
        else
            mProgressBar.setMessage(msg);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setCancelable(false);
        mProgressBar.show();
    }

    public void hideProgressDialog() {
        mProgressBar.dismiss();
    }

    public void showProgress(String msg) {
        mProgressBar = new ProgressDialog(this);
        mProgressBar.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mProgressBar.setCancelable(false);
        mProgressBar.show();
        mProgressBar.setContentView(R.layout.progress_layout);
        ((ProgressBar)mProgressBar.findViewById(R.id.progressBar1))
                .getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(this, R.color.redColor), PorterDuff.Mode.SRC_IN);
        if(msg != null)
            ((AppTextView)mProgressBar.findViewById(R.id.progressText)).setText(msg);
    }

    public void hideProgress() {
        mProgressBar.dismiss();
    }

    public void showNetworkError() {
       // Snackbar.make(view, getString(R.string.network_connection_error), Snackbar.LENGTH_LONG).setAction("Action", null).show();
        Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), getString(R.string.network_connection_error), Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this,R.color.textColor));
        snackbarView.setBackgroundColor(ContextCompat.getColor(this,android.R.color.holo_red_dark));
        snackbar.setDuration(1500);
        snackbar.show();
    }

    public void showError(String msg) {
        // Snackbar.make(view, getString(R.string.network_connection_error), Snackbar.LENGTH_LONG).setAction("Action", null).show();
        Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), msg, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this,R.color.textColor));
        snackbarView.setBackgroundColor(ContextCompat.getColor(this,android.R.color.holo_red_dark));
        snackbar.setDuration(2000);
        snackbar.show();
    }

    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void hideKeyboarFromView(View view) {
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void showKeyboarForView(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.msg_back_again), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 3000);
    }

    public void setDISCONNECT_TIMEOUT(long DISCONNECT_TIMEOUT) {
        this.DISCONNECT_TIMEOUT = DISCONNECT_TIMEOUT * 60 * 1000 ;
    }

    private long DISCONNECT_TIMEOUT = 15*60*1000; // 5 min = 5 * 60 * 1000 ms

    private Handler disconnectHandler = new Handler(){
        public void handleMessage(Message msg) {
        }
    };

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            logout();
        }
    };

    public void sessionExpired(String descrption){
        showError(descrption);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                logout();
            }
        },3000);
    }

    public void logout() {
        Logger.e(TAG,"In Logout method");
        VipConfigurator.INSTANCE.configure(this,new NavigationPresenter(),new NavigationInteractor());
        final NavigationInteractor navigationInteractor = getInteractor(NavigationInteractor.class);
        navigationInteractor.logout();
    }

    public void resetDisconnectTimer(){
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer(){
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction(){
        resetDisconnectTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }
}
