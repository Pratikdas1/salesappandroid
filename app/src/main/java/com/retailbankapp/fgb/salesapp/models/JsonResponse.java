package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by fgb on 11/27/16.
 */

public class JsonResponse {
    private String response;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
