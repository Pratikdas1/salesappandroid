package com.retailbankapp.fgb.salesapp.models;

/**
 * Created by fgb on 12/14/16.
 */

public final class MenuItem {
    private String name;
    private String description;
    private Integer iconID;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIconID() {
        return iconID;
    }

    public void setIconID(Integer iconID) {
        this.iconID = iconID;
    }
}
