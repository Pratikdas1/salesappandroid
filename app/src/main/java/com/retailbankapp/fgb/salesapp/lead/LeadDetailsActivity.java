package com.retailbankapp.fgb.salesapp.lead;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractActivity;
import com.retailbankapp.fgb.salesapp.customui.AppTextView;
import com.retailbankapp.fgb.salesapp.databinding.ActivityLeadDetailsBinding;
import com.retailbankapp.fgb.salesapp.helpers.Constants;
import com.retailbankapp.fgb.salesapp.models.Lead;

public class LeadDetailsActivity extends AbstractActivity {

    private ActivityLeadDetailsBinding mActivityLeadDetailsBinding ;
    private boolean isEditable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_lead_details);
        mActivityLeadDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_lead_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Lead lead = null ;
        if(getIntent() != null){
            lead = getIntent().getParcelableExtra(Constants.LEAD_DETAILS);
            isEditable = getIntent().getBooleanExtra(Constants.EDITABLE,false);
        }
        mActivityLeadDetailsBinding.setLead(lead);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(isEditable){
            getMenuInflater().inflate(R.menu.leaddetails_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_edit:
                Lead lead = mActivityLeadDetailsBinding.getLead();
                Intent intent = new Intent(this,UpdateLeadActivity.class);
                intent.putExtra(Constants.LEAD_DETAILS,lead);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(LeadDetailsActivity.this);
    }


    public void dialPhoneNumber(View view){
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        AppTextView appTextView = (AppTextView)view;

        if(!TextUtils.isEmpty(String.valueOf(appTextView.getText()))) {
            callIntent.setData(Uri.parse("tel:+" + String.valueOf(appTextView.getText())));
            startActivity(callIntent);
        }
    }
    public void composeEmail(View view){
        AppTextView appTextView = (AppTextView)view;

        if(!TextUtils.isEmpty(String.valueOf(appTextView.getText()))) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" +String.valueOf(appTextView.getText())));
            //emailIntent.putExtra(Intent.EXTRA_HTML_TEXT, body); //If you are using HTML in your body text
            startActivity(emailIntent);
        }
    }

}
