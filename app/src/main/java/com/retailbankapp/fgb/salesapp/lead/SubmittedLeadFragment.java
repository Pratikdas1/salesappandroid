package com.retailbankapp.fgb.salesapp.lead;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.retailbankapp.fgb.salesapp.AppApplication;
import com.retailbankapp.fgb.salesapp.R;
import com.retailbankapp.fgb.salesapp.common.AbstractFragment;
import com.retailbankapp.fgb.salesapp.common.VipConfigurator;
import com.retailbankapp.fgb.salesapp.helpers.AppConstants;
import com.retailbankapp.fgb.salesapp.helpers.Logger;
import com.retailbankapp.fgb.salesapp.models.FetchLeadRequest;
import com.retailbankapp.fgb.salesapp.models.FetchLeadResponse;
import com.retailbankapp.fgb.salesapp.models.Lead;
import com.retailbankapp.fgb.salesapp.models.LeadByCategoryHolder;
import com.retailbankapp.fgb.salesapp.models.SearchFilter;

import java.util.ArrayList;
import java.util.List;


public class SubmittedLeadFragment extends AbstractFragment implements LeadAdapter.LeadListener
        , SearchView.OnQueryTextListener, SwipeRefreshLayout.OnRefreshListener {


    private static final String TAG = SubmittedLeadFragment.class.getCanonicalName();
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private LeadAdapter mLeadAdapter;
    private List<Lead> mLeadArrayList;
    private SearchView mSearchView;
    private MenuItem mSearchMenuItem;
    private SwipeRefreshLayout mSwipeContainer;

    private int pageNumber;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_submitted_lead, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.submitedLeadRecyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // mRecyclerView.setNestedScrollingEnabled(false);

        mLeadArrayList = new ArrayList<Lead>();
        // specify an adapter (see also next example)
        AppApplication appApplication = (AppApplication) getActivity().getApplication();
        FetchLeadResponse fetchLeadResponse = appApplication.getmMyLeadData();
        List<Lead> leadList = getLeads(fetchLeadResponse);
        if (leadList != null && !leadList.isEmpty()) {
            mLeadArrayList.addAll(leadList);
        }
        if (mLeadArrayList.isEmpty()) {
            view.findViewById(R.id.noDataFoundTextView).setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            //mLeadArrayList = leadByCategoryHolderList.get(0).getLeads();
            mLeadAdapter = new LeadAdapter(mLeadArrayList, this, true, mRecyclerView);

            // if leads are greater than or equal to LEADS_PER_PAGE , it means we may get more leads in the next page
            if (mLeadArrayList.size() >= AppConstants.Values.LEADS_PER_PAGE) {
                mLeadArrayList.add(null);
            }
            //intitalizing page number to 1
            pageNumber = 1;
            mLeadAdapter.setOnLoadMoreListener(new LeadAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    Logger.e(TAG, "Search Query in OnLoadMore:" + mSearchView.getQuery());
                    //For pagination in search, Add one flag isServerSearchActivated which will be set to false
                    // in method onQueryTextChange if newText is not empty and will be set to true in onQueryTextSubmit method
                    // hence below condition would be  if (isServerSearchActivated  || (mSearchView != null && TextUtils.isEmpty(mSearchView.getQuery())))
                    if (mSearchView != null && TextUtils.isEmpty(mSearchView.getQuery())) {
                        if (mLeadArrayList.size() >= AppConstants.Values.LEADS_PER_PAGE) {
                            pageNumber++;
                            mLeadAdapter.setLoaded(true);
                            fetchLeadsByPage();
                        }
                    }
                }
            });
        }
        mRecyclerView.setAdapter(mLeadAdapter);
        mSwipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        mSwipeContainer.setOnRefreshListener(this);
        // Configure the refreshing colors
        mSwipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        return view;
    }

    private List<Lead> getLeads(FetchLeadResponse fetchLeadResponse) {
        if(null != fetchLeadResponse){
            List<LeadByCategoryHolder> leadByCategoryHolderList = fetchLeadResponse.getLeadsByAssigneeStatus();
            if (leadByCategoryHolderList != null && !leadByCategoryHolderList.isEmpty()) {
                for (LeadByCategoryHolder leadByCategoryHolder : leadByCategoryHolderList) {
                    if (AppConstants.LeadCategory.SUBMITTED.equalsIgnoreCase(leadByCategoryHolder.getCategory())) {
                        if (leadByCategoryHolder.getLeads() != null && !leadByCategoryHolder.getLeads().isEmpty()) {
                            return leadByCategoryHolder.getLeads();
                        }
                    }
                }
            }
        }
        return null;
    }

    private void fetchLeadsByPage() {
        final LeadInteractor leadInteractor = getInteractor(LeadInteractor.class);
        SearchFilter searchFilter = new SearchFilter();
        //Fetching first page
        searchFilter.setPageNumber(pageNumber);
        FetchLeadRequest fetchLeadRequest = new FetchLeadRequest();
        fetchLeadRequest.setSearchFilter(searchFilter);
        leadInteractor.fetchLeadsByPage(fetchLeadRequest);
        Logger.e(TAG, "Fetching Leads Page Number :" + pageNumber);
    }

    @Override
    public void leadsFetched(final FetchLeadResponse fetchLeadResponse) {

        //TODO Remove handler delay in actual service call
        //Adding mHandler to delay fetch from stub by 2 seconds . Please remove in actual servie call
        //mHandler = new Handler();
        //TODO replace finalLeadArrayList by leadArrayList in actual service call
        //final List<Lead> finalLeadArrayList = leadArrayList;
       /* mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {*/
        List<Lead> leadArrayList = getLeads(fetchLeadResponse);
        // Removing the progress bar item from the end of the list
        mLeadArrayList.remove(mLeadArrayList.size() - 1);
        mLeadAdapter.notifyItemRemoved(mLeadArrayList.size());
        mLeadAdapter.notifyItemRangeChanged(mLeadArrayList.size(), mLeadAdapter.getItemCount());
        // If leads returned by the service are less than LEADS_PER_PAGE , it means if we do not have more leads in the next page
        if (leadArrayList != null && !leadArrayList.isEmpty()) {
            mLeadArrayList.addAll(leadArrayList);
            if (leadArrayList != null && leadArrayList.size() >= AppConstants.Values.LEADS_PER_PAGE) {
                mLeadArrayList.add(null);
            }
            mLeadAdapter.notifyDataSetChanged();
            mLeadAdapter.setLoaded(false);
        }
         /*   }
        }, 2000);*/
    }

    @Override
    public void onRefresh() {
        //Clear search if any
        mSearchView.clearFocus();
        mSearchMenuItem.collapseActionView();
        pageNumber = 1;
        final LeadInteractor leadInteractor = getInteractor(LeadInteractor.class);
        SearchFilter searchFilter = new SearchFilter();
        //Fetching first page
        searchFilter.setPageNumber(pageNumber);
        FetchLeadRequest fetchLeadRequest = new FetchLeadRequest();
        fetchLeadRequest.setSearchFilter(searchFilter);
        leadInteractor.fetchLeads(fetchLeadRequest, true);
    }

    @Override
    public void refreshData() {
        //reset PageNumber
        pageNumber = 1;
        if (mLeadArrayList != null && !mLeadArrayList.isEmpty()) {
            mLeadArrayList.clear();
            mLeadAdapter.clear();
            // ...the data has come back, add new items to your adapter...
            AppApplication appApplication = (AppApplication) getActivity().getApplication();
            FetchLeadResponse fetchLeadResponse = appApplication.getmMyLeadData();
            List<Lead> leadList = getLeads(fetchLeadResponse);
            if (leadList != null && !leadList.isEmpty()) {
                mLeadArrayList.addAll(leadList);
                if (leadList.size() >= AppConstants.Values.LEADS_PER_PAGE) {
                    mLeadArrayList.add(null);
                }
                mLeadAdapter.setLoaded(false);
                mLeadAdapter.notifyDataSetChanged();
            }
        }
        mSwipeContainer.setRefreshing(false);
    }

    @Override
    public void leadClicked(Lead lead) {
        LeadInteractor leadInteractor = getInteractor(LeadInteractor.class);
        leadInteractor.showLeadDetails(lead, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        VipConfigurator.INSTANCE.configure(this, new LeadPresenter(), new LeadInteractor());
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        mSearchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) mSearchMenuItem.getActionView();
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //reset PageNumber
        pageNumber = 1;
        Logger.e(TAG, query);
        LeadInteractor leadInteractor = getInteractor(LeadInteractor.class);
        leadInteractor.searchLeads(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Logger.e(TAG, newText);
        if (mLeadAdapter != null) {
            mLeadAdapter.getFilter().filter(newText);
        }
        return false;
    }

    @Override
    public void setFilterResult(FetchLeadResponse response) {
        Logger.e(TAG, "setFilterResult");
        List<Lead> leadList = getLeads(response);
        if (mLeadAdapter != null) {
            mLeadAdapter.setmLeadList(leadList);
            mLeadAdapter.notifyDataSetChanged();
        }
        mSearchView.clearFocus();
       // mSearchMenuItem.collapseActionView();
    }


}
